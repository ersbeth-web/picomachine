import { Signal } from "@ersbeth/picosignal";
import type { State } from "../states";

export class CompoundMixin {
    #nodes: State[] = [];

    nodesUpdated = new Signal<void>();

    get nodes() {
        return this.#nodes;
    }

    set nodes(nodes) {
        this.#nodes = nodes;
        this.nodesUpdated.emit();
    }
}
