import type { CompoundState, State } from "../states";
import { NestedState } from "../states";

export class BaseMixin {
    #state: State;
    #name: string;
    #parent: CompoundState | null;

    constructor(state: State, name: string, parent: CompoundState | null) {
        this.#state = state;
        this.#name = name;
        this.#parent = parent;
    }

    get parent(): CompoundState | null {
        return this.#parent;
    }
    set parent(parent: CompoundState) {
        this.#parent = parent;
    } // for external state

    get name() {
        return this.#name;
    }

    get active(): boolean {
        if (!this.parent) return true;
        if (this.parent instanceof NestedState)
            return (
                this.parent.nested.current === this.#state &&
                this.parent.base.active
            );
        if (this.parent.parallel) return this.parent.base.active;

        throw "Internal error: parent should be either AndNode or OrNode";
    }

    get root(): State {
        if (this.#parent == null) return this.#state;
        return this.#parent.base.root;
    }
}
