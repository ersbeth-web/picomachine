import type { Action } from "../callbacks";

export class ActionMixin {
    #enterActions: Action[] = [];
    #exitActions: Action[] = [];

    get enterActions() {
        return this.#enterActions;
    }
    get exitActions() {
        return this.#exitActions;
    }

    addEnterActions(enterActions: Action[]) {
        this.#enterActions = [...enterActions, ...this.#enterActions];
    }

    addExitActions(exitActions: Action[]) {
        this.#exitActions = [...this.#exitActions, ...exitActions];
    }

    execEnterActions() {
        this.enterActions.forEach((action) => action());
    }

    execExitActions() {
        this.exitActions.forEach((action) => action());
    }
}
