import type { Activity } from "../callbacks";

export class AsyncMixin {
    #activity: Activity;

    constructor(activity: Activity) {
        this.#activity = activity;
    }

    get activity() {
        return this.#activity;
    }
}
