import type { Guard } from "../callbacks";
import type { CompoundState, State } from "../states";

export class NestedMixin {
    #state: CompoundState;

    #initial: State | null = null;
    #current: State | null = null;
    #history: State | null = null;
    #historyGuard: Guard;

    constructor(state: CompoundState, historyGuard: Guard) {
        this.#state = state;
        this.#historyGuard = historyGuard;
        this.forwardChanges();
        this.#state.compound.nodesUpdated.on(() => this.forwardChanges());
    }

    get initial(): State | null {
        return this.#initial;
    }
    set initial(initial: State) {
        this.#initial = initial;
    }

    get history() {
        return this.#history;
    }
    get current() {
        return this.#current;
    }

    set current(current) {
        this.#current = current;
        this.#history = current;
    }

    get historyGuard() {
        return this.#historyGuard;
    }

    forwardChanges() {
        this.#state.compound.nodes.forEach((node) =>
            node.execution.changed.on(() =>
                this.#state.execution.changed.emit(),
            ),
        );
    }

    resetHistory() {
        this.#history = null;
    }

    clearCurrent() {
        // don't use setter to keep history
        this.#current = null;
    }
}
