import { Observable, type Slot } from "@ersbeth/picosignal";
import { type State, isActorState, isCompoundState } from "../states";
import { ParallelState } from "../states";
import { Status } from "../status";

export class MachineMixin {
    status: Observable<Status> = new Observable(new Status([]));
    #state: State;
    #transitionning = false;
    #commandStack: { event: string; data: any }[] = [];

    constructor(state: State) {
        this.#state = state;
        this.status.value = new Status(this.#status);
        this.#state.execution.changed.on(() => {
            this.status.value = new Status(this.#status);
        });
    }

    get #status(): string[] {
        // compound state
        if (!isCompoundState(this.#state)) return [this.#state.base.name];

        // parallel state
        if (this.#state instanceof ParallelState)
            return this.#state.compound.nodes
                .flatMap((node) => node.machine.#status)
                .map((stateName) => `${this.#state.base.name}.${stateName}`);

        // nested state
        return !this.#state.nested.current
            ? [this.#state.base.name]
            : this.#state.nested.current.machine.#status.map(
                  (stateName) => `${this.#state.base.name}.${stateName}`,
              );
    }

    start() {
        this.#state.execution.enter();
    }

    send(event: string, data: any = null): void {
        if (!this.#transitionning) {
            this.#transitionning = true;
            this.#state.execution.execCommand(event, data);
            while (this.#commandStack.length > 0) {
                const command = this.#commandStack.shift();
                if (!command) throw "command is null";
                this.#state.execution.execCommand(command.event, command.data);
            }
            this.#transitionning = false;
        } else {
            this.#commandStack.push({ event, data });
        }
    }

    onError(callback: Slot<void>) {
        if (isActorState(this.#state))
            return this.#state.actor.onError.on(callback);
        throw "OnError must be implemented in subclass";
    }

    onDone(callback: Slot<void>) {
        if (isActorState(this.#state))
            return this.#state.actor.onDone.on(callback);
        throw "OnDone must be implemented in subclass";
    }

    wait(stateDescriptor: string | RegExp, timeout = 1000) {
        return new Promise<void>((resolve, reject) => {
            if (this.status.value.matches(stateDescriptor)) {
                resolve();
            } else {
                const timeoutID = setTimeout(() => {
                    reject(
                        `couldn't reach ${stateDescriptor} under ${timeout}ms`,
                    );
                }, timeout);
                const unbind: { func?: () => void } = {};
                unbind.func = this.status.onChanged(() => {
                    if (this.status.value.matches(stateDescriptor)) {
                        clearTimeout(timeoutID);
                        if (!unbind.func) throw "unbind.func is null";
                        unbind.func();
                        resolve();
                    }
                });
            }
        });
    }
}
