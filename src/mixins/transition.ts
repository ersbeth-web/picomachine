import type { Action, Guard } from "../callbacks";
import type { State } from "../states";

export class BaseTransition {
    readonly guard;
    readonly actions;
    readonly delay;

    constructor(guard: Guard | null, actions: Action[], delay: number) {
        this.guard = guard;
        this.actions = actions;
        this.delay = delay;
    }
}

export class InternalTransition extends BaseTransition {}

export class ExternalTransition extends BaseTransition {
    readonly target: State;

    constructor(
        target: State,
        guard: Guard | null,
        actions: Action[],
        delay: number,
    ) {
        super(guard, actions, delay);
        this.target = target;
    }
}

export type Transition = InternalTransition | ExternalTransition;
