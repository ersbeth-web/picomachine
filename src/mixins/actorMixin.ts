import { Observable, Signal } from "@ersbeth/picosignal";
import type { Transition } from "./transition";

export class ActorMixin {
    constructor() {
        this.done.onChanged((value) => {
            if (value) this.onDone.emit();
        });
        this.error.onChanged((value) => {
            if (value) this.onError.emit();
        });
    }

    #errorTransitions: Transition[] = [];
    #doneTransitions: Transition[] = [];

    done = new Observable(false);
    error = new Observable(false);

    onError = new Signal<void>();
    onDone = new Signal<void>();

    get doneTransitions() {
        return this.#doneTransitions;
    }
    get errorTransitions() {
        return this.#errorTransitions;
    }

    addErrorTransitions(errorTransitions: Transition[]) {
        this.#errorTransitions = [
            ...this.#errorTransitions,
            ...errorTransitions,
        ];
    }

    addDoneTransitions(doneTransitions: Transition[]) {
        this.#doneTransitions = [...this.#doneTransitions, ...doneTransitions];
    }
}
