import { type ActorState, type CompoundState, isActorState } from "../states";

export class ParallelMixin {
    #state: CompoundState;
    #actorNodes: ActorState[] = [];

    constructor(state: CompoundState) {
        this.#state = state;
        this.forwardChanges();
        this.#state.compound.nodesUpdated.on(() => this.forwardChanges());
    }

    forwardChanges() {
        this.#actorNodes = this.#state.compound.nodes.filter((node) =>
            isActorState(node),
        );
        this.#state.compound.nodes.forEach((node) =>
            node.execution.changed.on(() =>
                this.#state.execution.changed.emit(),
            ),
        );
        this.#actorNodes.forEach((node) => {
            if (!isActorState(node))
                throw "Internal error: node should have actor";
            node.actor.done.onChanged(() => this.processDone());
        });
        this.#actorNodes.forEach((node) => {
            if (!isActorState(node))
                throw "Internal error: node should have actor";
            node.actor.error.onChanged(() => this.processError());
        });
    }

    processDone() {
        const done = this.#actorNodes.reduce(
            (acc, node) => acc && (node.actor.done.value ?? false),
            true,
        );
        if (done) {
            this.#state.actor.done.value = true;
            this.#state.execution.execCommand("done", {});
        }
    }

    processError() {
        const error = this.#actorNodes.reduce(
            (acc, node) => acc || (node.actor.error.value ?? false),
            true,
        );
        if (error) {
            this.#state.actor.error.value = true;
            this.#state.execution.execCommand("error", {});
        }
    }
}
