import { Signal } from "@ersbeth/picosignal";
import { type State, isActorState, isFinalState } from "../states";
import {
    AsyncState,
    DoneState,
    ErrorState,
    NestedState,
    ParallelState,
} from "../states";
import {
    ExternalTransition,
    InternalTransition,
    type Transition,
} from "./transition";

export class ExecutionMixin {
    #state: State;
    #pendingTransitionIds: ReturnType<typeof setTimeout>[] = [];
    changed = new Signal<void>();

    constructor(state: State) {
        this.#state = state;
    }

    enter() {
        if (isActorState(this.#state)) {
            this.#state.actor.done.value = false;
            this.#state.actor.error.value = false;
        }

        this.#state.action.execEnterActions();

        if (
            this.#state.base.parent &&
            this.#state.base.parent instanceof NestedState
        ) {
            this.#state.base.parent.nested.current = this.#state;
        }

        this.changed.emit();

        // execute auto transitions
        if (!isFinalState(this.#state)) {
            this.#execTransitions(this.#state.transition.autoTransitions, {});
        }

        if (this.#state instanceof NestedState) {
            // we exited in an auto-transition, so we don't enter child
            if (
                this.#state.base.parent &&
                this.#state.base.parent instanceof NestedState &&
                this.#state.base.parent.nested.current !== this.#state
            )
                return;

            if (
                !this.#state.nested.historyGuard() ||
                !this.#state.nested.history
            )
                this.#state.nested.current = this.#state.nested.initial;
            else this.#state.nested.current = this.#state.nested.history;
            if (this.#state.nested.current)
                this.#state.nested.current.execution.enter();
        }

        if (this.#state instanceof ParallelState) {
            this.#state.compound.nodes.forEach((node) =>
                node.execution.enter(),
            );
        }

        if (this.#state instanceof AsyncState) {
            this.#state.asynch
                .activity()
                .then((value) => {
                    if (this.#state.base.active)
                        this.execCommand("done", value);
                })
                .catch((value) => {
                    if (this.#state.base.active)
                        this.execCommand("error", value);
                });
        }

        if (this.#state instanceof ErrorState) {
            if (this.#state.base.parent)
                this.#state.base.parent.execution.execCommand(
                    "error",
                    this.#state.base.name,
                );
        }

        if (this.#state instanceof DoneState) {
            if (this.#state.base.parent) {
                // reset history on done
                if (this.#state.base.parent instanceof NestedState)
                    this.#state.base.parent.nested.resetHistory();
                this.#state.base.parent.execution.execCommand(
                    "done",
                    this.#state.base.name,
                );
            }
        }
    }

    exit() {
        if (this.#state instanceof ParallelState) {
            this.#state.compound.nodes.forEach((node) => node.execution.exit());
        }

        if (this.#state instanceof NestedState) {
            if (this.#state.nested.current)
                this.#state.nested.current.execution.exit();
        }
        if (!isFinalState(this.#state)) this.#cancelPendingTransitions();
        this.#state.action.execExitActions();

        if (this.#state instanceof NestedState) {
            this.#state.nested.clearCurrent();
        }
    }

    execCommand(event: string, data: any): boolean {
        let accepted = (() => {
            if (isActorState(this.#state)) {
                switch (event) {
                    case "done":
                        this.#state.actor.done.value = true;
                        return this.#execTransitions(
                            this.#state.actor.doneTransitions,
                            data,
                        );
                    case "error":
                        this.#state.actor.error.value = true;
                        return this.#execTransitions(
                            this.#state.actor.errorTransitions,
                            data,
                        );
                }
            }

            if (!isFinalState(this.#state)) {
                const accepted = this.#execTransitions(
                    this.#state.transition.eventsTransitions[event],
                    data,
                );
                return accepted;
            }

            return false;
        })() as boolean;

        if (this.#state instanceof NestedState) {
            if (!accepted) {
                if (!this.#state.nested.current)
                    throw "[ExecutionMixin] nested.current is null";
                accepted = this.#state.nested.current.execution.execCommand(
                    event,
                    data,
                );
            }
        }

        if (this.#state instanceof ParallelState) {
            if (!accepted) {
                accepted = this.#state.compound.nodes
                    .map((node) => node.execution.execCommand(event, data))
                    .reduce((acc, accepted) => acc || accepted);
            }
        }

        return accepted;
    }

    raise(event: string, data: any) {
        this.#state.base.root.machine.send(event, data);
    }

    #execTransitions(transitions: Transition[], data?: any) {
        if (!isFinalState(this.#state)) {
            const transition = this.#state.transition.findTransition(
                transitions,
                data,
            );
            if (!transition) return false;

            if (!transition.delay) {
                this.#execTransition(transition, data);
            } else {
                this.#execDelayTransition(transition, data);
            }

            return true;
        }
        return false;
    }

    #execTransition(transition: Transition, data: any) {
        if (!isFinalState(this.#state)) {
            if (transition instanceof InternalTransition) {
                this.#state.transition.execTransitionActions(transition, data);
            }

            if (transition instanceof ExternalTransition) {
                this.exit();
                this.#state.transition.execTransitionActions(transition, data);

                const raiseAction = transition.actions.find(
                    (action) => action.isRaise,
                );

                // root: re-enter
                if (!this.#state.base.parent) {
                    this.#state.execution.enter();
                } else if (
                    this.#state.base.parent &&
                    this.#state.base.parent instanceof NestedState
                ) {
                    //raise: leave and clear current
                    if (raiseAction) {
                        this.#state.base.parent.nested.clearCurrent();
                    }
                    // normal: enter target
                    else {
                        this.#state.base.parent.nested.current =
                            transition.target; //allows root transitions

                        transition.target.execution.enter();
                    }
                }
            }
        }
    }

    #execDelayTransition(transition: Transition, data: any) {
        const pendingId = setTimeout(() => {
            this.#execTransition(transition, data);
        }, transition.delay);

        this.#pendingTransitionIds.push(pendingId);
    }

    #cancelPendingTransitions() {
        while (this.#pendingTransitionIds.length > 0) {
            const pendingId = this.#pendingTransitionIds.pop();
            clearTimeout(pendingId);
        }
    }
}
