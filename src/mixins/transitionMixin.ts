import type { BaseTransition, Transition } from "./transition";

export class TransitionMixin {
    #autoTransitions: Transition[] = [];
    #eventsTransitions: Record<string, Transition[]> = {};

    get autoTransitions() {
        return this.#autoTransitions;
    }
    get eventsTransitions() {
        return this.#eventsTransitions;
    }

    addAutoTransitions(autoTransitions: Transition[]) {
        this.#autoTransitions = [...this.#autoTransitions, ...autoTransitions];
    }

    addEventTransitions(eventsTransitions: Record<string, Transition[]>) {
        this.#eventsTransitions = {
            ...this.#eventsTransitions,
            ...eventsTransitions,
        };
    }

    findTransition(transitions: Transition[], data: any) {
        if (!transitions) return null;
        return transitions.find(
            (transition) => !transition.guard || transition.guard(data),
        );
    }

    execTransitionActions(transition: BaseTransition, data: any) {
        transition.actions.forEach((action) => action(data));
    }
}
