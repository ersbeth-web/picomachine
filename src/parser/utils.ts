type UnionKeys<T> = T extends T ? keyof T : never;
type Expand<T> = T extends T ? { [K in keyof T]: T[K] } : never;

// Exclusive Or type util
export type OneOf<T extends any[]> = {
    [K in keyof T]: Expand<
        T[K] & Partial<Record<Exclude<UnionKeys<T[number]>, keyof T[K]>, never>>
    >;
}[number];

// Safeguard at the end of a switch statement
export function assertNever(x: never): never {
    throw new Error(`Unexpected object: ${x}`);
}
