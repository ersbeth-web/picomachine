import { type Action, type Guard, isTrigger } from "../callbacks";
import {
    ExternalTransition,
    InternalTransition,
    type Transition,
} from "../mixins";
import {
    AsyncState,
    AtomicState,
    type CompoundState,
    DoneState,
    ErrorState,
    NestedState,
    ParallelState,
    type State,
    isActorState,
    isFinalState,
} from "../states";

import type {
    StrictActionConfig,
    StrictEventsConfig,
    StrictStateConfig,
    StrictTransitionConfig,
} from "./configStrict";
import { assertNever } from "./utils";

/**
 * Represents a command that maps an event to its corresponding actions and guards.
 *
 * @remarks
 * A `Command` is used internally by the state machine to associate an event with the
 * actions to be executed and the guard functions to be evaluated when the event is triggered.
 *
 * @public
 */
export interface Command {
    /**
     * The name of the event that triggers this command.
     */
    event: string;

    /**
     * An array of actions to execute when the event occurs.
     */
    actions: Action[];

    /**
     * An array of guard functions to evaluate when the event occurs.
     * All guards must return `true` for the command to be executed.
     */
    guards: Guard[];
}

export const Parser = {
    getCommands(config: StrictStateConfig): Command[] {
        const commands: Command[] = [];
        if (config.events) {
            Object.keys(config.events).forEach((event) => {
                const actions = config.events[event]
                    //@ts-ignore
                    .flatMap((cfg) => cfg.actions)
                    .filter((action) => !isTrigger(action)) as Action[];
                const guards = config.events[event]
                    .flatMap((cfg) => cfg.guard)
                    .filter((guard) => guard != null) as Guard[];

                commands.push({
                    event: event,
                    actions: actions,
                    guards: guards,
                });
            });
        }

        const childCommands = config.states
            ? Object.keys(config.states).flatMap((stateName) =>
                  Parser.getCommands(config.states[stateName]),
              )
            : [];

        childCommands.forEach((command) => {
            const existingCommand = commands.find(
                (c) => c.event === command.event,
            );
            if (existingCommand) {
                existingCommand.actions.push(...command.actions);
                existingCommand.guards.push(...command.guards);
            } else commands.push(command);
        });

        return commands;
    },

    getState(
        name: string,
        parent: CompoundState | null,
        config: StrictStateConfig,
    ) {
        // get correct state
        let node: State;

        switch (config.type) {
            case "atom":
                node = new AtomicState(name, parent);
                break;
            case "error":
                node = new ErrorState(name, parent);
                break;
            case "done":
                node = new DoneState(name, parent);
                break;
            case "async":
                node = new AsyncState(name, parent, config.activity);
                break;
            case "parallel":
                node = new ParallelState(name, parent);
                break;
            case "nested":
                node = new NestedState(name, parent, config.history);
                break;
            default:
                assertNever(config); // will raise a ts error if we forget to handle new config types
        }

        // apply enter/exit actions
        node.action.addEnterActions(Parser.getActions(config.enter));
        node.action.addExitActions(Parser.getActions(config.exit));

        // process children distributed event config if any

        if (config.type === "nested") {
            const childrenEventConfig: Record<
                string,
                StrictTransitionConfig[]
            > = {};
            Object.keys(config.events).forEach((event) => {
                const transitions = config.events[event];
                const forChildren = transitions.filter(
                    (transition) => transition.distribute,
                );
                const forCurrent = transitions.filter(
                    (transition) => !transition.distribute,
                );
                if (forChildren.length > 0) {
                    childrenEventConfig[event] = forChildren;
                }
                if (forCurrent.length > 0) {
                    config.events[event] = forCurrent;
                } else {
                    delete config.events[event];
                }
            });

            Object.keys(config.states).forEach((state) => {
                const childConfig = config.states[state];
                if (
                    childConfig.type !== "error" &&
                    childConfig.type !== "done"
                ) {
                    Object.keys(childrenEventConfig).forEach((event) => {
                        const transitions = childrenEventConfig[event].filter(
                            (transition) => transition.target !== state,
                        );
                        if (childConfig.events[event])
                            childConfig.events[event].unshift(...transitions);
                        else childConfig.events[event] = transitions;
                    });
                }
            });
        }

        // in case of root node apply self transitions (internal + external)
        // otherwise the parent will apply all the transitions.
        if (parent == null) {
            Parser.applyTransitions(node, config, [node]);
        }

        // in case of compound get children
        if (config.type === "nested" || config.type === "parallel") {
            const children = Object.keys(config.states).map((name) =>
                Parser.getState(
                    name,
                    node as CompoundState,
                    config.states[name],
                ),
            );
            (node as CompoundState).compound.nodes = children;

            // apply transitions to children
            children.forEach((child) => {
                // allow only self transitions inside parallel compound state
                const siblings = config.type === "nested" ? children : [child];
                Parser.applyTransitions(
                    child,
                    config.states[child.base.name],
                    siblings,
                );
            });

            // in case of nested get initial
            if (config.type === "nested")
                (node as NestedState).nested.initial = Parser.getInitial(
                    config.initial,
                    children,
                );
        }

        return node;
    },

    applyTransitions(
        node: State,
        config: StrictStateConfig,
        siblings: State[],
    ): void {
        if (!isFinalState(node) && config.always)
            node.transition.addAutoTransitions(
                Parser.getTransitions(config.always, node, siblings),
            );
        if (!isFinalState(node) && config.events)
            node.transition.addEventTransitions(
                Parser.getEventTransitions(config.events, node, siblings),
            );
        if (isActorState(node) && config.done)
            node.actor.addDoneTransitions(
                Parser.getTransitions(config.done, node, siblings),
            );
        if (isActorState(node) && config.error)
            node.actor.addErrorTransitions(
                Parser.getTransitions(config.error, node, siblings),
            );
    },

    getActions(config: StrictActionConfig, node?: State): Action[] {
        const actions = config.map((actionConfig) => {
            if (!isTrigger(actionConfig)) return actionConfig;

            // return raise action
            if (!node)
                throw `Can't raise event ${actionConfig}in enter/exit actions. Put it in a transition instead.`;
            const raise = (data: any) => {
                if (node.base.parent == null)
                    throw `Can't raise event ${actionConfig} on state ${node.base.name} without parent.`;
                node.base.parent.execution.raise(actionConfig, data);
            };
            raise.isRaise = true; // used later to test if action is a raise
            return raise;
        });
        return actions;
    },

    getTransition(config: StrictTransitionConfig, node: State, nodes: State[]) {
        if (config.target === "internal") {
            return new InternalTransition(
                config.guard,
                Parser.getActions(config.actions, node),
                config.delay,
            );
        }

        if (config.target === "self") {
            return new ExternalTransition(
                node,
                config.guard,
                Parser.getActions(config.actions, node),
                config.delay,
            );
        }

        const target = nodes.find((node) => node.base.name === config.target);
        if (!target) throw `target subnode ${config.target} is not defined`;
        if (target === node)
            throw `Use either "self" or "internal" target for self transitions`;
        return new ExternalTransition(
            target,
            config.guard,
            Parser.getActions(config.actions, node),
            config.delay,
        );
    },

    getTransitions(
        configs: StrictTransitionConfig[],
        node: State,
        nodes: State[],
    ) {
        return configs.map((config) =>
            Parser.getTransition(config, node, nodes),
        );
    },

    getEventTransitions(
        config: StrictEventsConfig,
        node: State,
        nodes: State[],
    ) {
        // compute events
        const events: { [key: string]: Transition[] } = {};
        Object.keys(config).forEach((eventName) => {
            const transitionsConfig = config[eventName];
            const transitions = Parser.getTransitions(
                transitionsConfig,
                node,
                nodes,
            );
            events[eventName] = transitions;
        });
        return events;
    },

    getInitial(config: string | undefined, nodes: State[]) {
        const node = nodes.find((node) => node.base.name === config);
        if (!node) throw `Initial state ${config} doesn't exist.`;
        return node;
    },
};
