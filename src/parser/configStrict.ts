import type { Action, Activity, Guard, Trigger } from "../callbacks";
import type { StateConfig, TransitionConfig } from "../config";
import type { OneOf } from "./utils";

export type StrictStateConfig = OneOf<
    [
        StrictAtomicConfig,
        StrictAsyncConfig,
        StrictParallelConfig,
        StrictNestedConfig,
        StrictDoneConfig,
        StrictErrorConfig,
    ]
>;

export function toStrict(config: StateConfig): StrictStateConfig {
    switch (config.type) {
        case "atom":
            return toStrictAtomic(config);
        case "async":
            return toStrictAsync(config);
        case "external":
            return toStrictExternal(config);
        case "nested":
            return toStrictNested(config);
        case "parallel":
            return toStrictParallel(config);
        case "error":
            return toStrictError(config);
        case "done":
            return toStrictFinal(config);
        default:
            throw new Error(`Unexpected object: ${config}`);
    }
}

export type StrictActionConfig = (Action | Trigger)[];

export interface StrictAtomicConfig extends StrictBaseConfig {
    type: "atom";
}

export interface StrictAsyncConfig extends StrictBaseConfig, StrictActorConfig {
    type: "async";
    activity: Activity;
}

export interface StrictParallelConfig
    extends StrictBaseConfig,
        StrictCompoundConfig,
        StrictActorConfig {
    type: "parallel";
}

export interface StrictNestedConfig
    extends StrictBaseConfig,
        StrictCompoundConfig,
        StrictActorConfig {
    type: "nested";
    initial: string;
    history: Guard;
}

export interface StrictDoneConfig extends StrictFinalConfig {
    type: "done";
}

export interface StrictErrorConfig extends StrictFinalConfig {
    type: "error";
}

export interface StrictFinalConfig {
    name?: string;
    enter: StrictActionConfig;
    exit: StrictActionConfig;
}

export interface StrictBaseConfig {
    name?: string;
    enter: StrictActionConfig;
    exit: StrictActionConfig;
    always: StrictTransitionConfig[];
    events: StrictEventsConfig;
}

export interface StrictActorConfig {
    done: StrictTransitionConfig[];
    error: StrictTransitionConfig[];
}

export interface StrictCompoundConfig {
    states: { [key: string]: StrictStateConfig };
}

export interface StrictTransitionConfig {
    target: string;
    guard: Guard | null;
    delay: number;
    actions: StrictActionConfig;
    distribute: boolean;
}

export type StrictEventsConfig = { [key: string]: StrictTransitionConfig[] };

function toStrictAtomic(config: StateConfig): StrictAtomicConfig {
    if (config.type !== "atom") throw new Error("Invalid config");

    return {
        type: config.type,
        ...toStrictBase(config),
    };
}

function toStrictAsync(config: StateConfig): StrictAsyncConfig {
    if (config.type !== "async") throw new Error("Invalid config");
    if (!config.activity) throw new Error("Invalid config");

    return {
        type: config.type,
        activity: config.activity,
        ...toStrictBase(config),
        ...toStrictActor(config),
    };
}

function toStrictExternal(config: StateConfig): StrictStateConfig {
    if (config.type !== "external") throw new Error("Invalid config");
    if (!config.source) throw new Error("Invalid config");

    const external = {
        ...toStrictBase(config),
        ...toStrictActor(config),
    };

    const internal = toStrict(config.source);

    internal.enter = [...external.enter, ...internal.enter];
    internal.exit = [...internal.exit, ...external.exit];
    internal.always = [...(internal.always ?? []), ...external.always];
    internal.done = [...(internal.done ?? []), ...external.done];
    internal.error = [...(internal.error ?? []), ...external.error];
    internal.events = {
        ...internal.events,
        ...external.events,
    };

    return internal;
}

function toStrictParallel(config: StateConfig): StrictParallelConfig {
    if (config.type !== "parallel") throw new Error("Invalid config");

    return {
        type: config.type,
        ...toStrictBase(config),
        ...toStrictCompound(config),
        ...toStrictActor(config),
    };
}

function toStrictNested(config: StateConfig): StrictNestedConfig {
    if (config.type !== "nested") throw new Error("Invalid config");
    if (!config.initial) throw new Error("Invalid config");

    return {
        type: config.type,
        initial: config.initial,
        history:
            typeof config.history === "function"
                ? config.history
                : config.history
                  ? () => true
                  : () => false,
        ...toStrictBase(config),
        ...toStrictCompound(config),
        ...toStrictActor(config),
    };
}

function toStrictFinal(config: StateConfig): StrictDoneConfig {
    if (config.type !== "done") throw new Error("Invalid config");
    return {
        type: config.type,
        ...toStrictTerminal(config),
    };
}

function toStrictError(config: StateConfig): StrictErrorConfig {
    if (config.type !== "error") throw new Error("Invalid config");
    return {
        type: config.type,
        ...toStrictTerminal(config),
    };
}

function toStrictBase(config: StateConfig): StrictBaseConfig {
    return {
        name: config.name,
        enter: toStrictActions(config.enter),
        exit: toStrictActions(config.exit),
        always: toStrictTransitions(config.always),
        events: toStrictEvents(config.events),
    };
}

function toStrictTerminal(config: StateConfig): StrictFinalConfig {
    if (config.type !== "done" && config.type !== "error")
        throw new Error("Invalid config");
    return {
        name: config.name,
        enter: toStrictActions(config.enter),
        exit: toStrictActions(config.exit),
    };
}

function toStrictActor(config: StateConfig): StrictActorConfig {
    return {
        done: toStrictTransitions(config.done),
        error: toStrictTransitions(config.error),
    };
}

function toStrictCompound(config: StateConfig): StrictCompoundConfig {
    if (config.type !== "nested" && config.type !== "parallel")
        throw new Error("Invalid config");
    if (!config.states) throw new Error("Invalid config");

    const strictConfig: any = {};
    Object.keys(config.states).forEach((stateName) => {
        strictConfig[stateName] = toStrict(config.states![stateName]);
    });
    return { states: strictConfig };
}

function toStrictActions(
    config?: Action | Trigger | (Action | Trigger)[],
): (Action | Trigger)[] {
    return config ? (config instanceof Array ? config : [config]) : [];
}

function toStrictTransition(config: TransitionConfig): StrictTransitionConfig {
    return {
        target: config.target,
        guard: config.guard ?? null,
        delay: config.delay ?? 0,
        distribute: config.distribute ?? false,
        actions: toStrictActions(config.actions),
    };
}

function toStrictTransitions(
    config?: Readonly<TransitionConfig | TransitionConfig[]>,
): StrictTransitionConfig[] {
    return config
        ? Array.isArray(config)
            ? config.map((cf) => toStrictTransition(cf))
            : [toStrictTransition(config as TransitionConfig)]
        : [];
}

function toStrictEvents(config?: {
    [key: string]: TransitionConfig | TransitionConfig[];
}): { [key: string]: StrictTransitionConfig[] } {
    if (!config) return {};
    const strictConfig: any = {};
    Object.keys(config).forEach((key) => {
        strictConfig[key] = toStrictTransitions(config[key]);
    });
    return strictConfig;
}
