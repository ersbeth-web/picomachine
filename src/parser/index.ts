export type { StateConfig } from "../config";
export { toStrict } from "./configStrict";
export { Parser, type Command } from "./parser";
