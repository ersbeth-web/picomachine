import type { StateConfig } from "./config";

// This forces TypeScript to simplify (or “collapse”) a type.
type Simplify<T> = { [K in keyof T]: T[K] } & {};

/**
 * Extracts the event keys defined on the current state configuration.
 *
 * If the configuration T has an `events` property, returns its keys; otherwise, never.
 */
export type ExtractCurrentEventNames<T extends StateConfig> = T extends {
    events: infer E;
}
    ? E extends Readonly<Record<string, any>>
        ? keyof E
        : never
    : never;

/**
 * Recursively extracts event names from the nested states (children) of the configuration.
 *
 * If T has a `states` property (a record of nested StateConfig objects),
 * then for each key K in that record, recursively extract event names and union them.
 */
export type ExtractChildrenEventNames<T extends StateConfig> = T extends {
    states: infer S;
}
    ? S extends Readonly<Record<string, StateConfig>>
        ? Simplify<{ [K in keyof S]: ExtractEventNames<S[K]> }[keyof S]>
        : never
    : never;

/**
 * Extracts event names from an external state.
 *
 * If T is an external state (with type "external" and a `source` property),
 * then extract event names from its source machine’s configuration.
 */
export type ExtractExternalEventNames<T extends StateConfig> = T extends {
    type: "external";
    source: infer Source;
}
    ? Source extends Readonly<StateConfig>
        ? Simplify<ExtractEventNames<Source>>
        : never
    : never;

/**
 * Recursively extracts event names from a state configuration.
 *
 * This type is the union of:
 * - The event keys defined on T directly,
 * - The event keys from any nested (child) states,
 * - And, if T is an external state, the event keys from its source.
 *
 * The result is wrapped in Simplify to collapse the union.
 */
export type ExtractEventNames<T extends StateConfig> = Simplify<
    | ExtractCurrentEventNames<T>
    | ExtractChildrenEventNames<T>
    | ExtractExternalEventNames<T>
>;

/**
 * Computes the qualified name for a given key.
 *
 * If no prefix is provided, the qualified name is simply K.
 * Otherwise, it concatenates the prefix and K with a dot.
 *
 * @template K - The current state's key.
 * @template Prefix - The accumulated prefix (defaults to an empty string).
 */
export type ExtractCurrentStateName<
    K extends string,
    Prefix extends string = "",
> = Prefix extends "" ? K : `${Prefix}.${K}`;

/**
 * Extracts state names from an external state.
 *
 * If T is an external state (i.e. type "external" with a source machine),
 * then extract the state names from its source configuration,
 * using the qualified current key as the new prefix.
 *
 * @template T - The state configuration.
 * @template Prefix - The prefix to qualify the external state's names.
 */
export type ExtractExternalStateNames<
    T extends StateConfig,
    Prefix extends string,
> = T extends { type: "external"; source: infer Source }
    ? Source extends Readonly<StateConfig>
        ? ExtractStateNames<Source, Prefix>
        : never
    : never;

/**
 * Recursively extracts state names from the children of T.
 *
 * If T has a `states` property, then for each key K in that record,
 * recursively extract state names from the nested configuration,
 * using the qualified name (computed by ExtractCurrentStateName) as the new prefix.
 *
 * @template T - The state configuration.
 * @template Prefix - The prefix to qualify the nested state's names.
 */
export type ExtractChildrenStateNames<
    T extends StateConfig,
    Prefix extends string,
> = T extends { states: infer S }
    ? S extends Readonly<Record<string, StateConfig>>
        ? {
              [K in keyof S]: ExtractStateNames<
                  S[K],
                  ExtractCurrentStateName<K & string, Prefix>
              >;
          }[keyof S]
        : never
    : never;

/**
 * Recursively extracts fully qualified state names from a state machine configuration.
 *
 * For a given configuration T and an optional Prefix, this type produces a union of:
 *
 * - The qualified names for the states at the current level, and
 * - The qualified names extracted recursively from nested states.
 *
 * When a state is marked as "external", its source machine’s state names are extracted
 * and qualified with the external state’s key.
 *
 * @template T - The state machine configuration.
 * @template Prefix - The accumulated prefix (default is an empty string).
 */
export type ExtractStateNames<
    T extends StateConfig,
    Prefix extends string = "",
> = T extends { states: infer S }
    ? S extends Readonly<Record<string, StateConfig>>
        ? {
              [K in keyof S]: // Compute the current qualified name for key K.
                  | ExtractCurrentStateName<K & string, Prefix>
                  // And union with either:
                  // a) For an external state: extract state names from its source,
                  // b) Otherwise, extract state names from its children.
                  | (S[K] extends { type: "external"; source: any }
                        ? ExtractExternalStateNames<
                              S[K],
                              ExtractCurrentStateName<K & string, Prefix>
                          >
                        : ExtractChildrenStateNames<
                              S[K],
                              ExtractCurrentStateName<K & string, Prefix>
                          >);
          }[keyof S]
        : Prefix extends ""
          ? never
          : Prefix
    : Prefix extends ""
      ? never
      : Prefix;

/**
 * Extracts fully qualified state names from a configuration.
 *
 * If the configuration has a `name` property, that name is used as the starting prefix.
 *
 * For example, if your configuration is declared as:
 *
 *    {
 *       name: "root",
 *       type: "nested",
 *       initial: "A",
 *       states: { ... }
 *    }
 *
 * then the resulting union will include "root", "root.A", "root.A.B", etc.
 */
export type ExtractFullStateNames<T extends StateConfig> = T extends {
    name: infer N extends string;
}
    ? `${N}` | ExtractStateNames<T, `${N}`>
    : ExtractStateNames<T>;
