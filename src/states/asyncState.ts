import type { Activity } from "../callbacks";
import {
    ActionMixin,
    ActorMixin,
    AsyncMixin,
    BaseMixin,
    ExecutionMixin,
    MachineMixin,
    TransitionMixin,
} from "../mixins";
import type { CompoundState } from "./compoundState";

export class AsyncState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    transition: TransitionMixin;
    actor: ActorMixin;
    asynch: AsyncMixin;

    constructor(
        name: string,
        parent: CompoundState | null,
        activity: Activity,
    ) {
        this.base = new BaseMixin(this, name, parent);
        this.action = new ActionMixin();
        this.transition = new TransitionMixin();
        this.actor = new ActorMixin();
        this.asynch = new AsyncMixin(activity);
        this.execution = new ExecutionMixin(this);
        this.machine = new MachineMixin(this);
    }
}
