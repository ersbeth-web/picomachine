import {
    ActionMixin,
    BaseMixin,
    ExecutionMixin,
    MachineMixin,
} from "../mixins";
import type { CompoundState } from "./compoundState";

export class ErrorState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    isError = true;

    constructor(name: string, parent: CompoundState | null) {
        this.base = new BaseMixin(this, name, parent);
        this.action = new ActionMixin();
        this.execution = new ExecutionMixin(this);
        this.machine = new MachineMixin(this);
    }
}
