import {
    ActionMixin,
    BaseMixin,
    ExecutionMixin,
    MachineMixin,
    TransitionMixin,
} from "../mixins";
import type { CompoundState } from "./compoundState";

export class AtomicState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    transition: TransitionMixin;
    isAtomic = true;

    constructor(name: string, parent: CompoundState | null) {
        this.base = new BaseMixin(this, name, parent);
        this.action = new ActionMixin();
        this.transition = new TransitionMixin();
        this.execution = new ExecutionMixin(this);
        this.machine = new MachineMixin(this);
    }
}
