import type { Guard } from "../callbacks";
import {
    ActionMixin,
    ActorMixin,
    BaseMixin,
    CompoundMixin,
    ExecutionMixin,
    MachineMixin,
    NestedMixin,
    TransitionMixin,
} from "../mixins";
import type { CompoundState } from "./compoundState";

export class NestedState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    transition: TransitionMixin;
    actor: ActorMixin;
    compound: CompoundMixin;
    nested: NestedMixin;

    constructor(
        name: string,
        parent: CompoundState | null,
        historyGuard: Guard,
    ) {
        this.base = new BaseMixin(this, name, parent);
        this.action = new ActionMixin();
        this.transition = new TransitionMixin();
        this.actor = new ActorMixin();
        this.compound = new CompoundMixin();
        this.nested = new NestedMixin(this, historyGuard);
        this.execution = new ExecutionMixin(this);
        this.machine = new MachineMixin(this);
    }
}
