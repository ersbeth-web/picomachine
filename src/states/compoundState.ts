import type { NestedState } from "./nestedState";
import type { ParallelState } from "./parallelState";
import type { State } from "./state";

export type CompoundState = NestedState | ParallelState;

export function isCompoundState(state: State): state is CompoundState {
    return (state as CompoundState).compound !== undefined;
}
