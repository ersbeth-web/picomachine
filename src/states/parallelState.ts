import {
    ActionMixin,
    ActorMixin,
    BaseMixin,
    CompoundMixin,
    ExecutionMixin,
    MachineMixin,
    ParallelMixin,
    TransitionMixin,
} from "../mixins";
import type { CompoundState } from "./compoundState";

export class ParallelState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    transition: TransitionMixin;
    actor: ActorMixin;
    compound: CompoundMixin;
    parallel: ParallelMixin;

    constructor(name: string, parent: CompoundState | null) {
        this.base = new BaseMixin(this, name, parent);
        this.action = new ActionMixin();
        this.transition = new TransitionMixin();
        this.actor = new ActorMixin();
        this.compound = new CompoundMixin();
        this.parallel = new ParallelMixin(this);
        this.execution = new ExecutionMixin(this);
        this.machine = new MachineMixin(this);
    }
}
