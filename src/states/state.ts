import type { AsyncState } from "./asyncState";
import type { AtomicState } from "./atomicState";
import type { DoneState } from "./doneState";
import type { ErrorState } from "./errorState";
import type { NestedState } from "./nestedState";
import type { ParallelState } from "./parallelState";

export type State =
    | AtomicState
    | AsyncState
    | NestedState
    | ParallelState
    | DoneState
    | ErrorState;
