import type { DoneState } from "./doneState";
import type { ErrorState } from "./errorState";
import type { State } from "./state";

export type FinalState = DoneState | ErrorState;

export function isFinalState(state: State): state is DoneState | ErrorState {
    return !Object.hasOwn(state, "transition");
}
