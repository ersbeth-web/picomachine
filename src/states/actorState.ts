import type { AsyncState } from "./asyncState";
import type { NestedState } from "./nestedState";
import type { ParallelState } from "./parallelState";
import type { State } from "./state";

export type ActorState = NestedState | AsyncState | ParallelState;

export function isActorState(state: State): state is ActorState {
    return (state as ActorState).actor !== undefined;
}
