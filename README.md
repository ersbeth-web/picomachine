# PicoMachine

![version](https://badgen.net/npm/v/@ersbeth/picomachine)
![pipeline](https://gitlab.com/ersbeth-web/picomachine/badges/main/pipeline.svg)
![coverage](https://gitlab.com/ersbeth-web/picomachine/badges/main/coverage.svg) 
![types](https://badgen.net/npm/types/@ersbeth/picomachine)
![license](https://badgen.net/npm/license/@ersbeth/picomachine)
![size](https://badgen.net/bundlephobia/minzip/@ersbeth/picomachine@0.1.1)

**Picomachine** is a lightweight yet powerful library for building [finite state machines](https://ersbeth-web.gitlab.io/picomachine/concepts/statemachine) and [statecharts](https://ersbeth-web.gitlab.io/picomachine/concepts/statechart) in TypeScript. It provides a minimal, developer-friendly API that enables you to model complex application logic in a **predictable** and **maintainable** way.

## Documentation

See the [PicoMachine website](https://ersbeth-web.gitlab.io/picomachine/) for more information.

## Overview

PicoMachine defines the behavior of your application using three core concepts:
- **States:** Represent distinct conditions or modes of your application. Each state can have its own entry and exit actions.
- **Events:** Serve as triggers (such as user actions or system signals) that prompt the state machine to evaluate transitions.
- **Transitions:** Define how the machine moves from one state to another when a specific event occurs, optionally using guard conditions and actions.

By combining these elements, PicoMachine helps you implement robust state management with features like nested states, parallel states, asynchronous operations, and more—all while keeping the API simple and efficient.

## Key Features

- **Hooks for Side Effects:** Execute custom actions when entering, exiting, or transitioning between states.
- **Nested States:** Organize your state logic into hierarchies for better structure and maintainability.
- **Parallel States:** Run multiple state machines concurrently.
- **Transient States:** Automatically trigger transitions upon state entry.
- **Multiple Transitions:** Support several transitions for a single event.
- **Guarded Transitions:** Conditionally execute transitions based on runtime checks.
- **Delayed Transitions:** Postpone transitions by a specified duration.
- **Async (Promise-Based) States:** Handle asynchronous operations effortlessly.
- **History:** Remember the last active substate when re-entering a nested state.
- **Child/Parent Communication:** Allow states to communicate with their parent or sibling states.
- **Composition:** Easily split, compose, and reuse parts of your state machines.

Embrace simplicity and reliability with PicoMachine—your state management solution for modern TypeScript applications.
