import { Observable, Signal } from '@ersbeth/picosignal';

function toStrict(config) {
  switch (config.type) {
    case "atom":
      return toStrictAtomic(config);
    case "async":
      return toStrictAsync(config);
    case "external":
      return toStrictExternal(config);
    case "nested":
      return toStrictNested(config);
    case "parallel":
      return toStrictParallel(config);
    case "error":
      return toStrictError(config);
    case "done":
      return toStrictFinal(config);
    default:
      throw new Error(`Unexpected object: ${config}`);
  }
}
function toStrictAtomic(config) {
  if (config.type !== "atom") throw new Error("Invalid config");
  return {
    type: config.type,
    ...toStrictBase(config)
  };
}
function toStrictAsync(config) {
  if (config.type !== "async") throw new Error("Invalid config");
  if (!config.activity) throw new Error("Invalid config");
  return {
    type: config.type,
    activity: config.activity,
    ...toStrictBase(config),
    ...toStrictActor(config)
  };
}
function toStrictExternal(config) {
  if (config.type !== "external") throw new Error("Invalid config");
  if (!config.source) throw new Error("Invalid config");
  const external = {
    ...toStrictBase(config),
    ...toStrictActor(config)
  };
  const internal = toStrict(config.source);
  internal.enter = [...external.enter, ...internal.enter];
  internal.exit = [...internal.exit, ...external.exit];
  internal.always = [...internal.always ?? [], ...external.always];
  internal.done = [...internal.done ?? [], ...external.done];
  internal.error = [...internal.error ?? [], ...external.error];
  internal.events = {
    ...internal.events,
    ...external.events
  };
  return internal;
}
function toStrictParallel(config) {
  if (config.type !== "parallel") throw new Error("Invalid config");
  return {
    type: config.type,
    ...toStrictBase(config),
    ...toStrictCompound(config),
    ...toStrictActor(config)
  };
}
function toStrictNested(config) {
  if (config.type !== "nested") throw new Error("Invalid config");
  if (!config.initial) throw new Error("Invalid config");
  return {
    type: config.type,
    initial: config.initial,
    history: typeof config.history === "function" ? config.history : config.history ? () => true : () => false,
    ...toStrictBase(config),
    ...toStrictCompound(config),
    ...toStrictActor(config)
  };
}
function toStrictFinal(config) {
  if (config.type !== "done") throw new Error("Invalid config");
  return {
    type: config.type,
    ...toStrictTerminal(config)
  };
}
function toStrictError(config) {
  if (config.type !== "error") throw new Error("Invalid config");
  return {
    type: config.type,
    ...toStrictTerminal(config)
  };
}
function toStrictBase(config) {
  return {
    name: config.name,
    enter: toStrictActions(config.enter),
    exit: toStrictActions(config.exit),
    always: toStrictTransitions(config.always),
    events: toStrictEvents(config.events)
  };
}
function toStrictTerminal(config) {
  if (config.type !== "done" && config.type !== "error")
    throw new Error("Invalid config");
  return {
    name: config.name,
    enter: toStrictActions(config.enter),
    exit: toStrictActions(config.exit)
  };
}
function toStrictActor(config) {
  return {
    done: toStrictTransitions(config.done),
    error: toStrictTransitions(config.error)
  };
}
function toStrictCompound(config) {
  if (config.type !== "nested" && config.type !== "parallel")
    throw new Error("Invalid config");
  if (!config.states) throw new Error("Invalid config");
  const strictConfig = {};
  Object.keys(config.states).forEach((stateName) => {
    strictConfig[stateName] = toStrict(config.states[stateName]);
  });
  return { states: strictConfig };
}
function toStrictActions(config) {
  return config ? config instanceof Array ? config : [config] : [];
}
function toStrictTransition(config) {
  return {
    target: config.target,
    guard: config.guard ?? null,
    delay: config.delay ?? 0,
    distribute: config.distribute ?? false,
    actions: toStrictActions(config.actions)
  };
}
function toStrictTransitions(config) {
  return config ? Array.isArray(config) ? config.map((cf) => toStrictTransition(cf)) : [toStrictTransition(config)] : [];
}
function toStrictEvents(config) {
  if (!config) return {};
  const strictConfig = {};
  Object.keys(config).forEach((key) => {
    strictConfig[key] = toStrictTransitions(config[key]);
  });
  return strictConfig;
}

function isTrigger(data) {
  return typeof data === "string";
}

class AsyncState {
  base;
  machine;
  execution;
  action;
  transition;
  actor;
  asynch;
  constructor(name, parent, activity) {
    this.base = new BaseMixin(this, name, parent);
    this.action = new ActionMixin();
    this.transition = new TransitionMixin();
    this.actor = new ActorMixin();
    this.asynch = new AsyncMixin(activity);
    this.execution = new ExecutionMixin(this);
    this.machine = new MachineMixin(this);
  }
}

class ParallelState {
  base;
  machine;
  execution;
  action;
  transition;
  actor;
  compound;
  parallel;
  constructor(name, parent) {
    this.base = new BaseMixin(this, name, parent);
    this.action = new ActionMixin();
    this.transition = new TransitionMixin();
    this.actor = new ActorMixin();
    this.compound = new CompoundMixin();
    this.parallel = new ParallelMixin(this);
    this.execution = new ExecutionMixin(this);
    this.machine = new MachineMixin(this);
  }
}

class AtomicState {
  base;
  machine;
  execution;
  action;
  transition;
  isAtomic = true;
  constructor(name, parent) {
    this.base = new BaseMixin(this, name, parent);
    this.action = new ActionMixin();
    this.transition = new TransitionMixin();
    this.execution = new ExecutionMixin(this);
    this.machine = new MachineMixin(this);
  }
}

class ErrorState {
  base;
  machine;
  execution;
  action;
  isError = true;
  constructor(name, parent) {
    this.base = new BaseMixin(this, name, parent);
    this.action = new ActionMixin();
    this.execution = new ExecutionMixin(this);
    this.machine = new MachineMixin(this);
  }
}

class DoneState {
  base;
  machine;
  execution;
  action;
  isFinal = true;
  constructor(name, parent) {
    this.base = new BaseMixin(this, name, parent);
    this.action = new ActionMixin();
    this.execution = new ExecutionMixin(this);
    this.machine = new MachineMixin(this);
  }
}

class NestedState {
  base;
  machine;
  execution;
  action;
  transition;
  actor;
  compound;
  nested;
  constructor(name, parent, historyGuard) {
    this.base = new BaseMixin(this, name, parent);
    this.action = new ActionMixin();
    this.transition = new TransitionMixin();
    this.actor = new ActorMixin();
    this.compound = new CompoundMixin();
    this.nested = new NestedMixin(this, historyGuard);
    this.execution = new ExecutionMixin(this);
    this.machine = new MachineMixin(this);
  }
}

function isCompoundState(state) {
  return state.compound !== undefined;
}

function isActorState(state) {
  return state.actor !== undefined;
}

function isFinalState(state) {
  return !Object.hasOwn(state, "transition");
}

class BaseMixin {
  #state;
  #name;
  #parent;
  constructor(state, name, parent) {
    this.#state = state;
    this.#name = name;
    this.#parent = parent;
  }
  get parent() {
    return this.#parent;
  }
  set parent(parent) {
    this.#parent = parent;
  }
  // for external state
  get name() {
    return this.#name;
  }
  get active() {
    if (!this.parent) return true;
    if (this.parent instanceof NestedState)
      return this.parent.nested.current === this.#state && this.parent.base.active;
    if (this.parent.parallel) return this.parent.base.active;
    throw "Internal error: parent should be either AndNode or OrNode";
  }
  get root() {
    if (this.#parent == null) return this.#state;
    return this.#parent.base.root;
  }
}

class ActionMixin {
  #enterActions = [];
  #exitActions = [];
  get enterActions() {
    return this.#enterActions;
  }
  get exitActions() {
    return this.#exitActions;
  }
  addEnterActions(enterActions) {
    this.#enterActions = [...enterActions, ...this.#enterActions];
  }
  addExitActions(exitActions) {
    this.#exitActions = [...this.#exitActions, ...exitActions];
  }
  execEnterActions() {
    this.enterActions.forEach((action) => action());
  }
  execExitActions() {
    this.exitActions.forEach((action) => action());
  }
}

class AsyncMixin {
  #activity;
  constructor(activity) {
    this.#activity = activity;
  }
  get activity() {
    return this.#activity;
  }
}

class ActorMixin {
  constructor() {
    this.done.onChanged((value) => {
      if (value) this.onDone.emit();
    });
    this.error.onChanged((value) => {
      if (value) this.onError.emit();
    });
  }
  #errorTransitions = [];
  #doneTransitions = [];
  done = new Observable(false);
  error = new Observable(false);
  onError = new Signal();
  onDone = new Signal();
  get doneTransitions() {
    return this.#doneTransitions;
  }
  get errorTransitions() {
    return this.#errorTransitions;
  }
  addErrorTransitions(errorTransitions) {
    this.#errorTransitions = [
      ...this.#errorTransitions,
      ...errorTransitions
    ];
  }
  addDoneTransitions(doneTransitions) {
    this.#doneTransitions = [...this.#doneTransitions, ...doneTransitions];
  }
}

class CompoundMixin {
  #nodes = [];
  nodesUpdated = new Signal();
  get nodes() {
    return this.#nodes;
  }
  set nodes(nodes) {
    this.#nodes = nodes;
    this.nodesUpdated.emit();
  }
}

class BaseTransition {
  guard;
  actions;
  delay;
  constructor(guard, actions, delay) {
    this.guard = guard;
    this.actions = actions;
    this.delay = delay;
  }
}
class InternalTransition extends BaseTransition {
}
class ExternalTransition extends BaseTransition {
  target;
  constructor(target, guard, actions, delay) {
    super(guard, actions, delay);
    this.target = target;
  }
}

class ExecutionMixin {
  #state;
  #pendingTransitionIds = [];
  changed = new Signal();
  constructor(state) {
    this.#state = state;
  }
  enter() {
    if (isActorState(this.#state)) {
      this.#state.actor.done.value = false;
      this.#state.actor.error.value = false;
    }
    this.#state.action.execEnterActions();
    if (this.#state.base.parent && this.#state.base.parent instanceof NestedState) {
      this.#state.base.parent.nested.current = this.#state;
    }
    this.changed.emit();
    if (!isFinalState(this.#state)) {
      this.#execTransitions(this.#state.transition.autoTransitions, {});
    }
    if (this.#state instanceof NestedState) {
      if (this.#state.base.parent && this.#state.base.parent instanceof NestedState && this.#state.base.parent.nested.current !== this.#state)
        return;
      if (!this.#state.nested.historyGuard() || !this.#state.nested.history)
        this.#state.nested.current = this.#state.nested.initial;
      else this.#state.nested.current = this.#state.nested.history;
      if (this.#state.nested.current)
        this.#state.nested.current.execution.enter();
    }
    if (this.#state instanceof ParallelState) {
      this.#state.compound.nodes.forEach(
        (node) => node.execution.enter()
      );
    }
    if (this.#state instanceof AsyncState) {
      this.#state.asynch.activity().then((value) => {
        if (this.#state.base.active)
          this.execCommand("done", value);
      }).catch((value) => {
        if (this.#state.base.active)
          this.execCommand("error", value);
      });
    }
    if (this.#state instanceof ErrorState) {
      if (this.#state.base.parent)
        this.#state.base.parent.execution.execCommand(
          "error",
          this.#state.base.name
        );
    }
    if (this.#state instanceof DoneState) {
      if (this.#state.base.parent) {
        if (this.#state.base.parent instanceof NestedState)
          this.#state.base.parent.nested.resetHistory();
        this.#state.base.parent.execution.execCommand(
          "done",
          this.#state.base.name
        );
      }
    }
  }
  exit() {
    if (this.#state instanceof ParallelState) {
      this.#state.compound.nodes.forEach((node) => node.execution.exit());
    }
    if (this.#state instanceof NestedState) {
      if (this.#state.nested.current)
        this.#state.nested.current.execution.exit();
    }
    if (!isFinalState(this.#state)) this.#cancelPendingTransitions();
    this.#state.action.execExitActions();
    if (this.#state instanceof NestedState) {
      this.#state.nested.clearCurrent();
    }
  }
  execCommand(event, data) {
    let accepted = (() => {
      if (isActorState(this.#state)) {
        switch (event) {
          case "done":
            this.#state.actor.done.value = true;
            return this.#execTransitions(
              this.#state.actor.doneTransitions,
              data
            );
          case "error":
            this.#state.actor.error.value = true;
            return this.#execTransitions(
              this.#state.actor.errorTransitions,
              data
            );
        }
      }
      if (!isFinalState(this.#state)) {
        const accepted2 = this.#execTransitions(
          this.#state.transition.eventsTransitions[event],
          data
        );
        return accepted2;
      }
      return false;
    })();
    if (this.#state instanceof NestedState) {
      if (!accepted) {
        if (!this.#state.nested.current)
          throw "[ExecutionMixin] nested.current is null";
        accepted = this.#state.nested.current.execution.execCommand(
          event,
          data
        );
      }
    }
    if (this.#state instanceof ParallelState) {
      if (!accepted) {
        accepted = this.#state.compound.nodes.map((node) => node.execution.execCommand(event, data)).reduce((acc, accepted2) => acc || accepted2);
      }
    }
    return accepted;
  }
  raise(event, data) {
    this.#state.base.root.machine.send(event, data);
  }
  #execTransitions(transitions, data) {
    if (!isFinalState(this.#state)) {
      const transition = this.#state.transition.findTransition(
        transitions,
        data
      );
      if (!transition) return false;
      if (!transition.delay) {
        this.#execTransition(transition, data);
      } else {
        this.#execDelayTransition(transition, data);
      }
      return true;
    }
    return false;
  }
  #execTransition(transition, data) {
    if (!isFinalState(this.#state)) {
      if (transition instanceof InternalTransition) {
        this.#state.transition.execTransitionActions(transition, data);
      }
      if (transition instanceof ExternalTransition) {
        this.exit();
        this.#state.transition.execTransitionActions(transition, data);
        const raiseAction = transition.actions.find(
          (action) => action.isRaise
        );
        if (!this.#state.base.parent) {
          this.#state.execution.enter();
        } else if (this.#state.base.parent && this.#state.base.parent instanceof NestedState) {
          if (raiseAction) {
            this.#state.base.parent.nested.clearCurrent();
          } else {
            this.#state.base.parent.nested.current = transition.target;
            transition.target.execution.enter();
          }
        }
      }
    }
  }
  #execDelayTransition(transition, data) {
    const pendingId = setTimeout(() => {
      this.#execTransition(transition, data);
    }, transition.delay);
    this.#pendingTransitionIds.push(pendingId);
  }
  #cancelPendingTransitions() {
    while (this.#pendingTransitionIds.length > 0) {
      const pendingId = this.#pendingTransitionIds.pop();
      clearTimeout(pendingId);
    }
  }
}

class Status {
  /**
   * The current state(s) of the state machine.
   *
   * @remarks
   * This property stores one or more state identifiers as strings.
   */
  states;
  constructor(states) {
    this.states = states;
  }
  /**
   * Determines whether any of the current state strings match the provided state descriptor.
   *
   * @param stateDescriptor - A string or regular expression used to test the current state(s).
   * @returns `true` if at least one state in the `states` array matches the descriptor; otherwise, `false`.
   *
   * @remarks
   * The method converts the provided `stateDescriptor` into a regular expression (if it is not already one)
   * and then tests each state string in the `states` array. If any state matches the regular expression,
   * the method returns `true`.
   */
  matches(stateDescriptor) {
    const regExp = new RegExp(stateDescriptor);
    return this.states.some((stateString) => regExp.test(stateString));
  }
}

class MachineMixin {
  status = new Observable(new Status([]));
  #state;
  #transitionning = false;
  #commandStack = [];
  constructor(state) {
    this.#state = state;
    this.status.value = new Status(this.#status);
    this.#state.execution.changed.on(() => {
      this.status.value = new Status(this.#status);
    });
  }
  get #status() {
    if (!isCompoundState(this.#state)) return [this.#state.base.name];
    if (this.#state instanceof ParallelState)
      return this.#state.compound.nodes.flatMap((node) => node.machine.#status).map((stateName) => `${this.#state.base.name}.${stateName}`);
    return !this.#state.nested.current ? [this.#state.base.name] : this.#state.nested.current.machine.#status.map(
      (stateName) => `${this.#state.base.name}.${stateName}`
    );
  }
  start() {
    this.#state.execution.enter();
  }
  send(event, data = null) {
    if (!this.#transitionning) {
      this.#transitionning = true;
      this.#state.execution.execCommand(event, data);
      while (this.#commandStack.length > 0) {
        const command = this.#commandStack.shift();
        if (!command) throw "command is null";
        this.#state.execution.execCommand(command.event, command.data);
      }
      this.#transitionning = false;
    } else {
      this.#commandStack.push({ event, data });
    }
  }
  onError(callback) {
    if (isActorState(this.#state))
      return this.#state.actor.onError.on(callback);
    throw "OnError must be implemented in subclass";
  }
  onDone(callback) {
    if (isActorState(this.#state))
      return this.#state.actor.onDone.on(callback);
    throw "OnDone must be implemented in subclass";
  }
  wait(stateDescriptor, timeout = 1e3) {
    return new Promise((resolve, reject) => {
      if (this.status.value.matches(stateDescriptor)) {
        resolve();
      } else {
        const timeoutID = setTimeout(() => {
          reject(
            `couldn't reach ${stateDescriptor} under ${timeout}ms`
          );
        }, timeout);
        const unbind = {};
        unbind.func = this.status.onChanged(() => {
          if (this.status.value.matches(stateDescriptor)) {
            clearTimeout(timeoutID);
            if (!unbind.func) throw "unbind.func is null";
            unbind.func();
            resolve();
          }
        });
      }
    });
  }
}

class NestedMixin {
  #state;
  #initial = null;
  #current = null;
  #history = null;
  #historyGuard;
  constructor(state, historyGuard) {
    this.#state = state;
    this.#historyGuard = historyGuard;
    this.forwardChanges();
    this.#state.compound.nodesUpdated.on(() => this.forwardChanges());
  }
  get initial() {
    return this.#initial;
  }
  set initial(initial) {
    this.#initial = initial;
  }
  get history() {
    return this.#history;
  }
  get current() {
    return this.#current;
  }
  set current(current) {
    this.#current = current;
    this.#history = current;
  }
  get historyGuard() {
    return this.#historyGuard;
  }
  forwardChanges() {
    this.#state.compound.nodes.forEach(
      (node) => node.execution.changed.on(
        () => this.#state.execution.changed.emit()
      )
    );
  }
  resetHistory() {
    this.#history = null;
  }
  clearCurrent() {
    this.#current = null;
  }
}

class ParallelMixin {
  #state;
  #actorNodes = [];
  constructor(state) {
    this.#state = state;
    this.forwardChanges();
    this.#state.compound.nodesUpdated.on(() => this.forwardChanges());
  }
  forwardChanges() {
    this.#actorNodes = this.#state.compound.nodes.filter(
      (node) => isActorState(node)
    );
    this.#state.compound.nodes.forEach(
      (node) => node.execution.changed.on(
        () => this.#state.execution.changed.emit()
      )
    );
    this.#actorNodes.forEach((node) => {
      if (!isActorState(node))
        throw "Internal error: node should have actor";
      node.actor.done.onChanged(() => this.processDone());
    });
    this.#actorNodes.forEach((node) => {
      if (!isActorState(node))
        throw "Internal error: node should have actor";
      node.actor.error.onChanged(() => this.processError());
    });
  }
  processDone() {
    const done = this.#actorNodes.reduce(
      (acc, node) => acc && (node.actor.done.value ?? false),
      true
    );
    if (done) {
      this.#state.actor.done.value = true;
      this.#state.execution.execCommand("done", {});
    }
  }
  processError() {
    const error = this.#actorNodes.reduce(
      (acc, node) => acc || (node.actor.error.value ?? false),
      true
    );
    if (error) {
      this.#state.actor.error.value = true;
      this.#state.execution.execCommand("error", {});
    }
  }
}

class TransitionMixin {
  #autoTransitions = [];
  #eventsTransitions = {};
  get autoTransitions() {
    return this.#autoTransitions;
  }
  get eventsTransitions() {
    return this.#eventsTransitions;
  }
  addAutoTransitions(autoTransitions) {
    this.#autoTransitions = [...this.#autoTransitions, ...autoTransitions];
  }
  addEventTransitions(eventsTransitions) {
    this.#eventsTransitions = {
      ...this.#eventsTransitions,
      ...eventsTransitions
    };
  }
  findTransition(transitions, data) {
    if (!transitions) return null;
    return transitions.find(
      (transition) => !transition.guard || transition.guard(data)
    );
  }
  execTransitionActions(transition, data) {
    transition.actions.forEach((action) => action(data));
  }
}

function assertNever(x) {
  throw new Error(`Unexpected object: ${x}`);
}

const Parser = {
  getCommands(config) {
    const commands = [];
    if (config.events) {
      Object.keys(config.events).forEach((event) => {
        const actions = config.events[event].flatMap((cfg) => cfg.actions).filter((action) => !isTrigger(action));
        const guards = config.events[event].flatMap((cfg) => cfg.guard).filter((guard) => guard != null);
        commands.push({
          event,
          actions,
          guards
        });
      });
    }
    const childCommands = config.states ? Object.keys(config.states).flatMap(
      (stateName) => Parser.getCommands(config.states[stateName])
    ) : [];
    childCommands.forEach((command) => {
      const existingCommand = commands.find(
        (c) => c.event === command.event
      );
      if (existingCommand) {
        existingCommand.actions.push(...command.actions);
        existingCommand.guards.push(...command.guards);
      } else commands.push(command);
    });
    return commands;
  },
  getState(name, parent, config) {
    let node;
    switch (config.type) {
      case "atom":
        node = new AtomicState(name, parent);
        break;
      case "error":
        node = new ErrorState(name, parent);
        break;
      case "done":
        node = new DoneState(name, parent);
        break;
      case "async":
        node = new AsyncState(name, parent, config.activity);
        break;
      case "parallel":
        node = new ParallelState(name, parent);
        break;
      case "nested":
        node = new NestedState(name, parent, config.history);
        break;
      default:
        assertNever(config);
    }
    node.action.addEnterActions(Parser.getActions(config.enter));
    node.action.addExitActions(Parser.getActions(config.exit));
    if (config.type === "nested") {
      const childrenEventConfig = {};
      Object.keys(config.events).forEach((event) => {
        const transitions = config.events[event];
        const forChildren = transitions.filter(
          (transition) => transition.distribute
        );
        const forCurrent = transitions.filter(
          (transition) => !transition.distribute
        );
        if (forChildren.length > 0) {
          childrenEventConfig[event] = forChildren;
        }
        if (forCurrent.length > 0) {
          config.events[event] = forCurrent;
        } else {
          delete config.events[event];
        }
      });
      Object.keys(config.states).forEach((state) => {
        const childConfig = config.states[state];
        if (childConfig.type !== "error" && childConfig.type !== "done") {
          Object.keys(childrenEventConfig).forEach((event) => {
            const transitions = childrenEventConfig[event].filter(
              (transition) => transition.target !== state
            );
            if (childConfig.events[event])
              childConfig.events[event].unshift(...transitions);
            else childConfig.events[event] = transitions;
          });
        }
      });
    }
    if (parent == null) {
      Parser.applyTransitions(node, config, [node]);
    }
    if (config.type === "nested" || config.type === "parallel") {
      const children = Object.keys(config.states).map(
        (name2) => Parser.getState(
          name2,
          node,
          config.states[name2]
        )
      );
      node.compound.nodes = children;
      children.forEach((child) => {
        const siblings = config.type === "nested" ? children : [child];
        Parser.applyTransitions(
          child,
          config.states[child.base.name],
          siblings
        );
      });
      if (config.type === "nested")
        node.nested.initial = Parser.getInitial(
          config.initial,
          children
        );
    }
    return node;
  },
  applyTransitions(node, config, siblings) {
    if (!isFinalState(node) && config.always)
      node.transition.addAutoTransitions(
        Parser.getTransitions(config.always, node, siblings)
      );
    if (!isFinalState(node) && config.events)
      node.transition.addEventTransitions(
        Parser.getEventTransitions(config.events, node, siblings)
      );
    if (isActorState(node) && config.done)
      node.actor.addDoneTransitions(
        Parser.getTransitions(config.done, node, siblings)
      );
    if (isActorState(node) && config.error)
      node.actor.addErrorTransitions(
        Parser.getTransitions(config.error, node, siblings)
      );
  },
  getActions(config, node) {
    const actions = config.map((actionConfig) => {
      if (!isTrigger(actionConfig)) return actionConfig;
      if (!node)
        throw `Can't raise event ${actionConfig}in enter/exit actions. Put it in a transition instead.`;
      const raise = (data) => {
        if (node.base.parent == null)
          throw `Can't raise event ${actionConfig} on state ${node.base.name} without parent.`;
        node.base.parent.execution.raise(actionConfig, data);
      };
      raise.isRaise = true;
      return raise;
    });
    return actions;
  },
  getTransition(config, node, nodes) {
    if (config.target === "internal") {
      return new InternalTransition(
        config.guard,
        Parser.getActions(config.actions, node),
        config.delay
      );
    }
    if (config.target === "self") {
      return new ExternalTransition(
        node,
        config.guard,
        Parser.getActions(config.actions, node),
        config.delay
      );
    }
    const target = nodes.find((node2) => node2.base.name === config.target);
    if (!target) throw `target subnode ${config.target} is not defined`;
    if (target === node)
      throw `Use either "self" or "internal" target for self transitions`;
    return new ExternalTransition(
      target,
      config.guard,
      Parser.getActions(config.actions, node),
      config.delay
    );
  },
  getTransitions(configs, node, nodes) {
    return configs.map(
      (config) => Parser.getTransition(config, node, nodes)
    );
  },
  getEventTransitions(config, node, nodes) {
    const events = {};
    Object.keys(config).forEach((eventName) => {
      const transitionsConfig = config[eventName];
      const transitions = Parser.getTransitions(
        transitionsConfig,
        node,
        nodes
      );
      events[eventName] = transitions;
    });
    return events;
  },
  getInitial(config, nodes) {
    const node = nodes.find((node2) => node2.base.name === config);
    if (!node) throw `Initial state ${config} doesn't exist.`;
    return node;
  }
};

class StateMachine {
  /**
   * The root state of this state machine.
   *
   * @remarks
   * The root state is generated from the provided configuration and represents the
   * top-level state of the state machine.
   *
   * @private
   */
  #root;
  /**
   * The list of commands extracted from the configuration.
   *
   * @remarks
   * Commands are used internally to map events to their corresponding transitions.
   *
   * @private
   */
  #commands;
  /**
   * Constructs a new StateMachine instance.
   *
   * @param name - The name of the state machine.
   * @param config - The configuration object describing the state hierarchy.
   *
   * @remarks
   * The configuration object must extend {@link StateConfig}.
   */
  constructor(config) {
    const strictConfig = toStrict(config);
    if (!strictConfig.name) throw new Error("Root state must have a name");
    this.#root = Parser.getState(strictConfig.name, null, strictConfig);
    this.#commands = Parser.getCommands(strictConfig);
  }
  /**
   * Gets the current status of the state machine.
   */
  get status() {
    return this.#root.machine.status.value;
  }
  /**
   * Starts the state machine.
   *
   * @remarks
   * This method initiates the state machine by starting the root state's machine.
   */
  start() {
    this.#root.machine.start();
  }
  /**
   * Sends an event to the state machine.
   *
   * @param event - The event name to send.
   * @param data - The data to pass along with the event.
   *
   * @remarks
   * The event is matched against the commands extracted from the configuration.
   * If no matching command is found, an error is thrown.
   */
  send(event, data = null) {
    const command = this.#commands.filter((c) => c.event === event);
    if (command.length === 0)
      throw `StateMachine: command ${event} doesnt exist on machine ${this.#root.base.name}`;
    this.#root.machine.send(event, data);
  }
  /**
   * Registers a callback to be invoked when the status of the state machine changes.
   *
   * @param callback - A function to call when a change event is triggered.
   * @returns A handle that can be used to unsubscribe the callback.
   */
  onChange(callback) {
    return this.#root.machine.status.onChanged(callback);
  }
  /**
   * Registers a callback to be invoked when an error occurs in the state machine.
   *
   * @param callback - A function to call when an error event is triggered.
   * @returns A handle that can be used to unsubscribe the callback.
   */
  onError(callback) {
    return this.#root.machine.onError(callback);
  }
  /**
   * Registers a callback to be invoked when the state machine has completed its execution.
   *
   * @param callback - A function to call when a done event is triggered.
   * @returns A handle that can be used to unsubscribe the callback.
   */
  onDone(callback) {
    return this.#root.machine.onDone(callback);
  }
  /**
   * Waits for the state machine to reach a state matching the specified descriptor.
   *
   * @param stateDescriptor - A string or regular expression to match against the current state.
   * @param timeout - The maximum time (in milliseconds) to wait (default is 1000 ms).
   * @returns A promise that resolves when the state is reached or rejects if the timeout is exceeded.
   *
   * @remarks
   * This method provides an asynchronous mechanism to wait for a particular state to become active.
   *
   * @internal
   */
  wait(stateDescriptor, timeout = 1e3) {
    return this.#root.machine.wait(stateDescriptor, timeout);
  }
}

export { StateMachine };
