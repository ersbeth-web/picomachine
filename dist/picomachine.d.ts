/**
 * @packageDocumentation
 * Picomachine is a lightweight and full-featured state machine library for TypeScript.
 * It provides a simple yet powerful API to model complex application logic using finite state machines and statecharts.
 *
 * With Picomachine, you can easily define, compose, and manage state transitions for your applications.
 *
 * @remarks
 * Developed and maintained by Elisabeth Rousset (https://gitlab.com/ersbeth).
 *
 * For any issues or contributions, please refer to the project's repository (https://gitlab.com/ersbeth-web/picomachine).
 */

/**
 * Represents a synchronous function that performs side effects.
 *
 * @remarks
 * An action function can optionally receive some data and performs a side effect.
 *
 * @public
 */
export declare type Action = (data?: any) => void;

/**
 * Configuration for one or more actions or triggers.
 *
 * @remarks
 * An action or trigger can be specified either as a single value or as an array.
 *
 * @public
 */
export declare type ActionsConfig = Action | Trigger | (Action | Trigger)[];

/**
 * Represents an asynchronous function that performs side effects.
 *
 * @remarks
 * An activity function performs a side effect and returns a promise.
 *
 * @public
 */
export declare type Activity = () => Promise<any>;

/**
 * Recursively extracts event names from the nested states (children) of the configuration.
 *
 * If T has a `states` property (a record of nested StateConfig objects),
 * then for each key K in that record, recursively extract event names and union them.
 */
declare type ExtractChildrenEventNames<T extends StateConfig> = T extends {
    states: infer S;
} ? S extends Readonly<Record<string, StateConfig>> ? Simplify<{
    [K in keyof S]: ExtractEventNames<S[K]>;
}[keyof S]> : never : never;

/**
 * Recursively extracts state names from the children of T.
 *
 * If T has a `states` property, then for each key K in that record,
 * recursively extract state names from the nested configuration,
 * using the qualified name (computed by ExtractCurrentStateName) as the new prefix.
 *
 * @template T - The state configuration.
 * @template Prefix - The prefix to qualify the nested state's names.
 */
declare type ExtractChildrenStateNames<T extends StateConfig, Prefix extends string> = T extends {
    states: infer S;
} ? S extends Readonly<Record<string, StateConfig>> ? {
    [K in keyof S]: ExtractStateNames<S[K], ExtractCurrentStateName<K & string, Prefix>>;
}[keyof S] : never : never;

/**
 * Extracts the event keys defined on the current state configuration.
 *
 * If the configuration T has an `events` property, returns its keys; otherwise, never.
 */
declare type ExtractCurrentEventNames<T extends StateConfig> = T extends {
    events: infer E;
} ? E extends Readonly<Record<string, any>> ? keyof E : never : never;

/**
 * Computes the qualified name for a given key.
 *
 * If no prefix is provided, the qualified name is simply K.
 * Otherwise, it concatenates the prefix and K with a dot.
 *
 * @template K - The current state's key.
 * @template Prefix - The accumulated prefix (defaults to an empty string).
 */
declare type ExtractCurrentStateName<K extends string, Prefix extends string = ""> = Prefix extends "" ? K : `${Prefix}.${K}`;

/**
 * Recursively extracts event names from a state configuration.
 *
 * This type is the union of:
 * - The event keys defined on T directly,
 * - The event keys from any nested (child) states,
 * - And, if T is an external state, the event keys from its source.
 *
 * The result is wrapped in Simplify to collapse the union.
 */
declare type ExtractEventNames<T extends StateConfig> = Simplify<ExtractCurrentEventNames<T> | ExtractChildrenEventNames<T> | ExtractExternalEventNames<T>>;

/**
 * Extracts event names from an external state.
 *
 * If T is an external state (with type "external" and a `source` property),
 * then extract event names from its source machine’s configuration.
 */
declare type ExtractExternalEventNames<T extends StateConfig> = T extends {
    type: "external";
    source: infer Source;
} ? Source extends Readonly<StateConfig> ? Simplify<ExtractEventNames<Source>> : never : never;

/**
 * Extracts state names from an external state.
 *
 * If T is an external state (i.e. type "external" with a source machine),
 * then extract the state names from its source configuration,
 * using the qualified current key as the new prefix.
 *
 * @template T - The state configuration.
 * @template Prefix - The prefix to qualify the external state's names.
 */
declare type ExtractExternalStateNames<T extends StateConfig, Prefix extends string> = T extends {
    type: "external";
    source: infer Source;
} ? Source extends Readonly<StateConfig> ? ExtractStateNames<Source, Prefix> : never : never;

/**
 * Extracts fully qualified state names from a configuration.
 *
 * If the configuration has a `name` property, that name is used as the starting prefix.
 *
 * For example, if your configuration is declared as:
 *
 *    {
 *       name: "root",
 *       type: "nested",
 *       initial: "A",
 *       states: { ... }
 *    }
 *
 * then the resulting union will include "root", "root.A", "root.A.B", etc.
 */
declare type ExtractFullStateNames<T extends StateConfig> = T extends {
    name: infer N extends string;
} ? `${N}` | ExtractStateNames<T, `${N}`> : ExtractStateNames<T>;

/**
 * Recursively extracts fully qualified state names from a state machine configuration.
 *
 * For a given configuration T and an optional Prefix, this type produces a union of:
 *
 * - The qualified names for the states at the current level, and
 * - The qualified names extracted recursively from nested states.
 *
 * When a state is marked as "external", its source machine’s state names are extracted
 * and qualified with the external state’s key.
 *
 * @template T - The state machine configuration.
 * @template Prefix - The accumulated prefix (default is an empty string).
 */
declare type ExtractStateNames<T extends StateConfig, Prefix extends string = ""> = T extends {
    states: infer S;
} ? S extends Readonly<Record<string, StateConfig>> ? {
    [K in keyof S]: ExtractCurrentStateName<K & string, Prefix> | (S[K] extends {
        type: "external";
        source: any;
    } ? ExtractExternalStateNames<S[K], ExtractCurrentStateName<K & string, Prefix>> : ExtractChildrenStateNames<S[K], ExtractCurrentStateName<K & string, Prefix>>);
}[keyof S] : Prefix extends "" ? never : Prefix : Prefix extends "" ? never : Prefix;

/**
 * Represents a synchronous function that determines whether a transition should occur.
 *
 * @remarks
 * A guard function can optionally receive some data and must return a boolean.
 *
 * @public
 */
export declare type Guard = (data?: any) => boolean;

declare type Simplify<T> = {
    [K in keyof T]: T[K];
} & {};

/**
 * Configuration for a state within a state machine.
 *
 * @remarks
 * A `StateConfig` object defines a state and its behavior within a state machine. The state machine supports
 * several types of states:
 *
 * - Atomic (`"atom"`) – a simple, indivisible state.
 *
 * - Asynchronous (`"async"`) – a state that performs asynchronous operations.
 *
 * - External (`"external"`) – a state whose configuration is provided by another configuration object.
 *
 * - Parallel (`"parallel"`) – a composite state with multiple concurrent substates.
 *
 * - Nested (`"nested"`) – a composite state that contains a set of substates.
 *
 * - Terminal (`"done"` or `"error"`) – states that indicate completion or an error.
 *
 * Certain properties are valid only for specific state types:
 *
 * - The `name` property should only be defined for the root state.
 *
 * - The `done` and `error` properties are only available for `async`, `nested`, and `parallel` states.
 *
 * - The `activity` function is only available for `async` states.
 *
 * - The `initial` and `history` properties are only applicable for `nested` states.
 *
 * - The `source` property is only available for `external` states.
 *
 * - The `always` and `events` properties are forbidden on terminal states (i.e. states of type `"done"` or `"error"`).
 *
 * - The `states` property is only allowed and required for composite states (i.e. states of type `"nested"` or `"parallel"`).
 *
 * @public
 */
export declare interface StateConfig {
    /**
     * Name of the root state.
     *
     * @remarks
     * The `name` property should only be defined for the root state. Children states must not define this property.
     */
    name?: string;
    /**
     * The type of the state.
     *
     * @remarks
     * Valid values include:
     * - `"atom"`: A simple, atomic state.
     *
     * - `"async"`: A state that performs asynchronous operations.
     *
     * - `"external"`: A state whose configuration is provided by another state machine.
     *
     * - `"parallel"`: A composite state that manages multiple concurrent substates.
     *
     * - `"nested"`: A composite state that contains a set of substates.
     *
     * - `"done"`: A terminal state indicating successful completion.
     *
     * - `"error"`: A terminal state indicating an error has occurred.
     */
    type: "atom" | "async" | "external" | "parallel" | "nested" | "done" | "error";
    /**
     * Actions or triggers executed upon entering the state.
     *
     * @remarks
     * Applicable to all state types.
     */
    enter?: ActionsConfig;
    /**
     * Actions or triggers executed upon exiting the state.
     *
     * @remarks
     * Applicable to all state types.
     */
    exit?: ActionsConfig;
    /**
     *  Transition configuration(s) executed immediately after the state is entered.
     *
     * @remarks
     * The `always` property is allowed on states except terminal states (i.e. not on `"done"` or `"error"`).
     */
    always?: Readonly<TransitionConfig | TransitionConfig[]>;
    /**
     * Transition configuration(s) for when the state completes successfully.
     *
     * @remarks
     * The `done` property is only available for asynchronous, nested, and parallel states.
     */
    done?: Readonly<TransitionConfig | TransitionConfig[]>;
    /**
     * Transition configuration(s) for when the state encounters an error.
     *
     * @remarks
     * The `error` property is only available for asynchronous, nested, and parallel states.
     */
    error?: Readonly<TransitionConfig | TransitionConfig[]>;
    /**
     * A mapping of event names to transition configuration(s) defining state changes in response to events.
     *
     * @remarks
     * The `events` property is forbidden on terminal states (i.e. states of type `"done"` or `"error"`).
     */
    events?: Readonly<{
        [key: string]: TransitionConfig | TransitionConfig[];
    }>;
    /**
     * An activity function that returns a promise.
     *
     * @remarks
     * The `activity` property is only available for asynchronous (`"async"`) states.
     */
    activity?: Activity;
    /**
     * The identifier of the initial substate.
     *
     * @remarks
     * The `initial` property is only applicable for nested states.
     */
    initial?: string;
    /**
     * A flag or guard to enable history tracking.
     *
     * @remarks
     * When enabled, the state machine will resume at the last active substate instead of starting at the initial substate.
     * The `history` property is only applicable for nested states.
     */
    history?: boolean | Guard;
    /**
     * A mapping of substate identifiers to their corresponding state configurations.
     *
     * @remarks
     * This property is optional and should only be provided for composite states, namely states of type
     * `"nested"` or `"parallel"`. For atomic or terminal states, this property must not be provided.
     */
    states?: {
        [key: string]: StateConfig;
    };
    /**
     * For external states, provides the configuration from the external source.
     *
     * @remarks
     * The `source` property is only available for states of type `"external"`.
     */
    source?: StateConfig;
}

/**
 * Represents a state machine that manages a hierarchical state structure.
 *
 * @remarks
 * The StateMachine class is the primary entry point for creating and interacting with
 * a state machine. It is initialized with a name and a configuration object that describes
 * the state hierarchy. The configuration is converted to a strict format and then parsed
 * to create the root state and associated commands.
 *
 * @public
 */
export declare class StateMachine<T extends StateConfig> {
    #private;
    /**
     * Constructs a new StateMachine instance.
     *
     * @param name - The name of the state machine.
     * @param config - The configuration object describing the state hierarchy.
     *
     * @remarks
     * The configuration object must extend {@link StateConfig}.
     */
    constructor(config: T);
    /**
     * Gets the current status of the state machine.
     */
    get status(): Status<ExtractFullStateNames<T>>;
    /**
     * Starts the state machine.
     *
     * @remarks
     * This method initiates the state machine by starting the root state's machine.
     */
    start(): void;
    /**
     * Sends an event to the state machine.
     *
     * @param event - The event name to send.
     * @param data - The data to pass along with the event.
     *
     * @remarks
     * The event is matched against the commands extracted from the configuration.
     * If no matching command is found, an error is thrown.
     */
    send<E extends ExtractEventNames<T> & string>(event: E, data?: any): void;
    /**
     * Registers a callback to be invoked when the status of the state machine changes.
     *
     * @param callback - A function to call when a change event is triggered.
     * @returns A handle that can be used to unsubscribe the callback.
     */
    onChange(callback: (status: Status) => void): () => void;
    /**
     * Registers a callback to be invoked when an error occurs in the state machine.
     *
     * @param callback - A function to call when an error event is triggered.
     * @returns A handle that can be used to unsubscribe the callback.
     */
    onError(callback: () => void): () => void;
    /**
     * Registers a callback to be invoked when the state machine has completed its execution.
     *
     * @param callback - A function to call when a done event is triggered.
     * @returns A handle that can be used to unsubscribe the callback.
     */
    onDone(callback: () => void): () => void;
    /**
     * Waits for the state machine to reach a state matching the specified descriptor.
     *
     * @param stateDescriptor - A string or regular expression to match against the current state.
     * @param timeout - The maximum time (in milliseconds) to wait (default is 1000 ms).
     * @returns A promise that resolves when the state is reached or rejects if the timeout is exceeded.
     *
     * @remarks
     * This method provides an asynchronous mechanism to wait for a particular state to become active.
     *
     * @internal
     */
    wait(stateDescriptor: string | RegExp, timeout?: number): Promise<void>;
}

/**
 * Represents the current status of a state machine.
 *
 * @remarks
 * The `Status` class exposes the active state(s) of a state machine using a simple array of strings.
 * This class provides utility methods for inspecting the status, such as checking whether the current state(s)
 * match a specified descriptor.
 *
 * @public
 */
export declare class Status<AvailableState extends string = string> {
    /**
     * The current state(s) of the state machine.
     *
     * @remarks
     * This property stores one or more state identifiers as strings.
     */
    readonly states: AvailableState[];
    constructor(states: AvailableState[]);
    /**
     * Determines whether any of the current state strings match the provided state descriptor.
     *
     * @param stateDescriptor - A string or regular expression used to test the current state(s).
     * @returns `true` if at least one state in the `states` array matches the descriptor; otherwise, `false`.
     *
     * @remarks
     * The method converts the provided `stateDescriptor` into a regular expression (if it is not already one)
     * and then tests each state string in the `states` array. If any state matches the regular expression,
     * the method returns `true`.
     */
    matches(stateDescriptor: AvailableState | RegExp): boolean;
}

/**
 * Configuration for a transition between states.
 *
 * @remarks
 * A `TransitionConfig` defines how the state machine transitions from one state to another.
 * It specifies the target state identifier and can optionally include:
 *
 * - A guard function (`guard`) that must return `true` for the transition to occur.
 *
 * - A delay (`delay`) in milliseconds before the transition is executed.
 *
 * - One or more actions (or a trigger) (`actions`) that are executed during the transition.
 *
 * - A flag (`distribute`) indicating whether the transition should be distributed across all children states.
 *
 * @public
 */
export declare interface TransitionConfig {
    /**
     * The identifier of the target state.
     */
    target: string;
    /**
     * A guard function to determine whether the transition should occur.
     *
     * @remarks
     * The transition is executed only if the guard returns `true`.
     */
    guard?: Guard;
    /**
     * An optional delay (in milliseconds) before executing the transition.
     */
    delay?: number;
    /**
     * One or more actions or triggers to execute during the transition.
     */
    actions?: ActionsConfig;
    /**
     * Indicates whether the transition should be distributed across all children states.
     */
    distribute?: boolean;
}

/**
 * Represents a trigger event.
 *
 * @remarks
 * A trigger is simply a string that identifies an event name.
 *
 * @public
 */
export declare type Trigger = string;

export { }
