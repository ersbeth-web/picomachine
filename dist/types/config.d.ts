import { Action, Activity, Guard, Trigger } from './callbacks';
/**
 * Configuration for one or more actions or triggers.
 *
 * @remarks
 * An action or trigger can be specified either as a single value or as an array.
 *
 * @public
 */
export type ActionsConfig = Action | Trigger | (Action | Trigger)[];
/**
 * Configuration for a transition between states.
 *
 * @remarks
 * A `TransitionConfig` defines how the state machine transitions from one state to another.
 * It specifies the target state identifier and can optionally include:
 *
 * - A guard function (`guard`) that must return `true` for the transition to occur.
 *
 * - A delay (`delay`) in milliseconds before the transition is executed.
 *
 * - One or more actions (or a trigger) (`actions`) that are executed during the transition.
 *
 * - A flag (`distribute`) indicating whether the transition should be distributed across all children states.
 *
 * @public
 */
export interface TransitionConfig {
    /**
     * The identifier of the target state.
     */
    target: string;
    /**
     * A guard function to determine whether the transition should occur.
     *
     * @remarks
     * The transition is executed only if the guard returns `true`.
     */
    guard?: Guard;
    /**
     * An optional delay (in milliseconds) before executing the transition.
     */
    delay?: number;
    /**
     * One or more actions or triggers to execute during the transition.
     */
    actions?: ActionsConfig;
    /**
     * Indicates whether the transition should be distributed across all children states.
     */
    distribute?: boolean;
}
/**
 * Configuration for a state within a state machine.
 *
 * @remarks
 * A `StateConfig` object defines a state and its behavior within a state machine. The state machine supports
 * several types of states:
 *
 * - Atomic (`"atom"`) – a simple, indivisible state.
 *
 * - Asynchronous (`"async"`) – a state that performs asynchronous operations.
 *
 * - External (`"external"`) – a state whose configuration is provided by another configuration object.
 *
 * - Parallel (`"parallel"`) – a composite state with multiple concurrent substates.
 *
 * - Nested (`"nested"`) – a composite state that contains a set of substates.
 *
 * - Terminal (`"done"` or `"error"`) – states that indicate completion or an error.
 *
 * Certain properties are valid only for specific state types:
 *
 * - The `name` property should only be defined for the root state.
 *
 * - The `done` and `error` properties are only available for `async`, `nested`, and `parallel` states.
 *
 * - The `activity` function is only available for `async` states.
 *
 * - The `initial` and `history` properties are only applicable for `nested` states.
 *
 * - The `source` property is only available for `external` states.
 *
 * - The `always` and `events` properties are forbidden on terminal states (i.e. states of type `"done"` or `"error"`).
 *
 * - The `states` property is only allowed and required for composite states (i.e. states of type `"nested"` or `"parallel"`).
 *
 * @public
 */
export interface StateConfig {
    /**
     * Name of the root state.
     *
     * @remarks
     * The `name` property should only be defined for the root state. Children states must not define this property.
     */
    name?: string;
    /**
     * The type of the state.
     *
     * @remarks
     * Valid values include:
     * - `"atom"`: A simple, atomic state.
     *
     * - `"async"`: A state that performs asynchronous operations.
     *
     * - `"external"`: A state whose configuration is provided by another state machine.
     *
     * - `"parallel"`: A composite state that manages multiple concurrent substates.
     *
     * - `"nested"`: A composite state that contains a set of substates.
     *
     * - `"done"`: A terminal state indicating successful completion.
     *
     * - `"error"`: A terminal state indicating an error has occurred.
     */
    type: "atom" | "async" | "external" | "parallel" | "nested" | "done" | "error";
    /**
     * Actions or triggers executed upon entering the state.
     *
     * @remarks
     * Applicable to all state types.
     */
    enter?: ActionsConfig;
    /**
     * Actions or triggers executed upon exiting the state.
     *
     * @remarks
     * Applicable to all state types.
     */
    exit?: ActionsConfig;
    /**
     *  Transition configuration(s) executed immediately after the state is entered.
     *
     * @remarks
     * The `always` property is allowed on states except terminal states (i.e. not on `"done"` or `"error"`).
     */
    always?: Readonly<TransitionConfig | TransitionConfig[]>;
    /**
     * Transition configuration(s) for when the state completes successfully.
     *
     * @remarks
     * The `done` property is only available for asynchronous, nested, and parallel states.
     */
    done?: Readonly<TransitionConfig | TransitionConfig[]>;
    /**
     * Transition configuration(s) for when the state encounters an error.
     *
     * @remarks
     * The `error` property is only available for asynchronous, nested, and parallel states.
     */
    error?: Readonly<TransitionConfig | TransitionConfig[]>;
    /**
     * A mapping of event names to transition configuration(s) defining state changes in response to events.
     *
     * @remarks
     * The `events` property is forbidden on terminal states (i.e. states of type `"done"` or `"error"`).
     */
    events?: Readonly<{
        [key: string]: TransitionConfig | TransitionConfig[];
    }>;
    /**
     * An activity function that returns a promise.
     *
     * @remarks
     * The `activity` property is only available for asynchronous (`"async"`) states.
     */
    activity?: Activity;
    /**
     * The identifier of the initial substate.
     *
     * @remarks
     * The `initial` property is only applicable for nested states.
     */
    initial?: string;
    /**
     * A flag or guard to enable history tracking.
     *
     * @remarks
     * When enabled, the state machine will resume at the last active substate instead of starting at the initial substate.
     * The `history` property is only applicable for nested states.
     */
    history?: boolean | Guard;
    /**
     * A mapping of substate identifiers to their corresponding state configurations.
     *
     * @remarks
     * This property is optional and should only be provided for composite states, namely states of type
     * `"nested"` or `"parallel"`. For atomic or terminal states, this property must not be provided.
     */
    states?: {
        [key: string]: StateConfig;
    };
    /**
     * For external states, provides the configuration from the external source.
     *
     * @remarks
     * The `source` property is only available for states of type `"external"`.
     */
    source?: StateConfig;
}
//# sourceMappingURL=config.d.ts.map