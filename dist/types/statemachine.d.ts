import { StateConfig } from './config';
import { Status } from './status';
import { ExtractEventNames, ExtractFullStateNames } from './typeExtractors';
/**
 * Represents a state machine that manages a hierarchical state structure.
 *
 * @remarks
 * The StateMachine class is the primary entry point for creating and interacting with
 * a state machine. It is initialized with a name and a configuration object that describes
 * the state hierarchy. The configuration is converted to a strict format and then parsed
 * to create the root state and associated commands.
 *
 * @public
 */
export declare class StateMachine<T extends StateConfig> {
    #private;
    /**
     * Constructs a new StateMachine instance.
     *
     * @param name - The name of the state machine.
     * @param config - The configuration object describing the state hierarchy.
     *
     * @remarks
     * The configuration object must extend {@link StateConfig}.
     */
    constructor(config: T);
    /**
     * Gets the current status of the state machine.
     */
    get status(): Status<ExtractFullStateNames<T>>;
    /**
     * Starts the state machine.
     *
     * @remarks
     * This method initiates the state machine by starting the root state's machine.
     */
    start(): void;
    /**
     * Sends an event to the state machine.
     *
     * @param event - The event name to send.
     * @param data - The data to pass along with the event.
     *
     * @remarks
     * The event is matched against the commands extracted from the configuration.
     * If no matching command is found, an error is thrown.
     */
    send<E extends ExtractEventNames<T> & string>(event: E, data?: any): void;
    /**
     * Registers a callback to be invoked when the status of the state machine changes.
     *
     * @param callback - A function to call when a change event is triggered.
     * @returns A handle that can be used to unsubscribe the callback.
     */
    onChange(callback: (status: Status) => void): () => void;
    /**
     * Registers a callback to be invoked when an error occurs in the state machine.
     *
     * @param callback - A function to call when an error event is triggered.
     * @returns A handle that can be used to unsubscribe the callback.
     */
    onError(callback: () => void): () => void;
    /**
     * Registers a callback to be invoked when the state machine has completed its execution.
     *
     * @param callback - A function to call when a done event is triggered.
     * @returns A handle that can be used to unsubscribe the callback.
     */
    onDone(callback: () => void): () => void;
    /**
     * Waits for the state machine to reach a state matching the specified descriptor.
     *
     * @param stateDescriptor - A string or regular expression to match against the current state.
     * @param timeout - The maximum time (in milliseconds) to wait (default is 1000 ms).
     * @returns A promise that resolves when the state is reached or rejects if the timeout is exceeded.
     *
     * @remarks
     * This method provides an asynchronous mechanism to wait for a particular state to become active.
     *
     * @internal
     */
    wait(stateDescriptor: string | RegExp, timeout?: number): Promise<void>;
}
//# sourceMappingURL=statemachine.d.ts.map