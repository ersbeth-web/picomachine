/**
 * @packageDocumentation
 * Picomachine is a lightweight and full-featured state machine library for TypeScript.
 * It provides a simple yet powerful API to model complex application logic using finite state machines and statecharts.
 *
 * With Picomachine, you can easily define, compose, and manage state transitions for your applications.
 *
 * @remarks
 * Developed and maintained by Elisabeth Rousset (https://gitlab.com/ersbeth).
 *
 * For any issues or contributions, please refer to the project's repository (https://gitlab.com/ersbeth-web/picomachine).
 */
export { StateMachine } from './statemachine';
export type { Action, Guard, Activity, Trigger } from './callbacks';
export type { ActionsConfig, StateConfig, TransitionConfig, } from './config';
export type { Status } from './status';
//# sourceMappingURL=exports.d.ts.map