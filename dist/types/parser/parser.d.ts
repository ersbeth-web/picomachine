import { Action, Guard } from '../callbacks';
import { ExternalTransition, InternalTransition, Transition } from '../mixins';
import { CompoundState, State } from '../states';
import { StrictActionConfig, StrictEventsConfig, StrictStateConfig, StrictTransitionConfig } from './configStrict';
/**
 * Represents a command that maps an event to its corresponding actions and guards.
 *
 * @remarks
 * A `Command` is used internally by the state machine to associate an event with the
 * actions to be executed and the guard functions to be evaluated when the event is triggered.
 *
 * @public
 */
export interface Command {
    /**
     * The name of the event that triggers this command.
     */
    event: string;
    /**
     * An array of actions to execute when the event occurs.
     */
    actions: Action[];
    /**
     * An array of guard functions to evaluate when the event occurs.
     * All guards must return `true` for the command to be executed.
     */
    guards: Guard[];
}
export declare const Parser: {
    getCommands(config: StrictStateConfig): Command[];
    getState(name: string, parent: CompoundState | null, config: StrictStateConfig): State;
    applyTransitions(node: State, config: StrictStateConfig, siblings: State[]): void;
    getActions(config: StrictActionConfig, node?: State): Action[];
    getTransition(config: StrictTransitionConfig, node: State, nodes: State[]): InternalTransition | ExternalTransition;
    getTransitions(configs: StrictTransitionConfig[], node: State, nodes: State[]): (InternalTransition | ExternalTransition)[];
    getEventTransitions(config: StrictEventsConfig, node: State, nodes: State[]): {
        [key: string]: Transition[];
    };
    getInitial(config: string | undefined, nodes: State[]): State;
};
//# sourceMappingURL=parser.d.ts.map