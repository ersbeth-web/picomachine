import { Action, Activity, Guard, Trigger } from '../callbacks';
import { StateConfig } from '../config';
import { OneOf } from './utils';
export type StrictStateConfig = OneOf<[
    StrictAtomicConfig,
    StrictAsyncConfig,
    StrictParallelConfig,
    StrictNestedConfig,
    StrictDoneConfig,
    StrictErrorConfig
]>;
export declare function toStrict(config: StateConfig): StrictStateConfig;
export type StrictActionConfig = (Action | Trigger)[];
export interface StrictAtomicConfig extends StrictBaseConfig {
    type: "atom";
}
export interface StrictAsyncConfig extends StrictBaseConfig, StrictActorConfig {
    type: "async";
    activity: Activity;
}
export interface StrictParallelConfig extends StrictBaseConfig, StrictCompoundConfig, StrictActorConfig {
    type: "parallel";
}
export interface StrictNestedConfig extends StrictBaseConfig, StrictCompoundConfig, StrictActorConfig {
    type: "nested";
    initial: string;
    history: Guard;
}
export interface StrictDoneConfig extends StrictFinalConfig {
    type: "done";
}
export interface StrictErrorConfig extends StrictFinalConfig {
    type: "error";
}
export interface StrictFinalConfig {
    name?: string;
    enter: StrictActionConfig;
    exit: StrictActionConfig;
}
export interface StrictBaseConfig {
    name?: string;
    enter: StrictActionConfig;
    exit: StrictActionConfig;
    always: StrictTransitionConfig[];
    events: StrictEventsConfig;
}
export interface StrictActorConfig {
    done: StrictTransitionConfig[];
    error: StrictTransitionConfig[];
}
export interface StrictCompoundConfig {
    states: {
        [key: string]: StrictStateConfig;
    };
}
export interface StrictTransitionConfig {
    target: string;
    guard: Guard | null;
    delay: number;
    actions: StrictActionConfig;
    distribute: boolean;
}
export type StrictEventsConfig = {
    [key: string]: StrictTransitionConfig[];
};
//# sourceMappingURL=configStrict.d.ts.map