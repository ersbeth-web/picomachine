/**
 * Represents the current status of a state machine.
 *
 * @remarks
 * The `Status` class exposes the active state(s) of a state machine using a simple array of strings.
 * This class provides utility methods for inspecting the status, such as checking whether the current state(s)
 * match a specified descriptor.
 *
 * @public
 */
export declare class Status<AvailableState extends string = string> {
    /**
     * The current state(s) of the state machine.
     *
     * @remarks
     * This property stores one or more state identifiers as strings.
     */
    readonly states: AvailableState[];
    constructor(states: AvailableState[]);
    /**
     * Determines whether any of the current state strings match the provided state descriptor.
     *
     * @param stateDescriptor - A string or regular expression used to test the current state(s).
     * @returns `true` if at least one state in the `states` array matches the descriptor; otherwise, `false`.
     *
     * @remarks
     * The method converts the provided `stateDescriptor` into a regular expression (if it is not already one)
     * and then tests each state string in the `states` array. If any state matches the regular expression,
     * the method returns `true`.
     */
    matches(stateDescriptor: AvailableState | RegExp): boolean;
}
//# sourceMappingURL=status.d.ts.map