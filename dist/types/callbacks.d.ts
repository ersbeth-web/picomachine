/**
 * Represents a synchronous function that performs side effects.
 *
 * @remarks
 * An action function can optionally receive some data and performs a side effect.
 *
 * @public
 */
export type Action = (data?: any) => void;
/**
 * Represents a synchronous function that determines whether a transition should occur.
 *
 * @remarks
 * A guard function can optionally receive some data and must return a boolean.
 *
 * @public
 */
export type Guard = (data?: any) => boolean;
/**
 * Represents an asynchronous function that performs side effects.
 *
 * @remarks
 * An activity function performs a side effect and returns a promise.
 *
 * @public
 */
export type Activity = () => Promise<any>;
/**
 * Represents a trigger event.
 *
 * @remarks
 * A trigger is simply a string that identifies an event name.
 *
 * @public
 */
export type Trigger = string;
/**
 * Determines whether the provided data is a valid trigger.
 *
 * @param data - The data to test.
 * @returns `true` if the data is a string (and therefore a valid trigger), otherwise `false`.
 *
 * @private
 */
export declare function isTrigger(data: any): data is Trigger;
//# sourceMappingURL=callbacks.d.ts.map