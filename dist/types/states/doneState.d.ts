import { ActionMixin, BaseMixin, ExecutionMixin, MachineMixin } from '../mixins';
import { CompoundState } from './compoundState';
export declare class DoneState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    isFinal: boolean;
    constructor(name: string, parent: CompoundState | null);
}
//# sourceMappingURL=doneState.d.ts.map