import { ActionMixin, ActorMixin, BaseMixin, CompoundMixin, ExecutionMixin, MachineMixin, ParallelMixin, TransitionMixin } from '../mixins';
import { CompoundState } from './compoundState';
export declare class ParallelState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    transition: TransitionMixin;
    actor: ActorMixin;
    compound: CompoundMixin;
    parallel: ParallelMixin;
    constructor(name: string, parent: CompoundState | null);
}
//# sourceMappingURL=parallelState.d.ts.map