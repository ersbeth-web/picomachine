import { NestedState } from './nestedState';
import { ParallelState } from './parallelState';
import { State } from './state';
export type CompoundState = NestedState | ParallelState;
export declare function isCompoundState(state: State): state is CompoundState;
//# sourceMappingURL=compoundState.d.ts.map