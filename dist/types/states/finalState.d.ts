import { DoneState } from './doneState';
import { ErrorState } from './errorState';
import { State } from './state';
export type FinalState = DoneState | ErrorState;
export declare function isFinalState(state: State): state is DoneState | ErrorState;
//# sourceMappingURL=finalState.d.ts.map