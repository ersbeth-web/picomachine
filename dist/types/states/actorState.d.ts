import { AsyncState } from './asyncState';
import { NestedState } from './nestedState';
import { ParallelState } from './parallelState';
import { State } from './state';
export type ActorState = NestedState | AsyncState | ParallelState;
export declare function isActorState(state: State): state is ActorState;
//# sourceMappingURL=actorState.d.ts.map