import { Guard } from '../callbacks';
import { ActionMixin, ActorMixin, BaseMixin, CompoundMixin, ExecutionMixin, MachineMixin, NestedMixin, TransitionMixin } from '../mixins';
import { CompoundState } from './compoundState';
export declare class NestedState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    transition: TransitionMixin;
    actor: ActorMixin;
    compound: CompoundMixin;
    nested: NestedMixin;
    constructor(name: string, parent: CompoundState | null, historyGuard: Guard);
}
//# sourceMappingURL=nestedState.d.ts.map