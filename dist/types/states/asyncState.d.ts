import { Activity } from '../callbacks';
import { ActionMixin, ActorMixin, AsyncMixin, BaseMixin, ExecutionMixin, MachineMixin, TransitionMixin } from '../mixins';
import { CompoundState } from './compoundState';
export declare class AsyncState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    transition: TransitionMixin;
    actor: ActorMixin;
    asynch: AsyncMixin;
    constructor(name: string, parent: CompoundState | null, activity: Activity);
}
//# sourceMappingURL=asyncState.d.ts.map