import { ActionMixin, BaseMixin, ExecutionMixin, MachineMixin, TransitionMixin } from '../mixins';
import { CompoundState } from './compoundState';
export declare class AtomicState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    transition: TransitionMixin;
    isAtomic: boolean;
    constructor(name: string, parent: CompoundState | null);
}
//# sourceMappingURL=atomicState.d.ts.map