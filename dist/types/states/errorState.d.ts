import { ActionMixin, BaseMixin, ExecutionMixin, MachineMixin } from '../mixins';
import { CompoundState } from './compoundState';
export declare class ErrorState {
    base: BaseMixin;
    machine: MachineMixin;
    execution: ExecutionMixin;
    action: ActionMixin;
    isError: boolean;
    constructor(name: string, parent: CompoundState | null);
}
//# sourceMappingURL=errorState.d.ts.map