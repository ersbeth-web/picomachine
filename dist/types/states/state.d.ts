import { AsyncState } from './asyncState';
import { AtomicState } from './atomicState';
import { DoneState } from './doneState';
import { ErrorState } from './errorState';
import { NestedState } from './nestedState';
import { ParallelState } from './parallelState';
export type State = AtomicState | AsyncState | NestedState | ParallelState | DoneState | ErrorState;
//# sourceMappingURL=state.d.ts.map