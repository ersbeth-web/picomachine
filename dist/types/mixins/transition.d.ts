import { Action, Guard } from '../callbacks';
import { State } from '../states';
export declare class BaseTransition {
    readonly guard: Guard | null;
    readonly actions: Action[];
    readonly delay: number;
    constructor(guard: Guard | null, actions: Action[], delay: number);
}
export declare class InternalTransition extends BaseTransition {
}
export declare class ExternalTransition extends BaseTransition {
    readonly target: State;
    constructor(target: State, guard: Guard | null, actions: Action[], delay: number);
}
export type Transition = InternalTransition | ExternalTransition;
//# sourceMappingURL=transition.d.ts.map