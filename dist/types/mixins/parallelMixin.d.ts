import { CompoundState } from '../states';
export declare class ParallelMixin {
    #private;
    constructor(state: CompoundState);
    forwardChanges(): void;
    processDone(): void;
    processError(): void;
}
//# sourceMappingURL=parallelMixin.d.ts.map