import { Action } from '../callbacks';
export declare class ActionMixin {
    #private;
    get enterActions(): Action[];
    get exitActions(): Action[];
    addEnterActions(enterActions: Action[]): void;
    addExitActions(exitActions: Action[]): void;
    execEnterActions(): void;
    execExitActions(): void;
}
//# sourceMappingURL=actionsMixin.d.ts.map