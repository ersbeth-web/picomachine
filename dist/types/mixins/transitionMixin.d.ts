import { BaseTransition, Transition } from './transition';
export declare class TransitionMixin {
    #private;
    get autoTransitions(): Transition[];
    get eventsTransitions(): Record<string, Transition[]>;
    addAutoTransitions(autoTransitions: Transition[]): void;
    addEventTransitions(eventsTransitions: Record<string, Transition[]>): void;
    findTransition(transitions: Transition[], data: any): Transition | null | undefined;
    execTransitionActions(transition: BaseTransition, data: any): void;
}
//# sourceMappingURL=transitionMixin.d.ts.map