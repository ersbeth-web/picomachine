import { Signal } from '@ersbeth/picosignal';
import { State } from '../states';
export declare class CompoundMixin {
    #private;
    nodesUpdated: Signal<void>;
    get nodes(): State[];
    set nodes(nodes: State[]);
}
//# sourceMappingURL=compoundMixin.d.ts.map