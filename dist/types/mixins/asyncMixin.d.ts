import { Activity } from '../callbacks';
export declare class AsyncMixin {
    #private;
    constructor(activity: Activity);
    get activity(): Activity;
}
//# sourceMappingURL=asyncMixin.d.ts.map