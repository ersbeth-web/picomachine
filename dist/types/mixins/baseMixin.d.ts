import { CompoundState, State } from '../states';
export declare class BaseMixin {
    #private;
    constructor(state: State, name: string, parent: CompoundState | null);
    get parent(): CompoundState | null;
    set parent(parent: CompoundState);
    get name(): string;
    get active(): boolean;
    get root(): State;
}
//# sourceMappingURL=baseMixin.d.ts.map