import { Signal } from '@ersbeth/picosignal';
import { State } from '../states';
export declare class ExecutionMixin {
    #private;
    changed: Signal<void>;
    constructor(state: State);
    enter(): void;
    exit(): void;
    execCommand(event: string, data: any): boolean;
    raise(event: string, data: any): void;
}
//# sourceMappingURL=executionMixin.d.ts.map