import { Guard } from '../callbacks';
import { CompoundState, State } from '../states';
export declare class NestedMixin {
    #private;
    constructor(state: CompoundState, historyGuard: Guard);
    get initial(): State | null;
    set initial(initial: State);
    get history(): State | null;
    get current(): State | null;
    set current(current: State | null);
    get historyGuard(): Guard;
    forwardChanges(): void;
    resetHistory(): void;
    clearCurrent(): void;
}
//# sourceMappingURL=nestedMixin.d.ts.map