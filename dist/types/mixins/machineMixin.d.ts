import { Observable, Slot } from '@ersbeth/picosignal';
import { State } from '../states';
import { Status } from '../status';
export declare class MachineMixin {
    #private;
    status: Observable<Status>;
    constructor(state: State);
    start(): void;
    send(event: string, data?: any): void;
    onError(callback: Slot<void>): () => void;
    onDone(callback: Slot<void>): () => void;
    wait(stateDescriptor: string | RegExp, timeout?: number): Promise<void>;
}
//# sourceMappingURL=machineMixin.d.ts.map