import { Observable, Signal } from '@ersbeth/picosignal';
import { Transition } from './transition';
export declare class ActorMixin {
    #private;
    constructor();
    done: Observable<boolean>;
    error: Observable<boolean>;
    onError: Signal<void>;
    onDone: Signal<void>;
    get doneTransitions(): Transition[];
    get errorTransitions(): Transition[];
    addErrorTransitions(errorTransitions: Transition[]): void;
    addDoneTransitions(doneTransitions: Transition[]): void;
}
//# sourceMappingURL=actorMixin.d.ts.map