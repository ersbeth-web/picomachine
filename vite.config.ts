import { defineConfig } from "vite";
import circleDependency from "vite-plugin-circular-dependency";
import dts from "vite-plugin-dts";

import PACKAGE from "./package.json" with { type: "json" };

export default defineConfig({
    root: "./src",
    build: {
        outDir: "../dist",
        emptyOutDir: true,
        lib: {
            entry: "exports.ts",
            formats: ["es"],
        },
        target: "esnext",
        minify: false,
        rollupOptions: {
            external: Object.keys(PACKAGE.dependencies),
        },
    },

    plugins: [
        // circleDependency(),
        dts({
            // ts types
            include: "**/*",
            outDir: "../dist/types",
            compilerOptions: {
                rootDir: "src",
            },
        }),
    ],
});
