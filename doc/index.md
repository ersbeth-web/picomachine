---
layout: home

hero:
  name: PicoMachine
  text: TypeScript StateCharts
  tagline: Simplify complex application logic using finite state machines
  image:
    src: /logo.png
    alt: VitePress
  actions:
    - theme: brand
      text: What's PicoMachine?
      link: /picomachine.md
    - theme: alt
      text: What's a State Machine?
      link: ./concepts/statemachine.md
    - theme: alt
      text: Get Started
      link: /guide/getting-started.md

features:
  - title: Lightweight
    icon: 
      src: /icons/feather-pointed-solid.svg
    details: Minimal overhead that integrates seamlessly into any TypeScript/JavaScript codebase.
  - title: Expressive
    icon: 
      src: /icons/palette-solid.svg
    details: Declaratively configure states, transitions, guards, and actions for clarity and control.
  - title: Flexible
    icon: 
      src: /icons/arrows-split-up-and-left-solid.svg
    details: Easily hook into transitions to trigger side effects—whether it’s model updates, validations, or complex logic.
  - title: Intuitive
    icon: 
      src: /icons/lightbulb-solid.svg
    details: Benefit from a clear and well-designed API that simplifies state management.
  - title: Complete
    icon: 
      src: /icons/check-double-solid.svg
    details: Enjoy a fully-fledged statechart implementation with nested states, parallel states, and history.
  - title: Powerful
    icon: 
      src: /icons/bolt-solid.svg
    details: Leverage guarded, transient, delayed, and even distributed transitions to handle any scenario.
  - title: Async-Ready
    icon: 
      src: /icons/spinner-solid.svg
    details: Seamlessly manage asynchronous tasks with dedicated activity states.
  - title: Composable
    icon: 
      src: /icons/puzzle-piece-solid.svg
    details: Effortlessly split, compose, and reuse your state machines across your entire codebase.
---