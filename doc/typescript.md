# TypeScript Integration

Picomachine is built with TypeScript support in mind. By leveraging TypeScript’s advanced type system, Picomachine ensures that your state machine configuration is fully type-safe. This means that:

- The [send](./reference/picomachine.statemachine.send.md) function only accepts event names that are defined in your configuration.
- The [matches](./reference/picomachine.status.matches.md) function only accepts state names that are defined in your configuration.

To enable this powerful type-checking, it is **mandatory** to declare your configuration using a combination of the `as const` assertion and the `satisfies StateConfig` operator. This combination preserves literal types so that Picomachine can extract and enforce the allowed event and state names.

## Declaring a Configuration

When you declare your configuration, you must use the `as const` assertion. This ensures that all string literals (such as event names and state names) are kept as literal types instead of being widened to `string`. Additionally, using the `satisfies StateConfig` operator forces the configuration to conform to the expected structure while still preserving its literal types.

**Example:**

```ts
import type { StateConfig } from "./picomachine.stateconfig";

const config = {
  name: "root",
  type: "nested",
  initial: "A",
  states: {
    A: {
      type: "atom",
      events: {
        FINISH: { target: "DONE" }
      }
    },
    DONE: { type: "done" }
  }
} as const satisfies StateConfig;
```

In this example, TypeScript retains the literal types:
- The state names `"root"`, `"A"`, and `"DONE"` remain literal.
- The event name `"FINISH"` is preserved as a literal type.

## Using Strongly Typed Methods

With your configuration defined as shown above, Picomachine extracts the allowed event and state names. This enables strong type-checking in the following ways:

### The `send` Method

The `send` method is typed so that it only accepts event names present in your configuration.

**Example:**

```ts
const machine = new StateMachine(config);

// Valid: "FINISH" is defined in the configuration.
machine.send("FINISH");

// Invalid: "WRONG_EVENT" is not part of the configuration.
// This line will cause a compile-time TypeScript error.
// machine.send("WRONG_EVENT");
```

### The `matches` Method

Similarly, the `matches` method only accepts state names extracted from the configuration. Typically, these are the fully qualified state names.

**Example:**

```ts
// Assuming the state name for A is qualified as "root.A" by Picomachine...
machine.status.matches("root.A");  // OK

// Invalid: "WRONG_STATE" is not defined in your configuration.
// This will trigger a compile-time TypeScript error.
// machine.status.matches("WRONG_STATE");
```

## Why Use `as const` and `satisfies StateConfig`?

- **`as const`:**  
  This assertion tells TypeScript to treat the object’s properties as immutable literal types. Without it, `"FINISH"` might be widened to `string`, which would defeat the purpose of extracting a union of literal event names.

- **`satisfies StateConfig`:**  
  This operator enforces that your configuration adheres to the `StateConfig` interface. It gives you the benefits of strict type checking (e.g., catching missing required fields) while preserving the literal types needed for Picomachine’s type extraction.

---

## Summary

By declaring your state machine configuration with:

```ts
const config = { ... } as const satisfies StateConfig;
```

you unlock the following benefits:

- **Type-Safe `send` Method:**  
  Only accepts event names that exist in your configuration, catching typos or invalid events at compile time.

- **Type-Safe `matches` Method:**  
  Only accepts state names derived from your configuration, ensuring you only reference valid states.

This approach leads to a robust and self-documenting design that leverages TypeScript’s strengths. It also provides excellent auto-completion support in your editor, helping you build state machines with confidence.