# Actions

**Actions** are functions that perform **side effects** during state transitions. In Picomachine, you can attach actions to several hooks in the finite state machine lifecycle:

- **Enter Hooks**: Executed when entering a state.
- **Exit Hooks**: Executed when leaving a state.
- **Transition Hooks**: Executed during the state transition (between exit and enter).

The execution order for these hooks is: **exit → transition → enter**.

::: warning
Actions must be **synchronous**. If you need to perform asynchronous (promise-based) side effects, use [activity states](./activity-states.md) instead.
:::

::: details REFERENCE
* [Action](../reference/picomachine.action.md)
* [TransitionConfig](../reference/picomachine.transitionconfig.md)
* [StateConfig](../reference/picomachine.stateconfig.md)
:::

## Syntax

**Actions** can be defined as functions, an array of functions, or a [Trigger](./trigger.md). They can be assigned to any hook (*enter*, *exit*, or *transition*) within a state's configuration.

You can assign a **single** action function to a hook:

```js
{
  enter: (ctx) => console.log("Entering state", ctx)
}
```

If you need to perform **several** side effects, you can provide an array of actions. They are executed in order:
```js
{
  exit: [
    (ctx) => console.log("Exiting state", ctx),
    (ctx) => { /* additional exit logic */ }
  ]
}
```

You can also send a `context` object along with an event. This `context` is passed as a parameter to the actions of the triggered transition:

```js
// action configuration
events: {
  eventName: { 
    target: "SOMEWHERE", 
    actions: (ctx) => doStuff(ctx) 
  }
}
```

```js
// sending the event+context
machine.send("eventName", context);
```

In this example, when the `eventName` is sent, the `doStuff` function will receive `context` as its argument.
## Example

Below is an example that demonstrates how to define actions and attach them to your state machine:

```mermaid
stateDiagram-v2
    state root {
        [*] --> OFF
        OFF --> ON: toggle / toON
        ON --> OFF: toggle / toOFF
    }
    note right of OFF
        enter / enterOFF
        exit / exitOFF
    end note
    note right of ON
        enter / enterON
        exit / exitON
    end note
```

<<< ./examples/actions.ts