# History States

**History states** are a powerful feature in Picomachine that allow a **nested state** to *remember* which substate was active when you last exited it. When you re-enter a state with history enabled, the state machine **resumes** from the last active substate instead of starting over from the initial substate.

This is especially useful in scenarios where you want to preserve user progress or the current status of a component.

::: details REFERENCE
* [StateConfig](../reference/picomachine.stateconfig.md)
:::

## Syntax

To enable history in a state, simply add the `history: true` property to the state definition:

```js
//...
STATE_NAME: {
    type:"nested",
    history: true,
    //...
}
//...
```

## Example
Imagine a media player: if a user toggles the player off and then back on, they might expect the player to resume from where it left off (either playing or paused). Enabling history in the player’s `ON` state makes this behavior seamless **without requiring extra code**.

```mermaid
stateDiagram-v2
    state root {
        [*] --> OFF
        OFF --> ON: toggle
        state ON {
            [*] --> PLAYING
            PLAYING --> PAUSED: pause
            PAUSED --> PLAYING: play
        }
        ON --> OFF: toggle
    }
    note right of ON
        History enabled: re-enters last active substate
    end note
```

In the corresponding code below, the `ON` state is configured as a nested state with two substates—`PLAYING` and `PAUSED`—and history is enabled. This means that if the machine leaves the `ON` state and later re-enters it, it will return to the substate that was active when it left, rather than defaulting to the initial substate.

<<< ./examples/history-states.ts{9}

In summary, enabling history in a nested state allows your state machine to restore the last active substate when re-entering that state, which can significantly improve user experience by preserving context across transitions.