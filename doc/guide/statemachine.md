# State Machine

:::tip
Be sure to read the [State Machine Concepts](../concepts/statemachine.md) page first as it provides a high-level overview of the concepts described in this guide.
:::

A **state machine** in Picomachine is a structure that defines the behavior of your application in terms of discrete **states** and the **transitions** between them triggered by **events**. It serves as a blueprint for managing complex logic in a predictable and maintainable way.

A state machine is built from three core concepts:

- **States:**  
  A state represents a distinct condition or **mode** of your application. Each state can have its own entry and exit [actions](./actions.md) and encapsulates behavior specific to that condition.

- **Events:**  
  Events are occurrences—such as user actions or system signals—that act as **inputs** to the state machine. They **trigger** the evaluation of transitions and drive changes in the machine's state.

- **Transitions:**  
  Transitions define how the state machine **moves** from one state to another when a specific event occurs. They can include additional logic, such as guard conditions and actions, to ensure that transitions happen only under the desired conditions.

In Picomachine, you create a state machine by instantiating the `StateMachine` class with a configuration object that describes the *state hierarchy*, the *events*, and the *transitions* between states.

::: details REFERENCE
* [StateMachine](../reference/picomachine.statemachine.md)
* [StateConfig](../reference/picomachine.stateconfig.md)
* [TransitionConfig](../reference/picomachine.transitionconfig.md)
:::

## Syntax

### Configuration

To define a state machine, you instantiate the `StateMachine` class with a configuration object that specifies:
   - The **name** of the state machine root.
   - The **type** of the state machine root (usually `"nested"` for [hierarchical states](./nested-states.md)).
   - The **initial** state.
   - A **states** object that maps state names to their configurations.
      - Within each state configuration, you can define an `events` object to map event names to transition configurations.

For example:

```js
new StateMachine({
  name: "root", // Root state name
  type: "nested", // Root supports nested children
  initial: "STATE_A", // The machine starts in the STATE_A state
  states: {
    STATE_A: {
      type:"atom", // STATE_A doesn't have children
      events: {
        event1: { target: "STATE_B" } // On 'event1', transition to STATE_B
      }
    },
    STATE_B: {
      type:"atom", // STATE_B doesn't have children
      events: {
        event2: { target: "STATE_A" } // On 'event2', transition back to STATE_A
      }
    }
  }
});
```

### Usage

Once you have configured your state machine, you can interact with it using its [available methods](../reference/picomachine.statemachine.md). The `StateMachine` class provides several methods to control and query the state machine:

* [start:](../reference/picomachine.statemachine.start)
Initiates the state machine, causing it to enter its initial state.

* [send:](../reference/picomachine.statemachine.send)
Sends an event to the state machine. The machine will evaluate its current state’s configuration and perform the corresponding transition if the event is defined. You can also pass an optional data object that is forwarded to the actions associated with the event.

* [onChange:](../reference/picomachine.statemachine.onchange)
Registers a callback to be invoked when an change occurs in the state machine.

* [onError:](../reference/picomachine.statemachine.onerror)
Registers a callback to be invoked when an error occurs in the state machine.

* [onDone:](../reference/picomachine.statemachine.ondone)
Registers a callback to be invoked when the state machine reaches a done (final) state.

* [status:](../reference/picomachine.statemachine.status)
Returns the current status of the state machine.

## Example

The following example demonstrates a simple toggle state machine. In this machine, there are two states—`OFF` and `ON`—and an event named `toggle` that switches the machine between these states.

```mermaid
stateDiagram-v2
    state root {
        [*] --> OFF
        OFF --> ON: toggle
        ON --> OFF: toggle
    }
```

<<< ./examples/statemachine.ts