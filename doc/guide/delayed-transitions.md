# Delayed Transitions

**Delayed transitions** allow you to postpone the execution of a state transition for a specified **duration** (in milliseconds), as long as the state machine remains in the current state. If the machine leaves the state before the delay period has elapsed, the transition is **cancelled**.

**Key Points:**

- **Delay Duration:**  
  The transition will be executed after the specified delay time, measured in milliseconds.

- **State Consistency:**  
  The transition is only executed if the machine is still in the state where the event was triggered when the delay period ends.

- **Guarded & Delayed Transitions:**  
  When an event transition is both guarded and delayed, the guard function is evaluated immediately. Once a transition is selected, its execution is postponed by the specified delay.

::: details REFERENCE
* [TransitionConfig](../reference/picomachine.transitionconfig.md)
:::

## Syntax

Define a **delayed transition** by specifying the `delay` property along with the target state in the event configuration:

```js
// transition will be executed 500ms after "eventName" is received
...
events: {
  eventName: { target: "TARGET_NAME", delay: 500 } 
}
...
```

## Example

Consider a state machine that starts in the `IDLE` state. When the `start` event is sent, the machine schedules a delayed transition to the `RUNNING` state after 500ms. However, if the `cancel` event is sent before the 500ms elapses, the machine immediately transitions to the `CANCELLED` state, thereby cancelling the delayed transition.

<<< ./examples/delayed-transitions.ts