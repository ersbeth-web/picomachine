
# Getting Started

**Picomachine** is a tiny yet powerful library for composing [finite state machines](../concepts/statemachine.md) and [statecharts](../concepts/statechart.md) in TypeScript. It provides all the features you’d expect in a minimal, developer-friendly API.

The library runs in both the browser and Node.js.

## Installation

```bash
npm install @ersbeth/picomachine
```

## Quick Example

Let’s kick things off with a toggle machine, which has only two states: `ON` and `OFF`. An event called `TOGGLE` moves it between these two states.

```mermaid
stateDiagram-v2
    state root {
        [*] --> OFF
        OFF --> ON: toggle
        ON --> OFF: toggle
    }
```

<<< ./examples/getting-started.ts

## TypeScript 
Picomachine is designed with TypeScript in mind. One of its key features is strong type-safety for your state machines. To enable this, it is mandatory to declare your machine configurations like so:

```ts
const config = {
  name: "root",
  type: "nested",
  initial: "A",
  states: {
    A: { 
      type: "atom", 
      events: { FINISH: { target: "DONE" } } 
    },
    DONE: { type: "done" }
  }
} as const satisfies StateConfig;

```

This combination preserves literal types so that Picomachine can extract the allowed event and state names.

For more details, please see our [TypeScript documentation](../typescript.md).

## Linter

In addition to type inference, Picomachine includes a [linter](../linter.md) that checks your state machine configurations for common issues, such as invalid transitions. We provide a dedicated [VS Code extension](https://marketplace.visualstudio.com/items?itemName=Ersbeth.picomachinelinter&ssr=false#overview) for real-time linting.

## What’s Next?

1. **Explore the Theory:** Discover the fundamentals of state machines and statecharts by exploring our [Concepts](../concepts/statemachine.md) section.
2. **Master Picomachine:** Learn how to harness the power of Picomachine by diving into our comprehensive [Guide](./statemachine.md).
3. **Consult the API Reference:** Get detailed documentation on every function, configuration, and class in our [API Reference](../reference/index.md) to fully understand the library.