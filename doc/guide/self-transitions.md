# Self-Transitions

Self-transitions allow a state to transition to **itself** — effectively “refreshing” the state. There are two distinct types of self-transitions:

- **External Self-Transition:**  
  This transition causes the state to **exit** and then **re-enter**. As a result, both the exit and entry action hooks are triggered, along with any actions defined on the transition itself. Use this when you want to reinitialize a state completely.

- **Internal Self-Transition:**  
  This transition does **not** cause the state to exit or re-enter. Only the transition action hook is executed. Use this when you need to perform some side effect or update without restarting the state’s lifecycle.

::: details REFERENCE
* [TransitionConfig](../reference/picomachine.transitionconfig.md)
:::

## Syntax
### External Self-Transition
To define an external self-transition, use the `self` keyword as the target.

```js
// External self-transition: triggers exit and entry hooks.
//..
events: {
  eventName: { target: "self" }
}
//...
```

### Internal Self-Transition
To define an internal self-transition, use the `internal` keyword as the target
```js
// Internal self-transition: does NOT trigger exit/enter hooks.
...
events: {
  eventName: { target: "internal" }
}
...
```

## Example

In this example, the `ACTIVE` state has both an **external** self-transition triggered by the `reset` event and an **internal** self-transition triggered by the `refresh` event. The external self-transition will log messages for both exit and entry actions, while the internal self-transition will only log the transition action.

<<< ./examples/self-transitions.ts