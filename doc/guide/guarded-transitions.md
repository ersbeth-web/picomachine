# Guarded Transitions

**Guarded transitions** allow you to incorporate conditional logic into your state machine. When an event occurs, the state machine checks the associated guarded transition(s) in order and executes the **first one** whose guard function returns `true`. This way, you can decide which transition to take based on the current context or other dynamic conditions.

::: details REFERENCE
* [Guard](../reference/picomachine.guard.md)
* [TransitionConfig](../reference/picomachine.transitionconfig.md)
:::

## Syntax

For a **single guarded transition**, define a guard function and assign it directly in the transition configuration:

```js
// Single guard use
const guard:Guard = //...
//...
events: {
  eventName: { target: "TARGET_NAME", guard: guard }
}
//...
```

If you need to evaluate **multiple conditions** for a single event, you can provide an array of transition configurations. Each configuration includes its own target and guard. The transitions are evaluated sequentially:

```js
// Multiple guards use
const guard1:Guard = //...
const guard2:Guard = //...
//...
events: {
  eventName: [
    { target: "TARGET_1_NAME", guard: guard1 },
    { target: "TARGET_2_NAME", guard: guard2 }
  ]
}
//...
```

## Example
 In the example below, the state machine models a simple authentication flow where, after logging in, the application routes the user to either an admin dashboard or a user dashboard based on their role. Guard functions inspect the context to decide which transition to take.

 <<< ./examples/guarded-transitions.ts