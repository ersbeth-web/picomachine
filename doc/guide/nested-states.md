# Nested States

**Nested states** allow you to group related states under a single **parent state**, which helps organize complex behaviors and encapsulate sub-flows within your state machine. In a nested state machine, a parent state can have its own substates, each with its own configuration. This is particularly useful when a state has multiple internal modes or behaviors.

::: details REFERENCE
* [StateConfig](../reference/picomachine.stateconfig.md)
:::

## Syntax

A nested state is defined this way:
```js
...
type: "nested" // indicates that this state contains sub-states
initial: "INITIAL_SUBSTATE_NAME" // the machine will start in this substate
states: {
    SUBSTATE_1: {...}
    SUBSTATE_2: {...}
    SUBSTATE_3: {...}
    ...
}
...
```

## Example

Below is a basic example of a player state machine. In this example, the player can be toggled **ON** or **OFF**. When the player is **ON**, it further subdivides into two substates: **PLAYING** and **PAUSED**.

```mermaid
stateDiagram-v2
    state root {
        [*] --> OFF
        OFF --> ON: toggle
        state ON {
            [*] --> PLAYING
            PLAYING --> PAUSED: pause
            PAUSED --> PLAYING: play
        }
        ON --> OFF: toggle
    }
```
<<< ./examples/nested-states.ts

