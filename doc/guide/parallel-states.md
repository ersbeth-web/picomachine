# Parallel States

**Parallel states** allow your state machine to run multiple independent sub-machines concurrently. This means that different aspects of your application—such as various UI components or independent processes—can operate simultaneously, each maintaining its own state.

In a parallel state, each sub-state machine runs in its own region. Although the parallel state itself **does not have final states**, its nested sub-states can have final states. In such cases:
- The parent's **done** transition is triggered only when **all** nested sub-states that have final states are complete.
- The parent's **error** transition is triggered if **any** nested sub-state enters an error state.

::: details REFERENCE
* [StateConfig](../reference/picomachine.stateconfig.md)
:::

## Syntax

To create a parallel state machine in Picomachine, set the `type` property to `"parallel"` in your state configuration.

```js
//...
STATE_NAME: {
    type: "parallel",
    //...
}
//...
```

## Example

The following code defines a parallel state machine for managing three independent styling features: **Bold**, **Underline**, and **Italics**. Each feature is implemented as its own nested state running concurrently.

```mermaid
stateDiagram-v2
    [*] --> BOLD
    state BOLD {
            [*] --> bold.OFF
            bold.OFF --> bold.ON: toggleBold
            bold.ON --> bold.OFF: toggleBold
    }

    [*] --> UNDERLINE
    state UNDERLINE {
            [*] --> underline.OFF
            underline.OFF --> underline.ON: toggleUnderline
            underline.ON --> underline.OFF: toggleUnderline
    }

    [*] --> ITALICS
    state ITALICS {
            [*] --> italics.OFF
            italics.OFF --> italics.ON: toggleItalics
            italics.ON --> italics.OFF: toggleItalics
    }
```

<<< ./examples/parallel-states.ts
