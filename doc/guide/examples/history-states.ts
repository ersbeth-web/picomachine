import { StateMachine } from "@ersbeth/picomachine";

const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "OFF",
    states: {
        ON: {
            type: "nested",
            history: true, // remembers the last active substate
            events: {
                toggle: { target: "OFF" },
            },
            initial: "PLAYING", // Default substate if no history is available
            states: {
                PLAYING: {
                    type: "atom",
                    events: {
                        pause: { target: "PAUSED" },
                    },
                },
                PAUSED: {
                    type: "atom",
                    events: {
                        play: { target: "PLAYING" },
                    },
                },
            },
        },
        OFF: {
            type: "atom",
            events: {
                toggle: { target: "ON" },
            },
        },
    },
});

machine.onChange((status) => console.log(status));

machine.start();
// Expected output:
// [ 'root' ]
// [ 'root.OFF' ]

machine.send("toggle");
// Expected output:
// [ 'root.ON' ]
// [ 'root.ON.PLAYING' ]

machine.send("pause");
// Expected output:
// [ 'root.ON.PAUSED' ]

machine.send("toggle");
// Expected output:
// [ 'root.OFF' ]

machine.send("toggle");
// Expected output:
// [ 'root.ON' ]
// [ 'root.ON.PAUSED' ]

/* Note: Without history enabled, the machine would have
 * re-entered 'ON' in the PLAYING substate.*/
