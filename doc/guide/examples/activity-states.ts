import { StateMachine } from "@ersbeth/picomachine";

// fetch function that returns a promise.
const fetchResource = (): Promise<any> => {
    return Promise.resolve();
    // or Promise.reject();
};

// Define the state machine with an activity state.
const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "FETCHING",
    states: {
        FETCHING: {
            type: "async", // Marks this state as an asynchronous (activity) state.
            // Transition to SUCCESS when the fetch resolves.
            done: { target: "SUCCESS" },
            // Transition to FAILED when the fetch fails.
            error: { target: "FAILED" },
            // The asynchronous function to execute.
            activity: fetchResource,
            // Log a message when entering FETCHING.
            enter: () =>
                console.log(
                    "Entering FETCHING state, starting resource fetch...",
                ),
        },
        SUCCESS: {
            type: "atom",
            // Log a success message upon entering SUCCESS.
            enter: (data?: any) => console.log("Fetch succeeded:", data),
        },
        FAILED: {
            type: "atom",
            // Log an error message upon entering FAILED.
            enter: (error?: any) => console.error("Fetch failed:", error),
        },
    },
});

// Start the state machine.
// The machine begins in the FETCHING state.
machine.start();

// Expected behavior:
// - When the machine starts, it enters the FETCHING state and logs the starting message.
// - After 1 second, based on the outcome, it either transitions to SUCCESS (logging the resource data)
//   or transitions to FAILED (logging the error).
