import { StateMachine } from "@ersbeth/picomachine";

// Guard functions evaluate the context to determine the user's role.
const guards = {
    isAdmin: (context: { userRole: string }) => context.userRole === "admin",
    isUser: (context: { userRole: string }) => context.userRole === "user",
};

// Create a state machine representing an authentication flow.
const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "LOGGED_OUT",
    states: {
        LOGGED_OUT: {
            type: "atom",
            events: {
                login: { target: "CHECK_ROLE" },
            },
        },
        CHECK_ROLE: {
            type: "atom",
            // The "continue" event triggers guarded transitions.
            events: {
                continue: [
                    { target: "ADMIN_DASHBOARD", guard: guards.isAdmin },
                    { target: "USER_DASHBOARD", guard: guards.isUser },
                ],
            },
        },
        ADMIN_DASHBOARD: {
            type: "atom",
            // Admin dashboard state...
        },
        USER_DASHBOARD: {
            type: "atom",
            // User dashboard state...
        },
    },
});

// Assume the context is set somewhere in your code with userRole: "admin" or "user".
declare const context: { userRole: string };

machine.start(); // Starts in 'LOGGED_OUT'

// When the "login" event occurs, transition to CHECK_ROLE.
machine.send("login");

// Later, when the "continue" event occurs, the machine evaluates the guards:
// - If context.userRole is "admin", it transitions to ADMIN_DASHBOARD.
// - If context.userRole is "user", it transitions to USER_DASHBOARD.
machine.send("continue", context);
