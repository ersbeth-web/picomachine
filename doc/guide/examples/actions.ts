import { StateMachine } from "@ersbeth/picomachine";

const actions = {
    enterON: () => console.log("enter ON"),
    exitON: () => console.log("exit ON"),
    enterOFF: () => console.log("enter OFF"),
    exitOFF: () => console.log("exit OFF"),
    toON: () => console.log("to ON"),
    toOFF: () => console.log("to OFF"),
};

const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "OFF",
    states: {
        ON: {
            type: "atom",
            enter: actions.enterON, // enter hook
            exit: actions.exitON, // exit hook
            events: {
                toggle: {
                    target: "OFF",
                    actions: actions.toOFF, // transition hook
                },
            },
        },
        OFF: {
            type: "atom",
            enter: actions.enterOFF, // enter hook
            exit: actions.exitOFF, // exit hook
            events: {
                toggle: {
                    target: "ON",
                    actions: actions.toON, // transition hook
                },
            },
        },
    },
});

// Attach a listener to observe state changes
machine.onChange((status) => console.log(status));

// Start the state machine
machine.start();
// Logs: [ 'root' ]
//       enter OFF
//       [ 'root.OFF' ]

machine.send("toggle");
// Logs: exit OFF
//       to ON
//       enter ON
//       [ 'root.ON' ]

machine.send("toggle");
// Logs: exit ON
//       to OFF
//       enter OFF
//       [ 'root.OFF' ]
