import { StateMachine } from "@ersbeth/picomachine";

// Create a simple toggle state machine
const machine = new StateMachine({
    name: "root",
    type: "nested", // Indicates a hierarchical state machine
    initial: "OFF", // The machine starts in the OFF state
    states: {
        OFF: {
            type: "atom",
            // When the 'toggle' event is triggered in OFF, transition to ON.
            events: {
                toggle: { target: "ON" },
            },
        },
        ON: {
            type: "atom",
            // When the 'toggle' event is triggered in ON, transition back to OFF.
            events: {
                toggle: { target: "OFF" },
            },
        },
    },
});

// Listen for status changes (for demonstration purposes)
machine.onChange((status) => console.log("Status:", status));

// Start the state machine (initially in the OFF state)
machine.start();
// Expected output:
// Status: [ 'root.OFF' ]

// Send the 'toggle' event to transition from OFF to ON
machine.send("toggle");
// Expected output:
// Status: [ 'root.ON' ]

// Send the 'toggle' event again to transition from ON to OFF
machine.send("toggle");
// Expected output:
// Status: [ 'root.OFF' ]
