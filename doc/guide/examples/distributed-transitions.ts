import { StateMachine } from "@ersbeth/picomachine";

// Define a simple state machine that uses distributed transitions.
const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "A", // Start in state A
    // Distributed event configuration is set at the parent level.
    events: {
        // The 'toD' event will cause a transition to state D from any child state.
        toD: { target: "children.D" },
    },
    states: {
        A: {
            type: "atom",
            // State A has no specific event configuration for 'toD'
        },
        B: {
            type: "atom",
            // State B has no specific event configuration for 'toD'
        },
        C: {
            type: "atom",
            // State C has no specific event configuration for 'toD'
        },
        D: {
            type: "atom",
            // The target state for the 'toD' event.
            enter: () => console.log("Entered state D"),
        },
    },
});

// Start the state machine.
machine.start();
// Expected initial status: "root.A"

// Send the 'toD' event. No matter which state (A, B, or C) is active,
// the distributed transition defined on the parent causes a transition to D.
machine.send("toD");
// Expected output:
// "Entered state D"
