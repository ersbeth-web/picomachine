import { StateMachine } from "@ersbeth/picomachine";

const machine = new StateMachine({
    name: "root",
    type: "nested", // Indicates that the root state is nest (it's nearly always the case)
    initial: "OFF", // The machine starts in the OFF state.
    states: {
        // The ON state is a parent state that itself has nested substates.
        ON: {
            type: "nested",
            events: {
                toggle: { target: "OFF" },
            },
            initial: "PLAYING", // When entering ON, the initial substate is PLAYING.
            states: {
                PLAYING: {
                    type: "atom",
                    events: {
                        pause: { target: "PAUSED" },
                    },
                },
                PAUSED: {
                    type: "atom",
                    events: {
                        play: { target: "PLAYING" },
                    },
                },
            },
        },

        // The OFF state is a simple state with no nested substates.
        OFF: {
            type: "atom",
            events: {
                toggle: { target: "ON" },
            },
        },
    },
});

// Attach a listener to observe state changes
machine.onChange((status) => console.log(status));

// Start the state machine
machine.start();
// Expected logs:
// [ 'root' ]
// [ 'root.OFF' ]

// Transition from OFF to ON. In ON, the machine starts in the PLAYING substate.
machine.send("toggle");
// Expected logs:
// [ 'root.ON' ]
// [ 'root.ON.PLAYING' ]

// Transition from PLAYING to PAUSED within the ON state.
machine.send("pause");
// Expected logs:
// [ 'root.ON.PAUSED' ]

// Transition from ON back to OFF.
machine.send("toggle");
// Expected logs:
// [ 'root.OFF' ]
