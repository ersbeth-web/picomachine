import { StateMachine } from "@ersbeth/picomachine";

const machine = new StateMachine({
    name: "root",
    type: "parallel", // concurrent execution of child state machines
    states: {
        BOLD: {
            type: "nested", // Nested state machine for bold styling
            initial: "OFF", // Start with bold off
            states: {
                ON: {
                    type: "atom",
                    events: {
                        toggleBold: { target: "OFF" },
                    },
                },
                OFF: {
                    type: "atom",
                    events: {
                        toggleBold: { target: "ON" },
                    },
                },
            },
        },
        UNDERLINE: {
            type: "nested", // Nested state machine for underline styling
            initial: "OFF", // Start with underline off
            states: {
                ON: {
                    type: "atom",
                    events: {
                        toggleUnderline: { target: "OFF" },
                    },
                },
                OFF: {
                    type: "atom",
                    events: {
                        toggleUnderline: { target: "ON" },
                    },
                },
            },
        },
        ITALICS: {
            type: "nested", // Nested state machine for italics styling
            initial: "OFF", // Start with italics off
            states: {
                ON: {
                    type: "atom",
                    events: {
                        toggleItalics: { target: "OFF" },
                    },
                },
                OFF: {
                    type: "atom",
                    events: {
                        toggleItalics: { target: "ON" },
                    },
                },
            },
        },
    },
});

// Listen for state changes
machine.onChange((status) => console.log(status));

machine.start();
// Logs initial state:
// [ 'root.BOLD.OFF', 'root.UNDERLINE.OFF', 'root.ITALICS.OFF' ]

machine.send("toggleBold");
// Toggles bold to ON:
// [ 'root.BOLD.ON', 'root.UNDERLINE.OFF', 'root.ITALICS.OFF' ]

machine.send("toggleUnderline");
// Toggles underline to ON:
// [ 'root.BOLD.ON', 'root.UNDERLINE.ON', 'root.ITALICS.OFF' ]

machine.send("toggleItalics");
// Toggles italics to ON:
// [ 'root.BOLD.ON', 'root.UNDERLINE.ON', 'root.ITALICS.ON' ]
