import { StateMachine } from "@ersbeth/picomachine";

// Define a state machine with parallel states.
const machine = new StateMachine({
    name: "root",
    type: "parallel",
    states: {
        // STATE_A will trigger an event on the parent.
        STATE_A: {
            type: "atom",
            events: {
                // When "notify" is sent to STATE_A, it triggers "alert" on the parent.
                notify: { target: "self", actions: "alert" },
            },
            enter: () => console.log("STATE_A entered"),
        },
        // STATE_B will handle the triggered event.
        STATE_B: {
            type: "atom",
            events: {
                // When "alert" is received, STATE_B executes its action.
                alert: {
                    target: "self",
                    actions: () => console.log("STATE_B handling alert"),
                },
            },
            enter: () => console.log("STATE_B entered"),
        },
    },
});

// Start the state machine.
machine.start();
// Expected output
// ['root.STATE_A', 'root.STATE_B']

// Send the "notify" event from STATE_A.
// This will trigger the "alert" event on the parent, which is then handled by STATE_B.
machine.send("notify");
// Expected output:
// "STATE_B handling alert"
