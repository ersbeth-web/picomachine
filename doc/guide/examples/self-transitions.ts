import { StateMachine } from "@ersbeth/picomachine";

// Define a simple state machine with self-transitions in the ACTIVE state.
const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "ACTIVE",
    states: {
        ACTIVE: {
            type: "atom",
            // Define event-triggered self-transitions.
            events: {
                // External self-transition: triggers exit and entry hooks.
                reset: {
                    target: "self",
                    actions: () =>
                        console.log("External self-transition executed: reset"),
                },
                // Internal self-transition: does not trigger exit/enter hooks.
                refresh: {
                    target: "internal",
                    actions: () =>
                        console.log(
                            "Internal self-transition executed: refresh",
                        ),
                },
            },
            // Entry and exit hooks for demonstration.
            enter: () => console.log("Entered ACTIVE state"),
            exit: () => console.log("Exited ACTIVE state"),
        },
    },
});

// Start the machine.
// Expected output: "Entered ACTIVE state"
machine.start();

// Send the reset event (external self-transition).
machine.send("reset");
// Expected output:
// "Exited ACTIVE state"
// "External self-transition executed: reset"
// "Entered ACTIVE state"

// Send the refresh event (internal self-transition).
machine.send("refresh");
// Expected output:
// "Internal self-transition executed: refresh"
// (No "Exited" or "Entered" logs should appear.)
