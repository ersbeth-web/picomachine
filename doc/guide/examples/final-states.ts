import { StateMachine } from "@ersbeth/picomachine";

// Define a state machine for file deletion confirmation.
const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "DELETE_OPERATION",
    // Parent-level transitions based on the outcome of the nested state.
    done: { target: "NOTIFY_SUCCESS" },
    error: { target: "NOTIFY_FAILURE" },

    states: {
        DELETE_OPERATION: {
            type: "nested",
            initial: "CONFIRM_DELETE",
            states: {
                CONFIRM_DELETE: {
                    type: "atom",
                    // The confirmation dialog: user must decide whether to delete.
                    enter: () =>
                        console.log(
                            "Displaying file deletion confirmation dialog",
                        ),
                    events: {
                        confirm: { target: "DELETED" },
                        cancel: { target: "CANCELLED" },
                    },
                },
                // Final state indicating the file was deleted.
                DELETED: {
                    type: "done",
                    enter: () => console.log("File deleted successfully"),
                },
                // Final state indicating the deletion was cancelled.
                CANCELLED: {
                    type: "error",
                    enter: () => console.log("File deletion cancelled"),
                },
            },
        },
        // Parent reacts to the final outcome:
        NOTIFY_SUCCESS: {
            type: "atom",
            enter: () => console.log("Notifying user: Deletion successful"),
        },
        NOTIFY_FAILURE: {
            type: "atom",
            enter: () => console.log("Notifying user: Deletion cancelled"),
        },
    },
});

// Listen for state changes (for demonstration)
machine.onChange((status) => console.log("Status:", status));

// Start the state machine.
machine.start();
// Expected output: "Displaying file deletion confirmation dialog"
// and status: [ 'root.DELETE_OPERATION.CONFIRM_DELETE' ]

// Scenario 1: User confirms deletion.
machine.send("confirm");
// Expected output sequence:
// "File deleted successfully"
// "Notifying user: Deletion successful"
// Status: [ 'root.NOTIFY_SUCCESS' ]

// Scenario 2: User cancels deletion.
// If instead the user cancels, you would send:
// machine.send("cancel");
// Expected output sequence:
// "File deletion cancelled"
// "Notifying user: Deletion cancelled"
// Status: [ 'root.NOTIFY_FAILURE' ]
