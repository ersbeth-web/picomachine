import { StateMachine } from "@ersbeth/picomachine";

// Define a state machine with a delayed transition and a cancel option.
const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "IDLE",
    states: {
        IDLE: {
            type: "atom",
            events: {
                // When "start" is received, schedule a transition to RUNNING after 500ms.
                start: { target: "RUNNING", delay: 500 },
                // If "cancel" is received before 500ms expire, transition immediately to CANCELLED.
                cancel: { target: "CANCELLED" },
            },
        },
        RUNNING: {
            type: "atom",
            // Configuration for RUNNING state (if needed).
        },
        CANCELLED: {
            type: "atom",
            // Configuration for CANCELLED state (if needed).
        },
    },
});

// Start the state machine (it begins in the IDLE state).
machine.start(); // Logs: [ 'root.IDLE' ]

// Scenario 1: Allow the delayed transition to occur.
// If no other event is sent within 500ms, the machine will transition from IDLE to RUNNING.
// machine.send("start");

// Scenario 2: Cancel the delayed transition before 500ms elapse.
machine.send("start"); // Schedules transition from IDLE to RUNNING after 500ms.
machine.send("cancel"); // Immediately transitions to CANCELLED, cancelling the scheduled transition.
