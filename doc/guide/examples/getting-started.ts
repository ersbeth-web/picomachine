import { StateMachine } from "@ersbeth/picomachine";

// Define the machine
const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "OFF",
    states: {
        // keys are states names
        ON: {
            type: "atom",
            events: {
                // keys are events names
                toggle: { target: "OFF" },
            },
        },
        OFF: {
            type: "atom",
            events: {
                toggle: { target: "ON" },
            },
        },
    },
});

// Attach a listener to see state changes
machine.onChange((status) => console.log(status));

// Start the machine
machine.start();
// Logs: [ 'root' ] then [ 'root.OFF' ]

machine.send("toggle");
// Logs: [ 'root.ON' ]

machine.send("toggle");
// Logs: [ 'root.OFF' ]
