import { StateMachine } from "@ersbeth/picomachine";

// Simulated condition: change this value to test different outcomes.
const isUserAuthenticated = true;

// Guard function that returns true if the user is authenticated.
const isAuthenticated = () => isUserAuthenticated;

// Guard function that returns true if the user is not authenticated.
const isAnonymous = () => !isUserAuthenticated;

// Define the state machine.
const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "CHECK_AUTH",
    states: {
        CHECK_AUTH: {
            type: "atom",
            // Entry action for logging purposes.
            enter: () => console.log("Entering CHECK_AUTH state"),
            // Transient transitions: evaluated immediately after entry.
            always: [
                { target: "AUTHENTICATED", guard: isAuthenticated },
                { target: "ANONYMOUS", guard: isAnonymous },
            ],
        },
        AUTHENTICATED: {
            type: "atom",
            enter: () => console.log("User is authenticated"),
        },
        ANONYMOUS: {
            type: "atom",
            enter: () => console.log("User is anonymous"),
        },
    },
});

// Start the state machine.
machine.start();

// Expected behavior:
// - The machine starts in the CHECK_AUTH state, logging "Entering CHECK_AUTH state".
// - Immediately after entry, the transient transitions are evaluated.
//   If isAuthenticated() returns true, the machine transitions to AUTHENTICATED,
//   logging "User is authenticated".
//   Otherwise, it transitions to ANONYMOUS, logging "User is anonymous".
