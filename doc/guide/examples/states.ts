import { StateMachine } from "@ersbeth/picomachine";

const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "OFF",
    states: {
        ON: {
            type: "atom",
            events: {
                toggle: { target: "OFF" },
            },
        },
        OFF: {
            type: "atom",
            events: {
                toggle: { target: "ON" },
            },
        },
    },
});

// Start the machine and send events:
machine.start(); // Starts in 'OFF'
machine.send("toggle"); // Transitions to 'ON'
machine.send("toggle"); // Transitions back to 'OFF'
