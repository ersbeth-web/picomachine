import { StateMachine } from "@ersbeth/picomachine";

// Define a simple toggle state machine
const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "OFF", // The machine starts in the OFF state
    states: {
        ON: {
            type: "atom",
            events: {
                toggle: { target: "OFF" }, // When in ON, sending "toggle" goes to OFF
            },
        },
        OFF: {
            type: "atom",
            events: {
                toggle: { target: "ON" }, // When in OFF, sending "toggle" goes to ON
            },
        },
    },
});

// Start the state machine
machine.start(); // Machine is now in the "OFF" state

// Send the "toggle" event to change the state
machine.send("toggle"); // Transitions from OFF to ON
machine.send("toggle"); // Transitions from ON back to OFF
