# Trigger

In Picomachine, a **trigger** is a special type of action that allows a child state to send an event to its parent. This mechanism is especially useful in [parallel states](./parallel-states.md), where child states may need to communicate or coordinate with each other by sending events upward. When a trigger is activated, the event is added to the [JavaScript event queue](https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop). This ensures that any ongoing state transition completes before the new event is processed.

- **Trigger Action:** Instead of executing a function, a trigger action is specified as a string (the event name) in the `actions` field.
- **Event Distribution:** When the trigger is fired, the event is sent to the parent state. This allows sibling states to react to the event, without each having to define the same event configuration.
- **Event Queueing:** The triggered event is queued so that it is handled only after the current transition has finished, ensuring orderly state changes.


::: details REFERENCE
* [TransitionConfig](../reference/picomachine.transitionconfig.md)
* [Trigger](../reference/picomachine.trigger.html)
:::

## Syntax

You define a trigger by setting the `actions` property in an event configuration to an event name string. 

```js
//...
PARENT_STATE: {
    events: {
        CHILD_STATE: { 
            target: "SOME_SIBLING_STATE",
            actions: "SOME_EVENT" // trigger this event on the parent
        }
        //...
    },
    //...
},
//...
```

## Example

Below is a complete example demonstrating how a child state can trigger an event on its parent to facilitate communication between parallel states. In this example, we have a parallel state machine with two child states: `STATE_A` and `STATE_B`.
* When `STATE_A` receives the notify event, it triggers the alert event on the parent.
* `STATE_B` is configured to listen for the alert event and handle it.

<<< ./examples/trigger.ts