# Event Transitions

**Event transitions** define how a state machine changes its **state** when a specific **event** occurs. In other words, when an event is sent to the state machine, it checks the current state's transition configuration for that event. If a matching transition is found, the machine moves to the specified target state.

::: details REFERENCE
* [StateMachine](../reference/picomachine.statemachine.md)
* [TransitionConfig](../reference/picomachine.transitionconfig.md)
:::

## Syntax

To define an **event transition**, include an `events` object in your state configuration. Each key in the object is the name of an event, and its value is a transition configuration that specifies the target state.

```js
//...
events: {
  eventName: { target: "TARGET_NAME" }
}
//...
```
When you want to **trigger** this transition, you send the event to your state machine:
```js
myStateMachine.send("eventName")
```

## Example

Consider a simple toggle state machine that switches between an `OFF` state and an `ON` state. When the `toggle` event is sent, the machine transitions from `OFF` to `ON` or from `ON` back to `OFF`.

```mermaid
stateDiagram-v2
    state root {
        [*] --> OFF
        OFF --> ON: toggle
        ON --> OFF: toggle
    }
```

In this example, every time the `toggle` event is sent, the state machine checks its current state and moves to the defined target state accordingly.

<<< ./examples/event-transitions.ts