# Transient Transitions

**Transient transitions** are automatically triggered as soon as a state is **entered**. They are declared using the `always` property and execute **immediately after** the state’s entry [action hook](./actions.md) has completed.

Transient transitions are ideal for situations where an immediate decision is needed upon entering a state. They allow the state machine to automatically determine the next state based on current conditions (especially when used in conjunction with guards).

::: details REFERENCE
* [TransitionConfig](../reference/picomachine.transitionconfig.md)
* [StateConfig](../reference/picomachine.stateconfig.md)
:::

## Syntax

A transient transition is defined by adding an `always` property to the state configuration.

```js
//...
states: {
  MY_STATE: {
    always: { target: "SOMEWHERE" }
  }
}
//...
```

## Example

In this example, the state machine starts in the `CHECK_AUTH` state. As soon as it enters this state, it immediately evaluates two guarded transient transitions:
* If the guard function `isAuthenticated` returns true, the machine transitions to the `AUTHENTICATED` state.
* Otherwise, if the guard function `isAnonymous` returns true, it transitions to the `ANONYMOUS` state.

<<< ./examples/transient-transitions.ts


