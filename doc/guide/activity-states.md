# Activity States

In real-world applications, you often need to perform **asynchronous operations**—like fetching data from an API—that return promises. In Picomachine, these asynchronous side effects are managed using **activity states**. An activity state lets you integrate promise-based operations into your state machine, automatically handling both success and failure cases with built-in transitions.

When you define an activity state, you specify:
- An **activity function** that returns a promise.
- A `done` transition, which is triggered when the promise resolves.
- An `error` transition, which is triggered when the promise is rejected.

This approach simplifies the handling of asynchronous tasks by automating the transition process based on the outcome of the promise.

::: details REFERENCE
* [Activity](../reference/picomachine.activity.md)
* [StateConfig](../reference/picomachine.stateconfig.md)
:::

## Syntax

An activity state is defined by setting:
- `type` to `"async"`.
- A `done` transition that leads to a state when the promise resolves.
- An `error` transition that handles the failure case.
- An `activity` function that returns a promise.

```js
//...
    states: {
        STATE_A: {
            type: "async",            
            done: { target: "STATE_B" },
            error: { target: "STATE_B" }, 
            activity: activityFunction
        },
        STATE_B: {} 
        STATE_C: {}
    }
//...
```

## Example

In the example below, the state machine starts in the `FETCHING` state. It executes the `fetchResource` function, which makes an API. If the API call succeeds (i.e. the promise resolves), the machine transitions to the `SUCCESS` state; if it fails (i.e. the promise rejects), the machine transitions to the `FAILED` state.

```mermaid
stateDiagram-v2
    state root {
        [*] --> FETCHING
        FETCHING --> SUCCESS: done (resolved)
        FETCHING --> FAILED: error (rejected)
    }
    note right of FETCHING
        Activity state: executes 'fetchResource'
    end note
```

<<< ./examples/activity-states.ts