# Final States

**Final states** represent the conclusion of a sub-flow within a [nested state](./nested-states.md). They signal that a particular process has either completed successfully or failed. In Picomachine, you declare a final state by setting its *type* to either `done` (to indicate **success**) or `error` (to indicate **failure**). When a final state is entered, it automatically triggers an event—either `done` or `error`—on the parent state. This mechanism enables the parent state to react appropriately based on the outcome of its child state machine.

::: details REFERENCE
* [StateConfig](../reference/picomachine.stateconfig.md)
:::

## Syntax

Within a **nested state** configuration, you can define how the parent should respond to its final states:

```js
//...
  type: "nested",
  // When a 'done' final state is reached, transition to SOMEWHERE.
  done: { target: "SOMEWHERE" },
  // When an 'error' final state is reached, transition to SOMEWHERE_ELSE.
  error: { target: "SOMEWHERE_ELSE" },
  states: {
    //...
    ERROR: { type: "error" }, // Final state indicating a failure. 
    DONE: { type: "done" }, // Final state indicating success.
  }
//...
```

## Example

In this example, the state machine handles a **file deletion confirmation process**. The process is modeled as follows:
* The machine starts in the parent state `DELETE_OPERATION`, which contains a nested state machine.
* The nested state machine starts in the `CONFIRM_DELETE` state, where the user is prompted with a confirmation dialog.
* Based on the user’s decision:
  * If the user confirms deletion (via the `confirm` event), the machine transitions to the `DELETED` state (a final state of type `done`).
	* If the user cancels deletion (via the `cancel` event), the machine transitions to the `CANCELLED` state (a final state of type `error`).
* At the parent level, final events from the nested state are handled as follows:
  * A `done` event (from reaching `DELETED`) triggers a transition to the `NOTIFY_SUCCESS` state.
  *	An `error` event (from reaching `CANCELLED`) triggers a transition to the `NOTIFY_FAILURE` state.

```mermaid
stateDiagram-v2
    state root {
      [*] --> DELETE_OPERATION
      state DELETE_OPERATION {
        [*] --> CONFIRM_DELETE
        CONFIRM_DELETE --> DELETED: confirm
        CONFIRM_DELETE --> CANCELLED: cancel
      }
      DELETE_OPERATION --> NOTIFY_SUCCESS: done (if DELETED)
      DELETE_OPERATION --> NOTIFY_FAILURE: error (if CANCELLED)
    }
```
<<< ./examples/final-states.ts

## Advanced

If a parent state has multiple final states of the same type, you can use guarded transitions to decide the next target based on the final state’s name. For example:

```js
// ...
PARENT: {
  done: [
    { 
      target: "OUTPUT_1", 
      guard: (finalStateName) => finalStateName === "MY_DONE_1" 
    },
    { 
      target: "OUTPUT_2", 
      guard: (finalStateName) => finalStateName === "MY_DONE_2"
    },
  ],
  // ... 
}
// ...
```

::: info
The parent evaluates the *done* transitions in order. The first guard function that returns `true` determines the next state.
:::