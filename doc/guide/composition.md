# Composition

Picomachine enables you to construct complex state machines by composing smaller, modular pieces.

You can combine multiple configuration objects into a single state machine using the `"external"` configuration type. This type lets you reference external configuration objects via the `source` property and merge them seamlessly into a unified configuration.

::: info
Transitions and actions can be defined in the parent configuration, the child configurations, or both. When they’re defined at multiple levels, Picomachine automatically merges them.
:::

::: warning
In a child configuration, you can only define self-transitions because the child is unaware of its sibling configurations. To create transitions between different children (i.e. siblings), you must define those transitions in the parent configuration.
:::

::: details REFERENCE
* [StateConfig](../reference/picomachine.stateconfig.md)
:::

## Exemple

In this example, the parent configuration object `root` incorporates two child configurations (`configA` and `configB`) via the `source` property. 

```ts
// Assume these configs are defined elsewhere in your codebase.
const configA = {
    name:"A",
    // ... configuration for stateA
} as const satifies StateConfig; // Mandaroty to enable type inference

const configB = {
    name:"B",
    // ... configuration for stateB
} as const satifies StateConfig; // Mandaroty to enable type inference

// Combine stateA and stateB under the root machine.
const machine = new StateMachine({
  name: "root",
  type: "nested",
  initial: "A",
  states: {
    A: {
      type: "external"
      source: configA, 
    },
    B: {
      type: "external"
      source: configB,
    }
  }
});
```