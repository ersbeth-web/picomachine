# Distributed Transitions

**Distributed transitions** help reduce code duplication when the same event should trigger a transition to a specific target from every sibling state. Instead of defining the same event and transition configuration in each sibling state, you can define it once at the parent level using the `children.` prefix. This tells Picomachine to automatically distribute the transition to all child states.

::: details REFERENCE
* [TransitionConfig](../reference/picomachine.transitionconfig.md)
:::

## Syntax

Using a distributed transition looks like this:

```js
// with distributed transition
//...
events: {
    toD: { target: "children.D" },
},
states: {
    A: { 
        //...
    },
    B: { 
        //... 
    },
    C: { 
        //...
    },
    D: { 
        //... 
    }
}
//...
```

Without distributed transitions, you would need to duplicate the event configuration in each child state:

```js
// without distributed transition
//...
states: {
    A: {
        //...
        events: {
            toD: {target: "D"}
        }
    },
    B: {
        //...
        events: {
            toD: {target: "D"}
        }
    },
    C: {
        //...
        events: {
            toD: {target: "D"}
        }
    },
    D: { 
        //... 
    }
}
//...
```


## Example

Below is a complete example demonstrating distributed transitions. In this example, the state machine is defined with four child states: `A`, `B`, `C`, and `D`. The parent state is configured to handle the event `toD` using the distributed syntax. When the event is sent, regardless of whether the machine is in state `A`, `B`, or `C`, it will transition to state `D`.

<<< ./examples/distributed-transitions.ts

