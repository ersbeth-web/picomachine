# Linter

The [**Picomachine VS Code Extension**](https://marketplace.visualstudio.com/items?itemName=Ersbeth.picomachinelinter&ssr=false#overview) brings **Picomachine’s** linter directly into your editor. When you work with Picomachine `StateConfig` objects, the extension automatically validates your code and displays clear error messages to help you catch issues early.

![example](./linter.gif)

The extension is designed to work best when used in combination with TypeScript. For optimal results, you should declare your state machine configuration with the `as const satisfies StateConfig` syntax (see [TypeScript integration](./typescript.md)):

```ts
const config = {
  name: "root",
  type: "nested",
  initial: "A",
  states: {
    A: { 
      type: "atom", 
      events: { FINISH: { target: "DONE" } } 
    },
    DONE: { type: "done" }
  }
} as const satisfies StateConfig;
```

Using `as const satisfies StateConfig` preserves literal types and allows Picomachine to extract the available event and state names accurately.

## Quick Start

After installing the extension from the Visual Studio Code Marketplace, simply open any file that contains a Picomachine configuration. The extension will automatically run its linter and immediately highlight any errors directly in your editor. 

## Key Features

- **Real-Time Error Checking:**  
  The extension continuously validates your Picomachine configuration as you type, so you get immediate feedback.

- **Inline Error Display:**  
  Errors are highlighted directly in your editor with swiggles, and hovering over the marked code reveals detailed error messages.

- **Syntax Highlighting:**  
  Custom syntax highlighting for Picomachine `StateConfig` objects helps you visually distinguish key parts of your configuration.