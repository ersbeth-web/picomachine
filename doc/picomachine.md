# What is PicoMachine?

**Picomachine** is a lightweight yet powerful library for building [finite state machines](./concepts/statemachine.md) and [statecharts](./concepts/statechart.md) in TypeScript. It provides a minimal, developer-friendly API that enables you to model complex application logic in a predictable and maintainable way.

## Overview

PicoMachine defines the behavior of your application using three core concepts:
- **States:** Represent distinct conditions or modes of your application. Each state can have its own entry and exit actions.
- **Events:** Serve as triggers (such as user actions or system signals) that prompt the state machine to evaluate transitions.
- **Transitions:** Define how the machine moves from one state to another when a specific event occurs, optionally using guard conditions and actions.

By combining these elements, PicoMachine helps you implement robust state management with features like nested states, parallel states, asynchronous operations, and more—all while keeping the API simple and efficient.

## Key Features

- **Hooks for Side Effects:** Execute custom actions when entering, exiting, or transitioning between states.
- **Nested States:** Organize your state logic into hierarchies for better structure and maintainability.
- **Parallel States:** Run multiple state machines concurrently.
- **Transient States:** Automatically trigger transitions upon state entry.
- **Multiple Transitions:** Support several transitions for a single event.
- **Guarded Transitions:** Conditionally execute transitions based on runtime checks.
- **Delayed Transitions:** Postpone transitions by a specified duration.
- **Async (Promise-Based) States:** Handle asynchronous operations effortlessly.
- **History:** Remember the last active substate when re-entering a nested state.
- **Child/Parent Communication:** Allow states to communicate with their parent or sibling states.
- **Composition:** Easily split, compose, and reuse parts of your state machines.

## Production Ready

[![Pipeline Status](https://gitlab.com/ersbeth-web/picomachine/badges/main/pipeline.svg)](https://gitlab.com/ersbeth-web/picomachine/-/pipelines)

[![Coverage Report](https://gitlab.com/ersbeth-web/picomachine/badges/main/coverage.svg)](https://gitlab.com/ersbeth-web/picomachine/-/pipelines)  

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)  

Picomachine is designed for production use:
- **Continuous Integration:** Our [pipeline status](https://gitlab.com/ersbeth-web/picomachine/-/pipelines) shows that all tests are passing consistently.
- **High Test Coverage:** Our comprehensive test suite is reflected in our [coverage report](https://gitlab.com/ersbeth-web/picomachine/-/pipelines), ensuring reliability and maintainability.
- **Adopted by Industry:** companies such as [Dioxygen Software](https://www.dioxygen.io/) trust PicoMachine in their production systems, demonstrating its robustness and performance.

## Why PicoMachine?

PicoMachine was created to address the need for a complex logic management solution that is both powerful and minimal. While there are several excellent state machine libraries available for JavaScript (such as [Xstate](https://xstate.js.org/), [Robot](https://thisrobot.life/), ...), PicoMachine stands out for its:
- **Simplicity:** It focuses on essential features without unnecessary overhead.
- **Ease of Use:** Its clean API design (inspired by [Xstate](https://xstate.js.org/)) makes it easy to integrate and understand.
- **Performance:** With a lightweight footprint, PicoMachine is optimized for both browser and Node.js environments.
- **Comprehensive Statechart Support:** PicoMachine is designed to cover all essential aspects of statechart modeling

Our goal is to provide a reliable and efficient tool that developers can trust for managing complex application logic in production environments.

## What’s Next?

1. **Explore the Theory:**  
   Learn the fundamentals of state machines and statecharts in our [Concepts](./concepts/statemachine.md) section.
2. **Get Started:**  
   Follow our [Getting Started](./guide/getting-started.md) guide to quickly set up and experiment with PicoMachine.
3. **Master PicoMachine:**  
   Dive into practical examples and best practices in our comprehensive [Guide](./guide/statemachine.md).
4. **Consult the API Reference:**  
   For detailed information on every function, configuration, and class, refer to our [API Reference](./reference/index.md).

Embrace simplicity and reliability with PicoMachine—your state management solution for modern TypeScript applications.