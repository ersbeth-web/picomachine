import { defineConfig } from "vitepress";
import { withMermaid } from "vitepress-plugin-mermaid";

// https://vitepress.dev/reference/site-config
export default withMermaid({
    base: "/picomachine/",
    title: "PicoMachine",
    description: "PicoMachine documentation",
    themeConfig: {
        // https://vitepress.dev/reference/default-theme-config
        logo: "/logo.png",
        nav: [
            { text: "Home", link: "/" },
            { text: "Concepts", link: "/concepts/statemachine" },
            { text: "Guide", link: "/guide/getting-started" },
            { text: "Reference", link: "/reference/index" },
        ],

        sidebar: [
            { text: "What is PicoMachine?", link: "/picomachine" },
            {
                text: "Concepts",
                items: [
                    { text: "State Machines", link: "/concepts/statemachine" },
                    { text: "StateCharts", link: "/concepts/statechart" },
                    { text: "Usage", link: "/concepts/usage" },
                ],
            },
            {
                text: "Basic Guide",
                items: [
                    { text: "Getting started", link: "/guide/getting-started" },
                    { text: "StateMachine", link: "/guide/statemachine" },
                    {
                        text: "Event Transitions",
                        link: "/guide/event-transitions",
                    },
                    { text: "Actions", link: "/guide/actions" },
                    { text: "Nested States", link: "/guide/nested-states" },
                    { text: "Final States", link: "/guide/final-states" },
                    { text: "History States", link: "/guide/history-states" },
                    { text: "Parallel States", link: "/guide/parallel-states" },
                    { text: "Activity States", link: "/guide/activity-states" },
                ],
            },
            {
                text: "Advanced Guide",
                items: [
                    { text: "Composition", link: "/guide/composition" },
                    {
                        text: "Self Transitions",
                        link: "/guide/self-transitions",
                    },
                    {
                        text: "Guarded Transitions",
                        link: "/guide/guarded-transitions",
                    },
                    {
                        text: "Delayed Transitions",
                        link: "/guide/delayed-transitions",
                    },
                    {
                        text: "Transient Transitions",
                        link: "/guide/transient-transitions",
                    },
                    {
                        text: "Distributed Transitions",
                        link: "/guide/distributed-transitions",
                    },
                    { text: "Trigger", link: "/guide/trigger" },
                ],
            },
            {
                text: "Reference",
                items: [
                    { text: "Linter", link: "/linter" },
                    { text: "TypeScript", link: "/typescript" },
                    { text: "API", link: "/reference/index" },
                ],
            },
        ],

        socialLinks: [
            {
                icon: "gitlab",
                link: "https://gitlab.com/ersbeth-web/picomachine",
            },
        ],
    },
});
