# What's a Statechart?

**Statecharts** extend the basic **state machine** model to handle complex, real-world processes in a clear and organized way. While a state machine defines a system with a limited set of states and transitions triggered by events, statecharts add three powerful capabilities:

- **Nested States:** Group related states together within a parent state.
- **Parallel States:** Allow multiple state machines to run concurrently.
- **History States:** Remember the last active substate when re-entering a parent state.

These features enable you to model processes like multi-step workflows or concurrent operations with ease.

## Nested States

**Nested**(or **hierarchical**) **states** let you organize your state machine into **parent** and **child** states. This helps keep the top-level structure clean and makes it easier to manage related states as a single unit.

### Example

Imagine you're building a checkout process with several stages. Instead of listing every step at the top level, you can group them under a single `Checkout` state:

```mermaid
stateDiagram-v2
    [*] --> Checkout
    state Checkout {
        [*] --> Billing
        Billing --> Shipping: NEXT
        Shipping --> Review: NEXT
        Review --> Billing: BACK
    }
```

## History States
**History states** allow a parent state to remember its **last** active substate. This is particularly useful when a process is interrupted and later resumed, ensuring the user returns to the correct step.

### Example

Consider a scenario where a user leaves the checkout process and then returns. A **history state** ensures that the machine **resumes** at the last step (e.g., `Shipping` or `Review`) rather than starting over.

```mermaid
stateDiagram-v2
    [*] --> OUTSIDE_CHECKOUT

    OUTSIDE_CHECKOUT --> CHECKOUT: goToCheckout
    CHECKOUT --> OUTSIDE_CHECKOUT: leaveCheckout

    state CHECKOUT {
        [*] --> BILLING
        BILLING --> SHIPPING: next
        SHIPPING --> REVIEW: next
        REVIEW --> BILLING: back
    }

    note right of CHECKOUT
        History enabled: re-enters last active substate
    end note
```

## Parallel States

**Parallel states** let multiple state machines run at the same time. This is useful when different parts of your application operate independently

### Example

Suppose your application needs to manage user authentication and notifications concurrently. Here’s how that might look:

```mermaid
stateDiagram-v2
    [*] --> AUTH
    [*] --> NOTIFICAITONS

    state AUTH {
        [*] --> LOGGED_OUT
        LOGGED_OUT --> LOGGED_IN: loginSuccess
        LOGGED_IN --> LOGGED_OUT: logout
    }

    state NOTIFICAITONS {
        [*] --> IDLE
        IDLE --> FETCHING: fetchNotifs
        FETCHING --> IDLE: fetchSuccess
        FETCHING --> ERROR: fetchFailure
        ERROR --> IDLE: retry
    }
```

## Why use statecharts?

Statecharts take the predictability and structure of basic state machines and extend them to manage complex flows. They allow you to:
* **Reduce Complexity:** Break down intricate workflows into nested, manageable parts.
* **Improve Clarity:** Visualize your application’s behavior with clear, structured diagrams.
* **Enhance Maintainability:** Keep your code organized and make changes in a controlled, modular way.

Whether you’re orchestrating a multi-step process or managing concurrent operations, statecharts provide the tools to build reliable and maintainable applications.

In the next section, we will explore real-world examples that demonstrate how statecharts benefit complex scenarios.