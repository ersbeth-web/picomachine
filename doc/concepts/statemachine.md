# What's a State Machine?

A **state machine** is a model that describes the behavior of a system in terms of a limited set of **states**, **events**, and **transitions**. It ensures that your system is always in one clear, defined state, and it only changes state when a specific event occurs.

Imagine a coffee machine:
- When the machine is *idle*, it waits for someone to press a button.
- Once the button is pressed, it transitions to a *brewing* state.
- After brewing, it might either return to *idle* or, if it's a fancy machine, enter a *cleaning* state.

This simple process—defining states and the rules (transitions) that govern moving between them when events occur—is the core idea behind a **state machine**.

## Key Concepts

- **State Machine:** A system that is always in one of a predefined set of states.
- **State:** A specific mode or condition of the system (e.g., `idle`, `brewing`, `cleaning`).
- **Event:** An occurrence (such as a button press or network response) that can trigger a state change.
- **Transition:** A rule that defines how the system moves from one state to another when an event occurs.

## Coffee Machine Example

Consider our coffee machine represented as a state machine. The following diagram shows three states:
- `idle`: The machine is waiting.
- `brewing`: The machine is making coffee.
- `cleaning`: The machine is cleaning itself.

An event called `BREW_REQUEST` triggers the transition from `idle` to `brewing`, and so on.

```mermaid
stateDiagram-v2
    [*] --> idle
    idle --> brewing: BREW_REQUEST
    brewing --> cleaning: BREW_DONE
    cleaning --> idle: CLEANING_DONE
```

## Why Use a State Machine?

By organizing your logic into states and transitions, a state machine helps you:
* **Predict** how your system will behave when events occur.
* **Maintain** your code more easily, with a clear, centralized design.
* **Communicate** your application’s flow clearly to your team.

This structured approach simplifies debugging and helps catch missing logic or edge cases early in the design process.

## Limitations

**State machines** are powerful, but when your application requires advanced features like nested states, parallel processing, or history tracking, you might need to explore **statecharts**. In the next page, we’ll explain how statecharts extend state machines to handle these complex scenarios.
