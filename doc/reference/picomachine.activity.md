<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [@ersbeth/picomachine](./picomachine.md) &gt; [Activity](./picomachine.activity.md)

## Activity type

Represents an asynchronous function that performs side effects.

**Signature:**

```typescript
export type Activity = () => Promise<any>;
```

## Remarks

An activity function performs a side effect and returns a promise.

