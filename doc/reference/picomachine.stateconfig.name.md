<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [@ersbeth/picomachine](./picomachine.md) &gt; [StateConfig](./picomachine.stateconfig.md) &gt; [name](./picomachine.stateconfig.name.md)

## StateConfig.name property

Name of the root state.

**Signature:**

```typescript
name?: string;
```

## Remarks

The `name` property should only be defined for the root state. Children states must not define this property.

