<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [@ersbeth/picomachine](./picomachine.md) &gt; [TransitionConfig](./picomachine.transitionconfig.md) &gt; [delay](./picomachine.transitionconfig.delay.md)

## TransitionConfig.delay property

An optional delay (in milliseconds) before executing the transition.

**Signature:**

```typescript
delay?: number;
```
