/// <reference types="@vitest/browser/providers/playwright" />

import { defineConfig } from "vitest/config";
import PACKAGE from "./package.json" with { type: "json" };

export default defineConfig({
    test: {
        alias: {
            // import source file instead of package
            [PACKAGE.name]: new URL(PACKAGE.source, import.meta.url).pathname,
        },
        include: ["**/*.test.ts"],
        coverage: {
            reporter: ["text"],
            include: ["src/**/*"],
        },
        browser: {
            enabled: false,
            provider: "playwright",
            instances: [{ browser: "chromium" }],
        },
    },
});
