import type { StateConfig } from "../src/parser";
import { StateMachine } from "../src/statemachine";

const actions = {
    action1: () => {},
    action2: () => {},
    action3: () => {},
};

const guards = {
    guard1: () => true,
    guard2: () => true,
};

const activities = {
    activity1: () => Promise.resolve(),
};

const externalConfig = {
    name: "external",
    type: "error",
} as const satisfies StateConfig;

const bigmachine = new StateMachine({
    name: "test",
    type: "nested",
    initial: "A",
    states: {
        A: {
            type: "parallel",
            events: {
                toA: { target: "internal", actions: [actions.action1] },
                toB: { target: "B", actions: actions.action2 },
            },
            states: {
                A1: {
                    type: "async",
                    activity: activities.activity1,
                    always: [
                        {
                            target: "internal",
                            guard: guards.guard1,
                            actions: [actions.action3],
                        },
                        { target: "self" },
                    ],
                    done: [{ target: "self" }],
                    error: [{ target: "internal" }],
                    events: {
                        TO_A: [{ target: "internal" }],
                        TO_B: { target: "self" },
                    },
                },
                A2: {
                    type: "external",
                    source: externalConfig,
                },
            },
        },
        B: {
            // name: "test",
            type: "nested",
            initial: "B1",
            events: {
                TO_B3: { distribute: true, target: "B3" },
            },
            states: {
                B1: {
                    type: "async",
                    always: [{ target: "self" }],
                    done: [{ target: "self" }],
                    error: { target: "self" },
                    activity: activities.activity1,
                },
                B2: {
                    type: "atom",
                    events: {
                        TO_A: [{ target: "B1" }],
                    },
                },
                B3: {
                    type: "async",
                    events: {
                        TO_A: { target: "B1" },
                    },
                    activity: activities.activity1,
                },
                B4: {
                    type: "async",
                    events: {
                        TO_A: { target: "B1" },
                    },
                    activity: () => Promise.resolve(),
                },
                B5: {
                    type: "external",
                    source: externalConfig,
                },
            },
        },
    },
} as const satisfies StateConfig);

bigmachine.send("TO_B");

function createA() {
    const A = {
        name: "A",
        type: "nested",
        initial: "B",
        states: {
            B: {
                type: "atom",
                events: {
                    FINISH_C: { target: "DONE_C" },
                    FINISH_D: { target: "DONE_D" },
                },
            },
            DONE_C: { type: "done" },
            DONE_D: { type: "done" },
        },
    } as const satisfies StateConfig;
    return A;
}

const machine = new StateMachine({
    name: "root",
    type: "nested",
    initial: "A",
    states: {
        A: {
            type: "external",
            source: createA(),
            done: [
                { target: "C", guard: (data: any) => data === "DONE_C" },
                { target: "D", guard: (data: any) => data === "DONE_D" },
            ],
        },
        C: {
            type: "atom",
            events: { TO_A: { target: "A" } },
        },
        D: { type: "atom" },
    },
} as const satisfies StateConfig);

// HERE IS THE TS ERROR
machine.status.matches("root.A.B"); //Argument of type '"root.A.B"' is not assignable to parameter of type 'RegExp | "A" | "C" | "D" | "A.B" | "A.DONE_C" | "A.DONE_D"'.ts(2345)
machine.send("FINISH_D");
