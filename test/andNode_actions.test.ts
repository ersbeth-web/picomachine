import { type StateConfig, StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("enter actions order", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        enter: () => log.push("enter root"),
        type: "parallel",
        states: {
            A: { enter: () => log.push("enter A"), type: "atom" },
            B: { enter: () => log.push("enter B"), type: "atom" },
            C: { enter: () => log.push("enter C"), type: "atom" },
        },
    });

    const machine = root;

    machine.start();
    expect(log).toEqual(["enter root", "enter A", "enter B", "enter C"]);
});

test("exit actions order", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "BASE",
        states: {
            BASE: {
                exit: () => log.push("exit root"),
                events: {
                    EXIT: {
                        target: "END",
                    },
                },
                type: "parallel",
                states: {
                    A: { exit: () => log.push("exit A"), type: "atom" },
                    B: { exit: () => log.push("exit B"), type: "atom" },
                    C: { exit: () => log.push("exit C"), type: "atom" },
                },
            },
            END: { type: "atom" },
        },
    });

    const machine = root;

    machine.start();
    machine.send("EXIT");
    expect(log).toEqual(["exit A", "exit B", "exit C", "exit root"]);
});

test("raise event", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "parallel",
        events: {
            RAISED: { target: "self", actions: () => log.push("RAISED") },
        },
        states: {
            A: { type: "atom" },
            B: {
                events: {
                    RAISE: {
                        target: "self",
                        actions: "RAISED",
                    },
                },
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    machine.send("RAISE");
    await new Promise<void>((resolve) => setTimeout(() => resolve(), 0)); // wait until next tick
    expect(log).toEqual(["RAISED"]);
});

test("composed: raise event", async () => {
    const log: string[] = [];

    const B = {
        name: "B",
        type: "atom",
        events: {
            RAISE: {
                target: "self",
                actions: "RAISED",
            },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "parallel",
        events: {
            RAISED: { target: "self", actions: () => log.push("RAISED") },
        },
        states: {
            A: { type: "atom" },
            B: {
                type: "external",
                source: B,
            },
        },
    } as const satisfies StateConfig);
    const machine = root;

    machine.start();
    machine.send("RAISE");
    await new Promise<void>((resolve) => setTimeout(() => resolve(), 0)); // wait until next tick
    expect(log).toEqual(["RAISED"]);
});

test("composed: enter/exit actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
        type: "parallel",
        states: {
            E: { type: "atom" },
            D: { type: "atom" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("TO_B");
    expect(log).toEqual([
        "enter OUTSIDE",
        "enter INSIDE",
        "exit INSIDE",
        "exit OUTSIDE",
    ]);
});

test("composed: events actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        events: {
            SELF: { target: "self", actions: () => log.push("SELF") },
        },
        type: "parallel",
        states: {
            E: { type: "atom" },
            D: { type: "atom" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        events: {
                            TO_B: {
                                target: "B",
                                actions: () => log.push("TO_B"),
                            },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("SELF");
    machine.send("TO_B");
    expect(log).toEqual(["SELF", "TO_B"]);
});

test("composed: auto actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "parallel",
        always: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
        states: {
            E: { type: "atom" },
            D: { type: "atom" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "C",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        always: {
                            target: "B",
                            actions: () => log.push("OUTSIDE"),
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});

test("composed: done actions defined inside and outside", async () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "parallel",
        states: {
            E: {
                type: "async",
                activity: () => Promise.resolve(),
            },
            D: {
                type: "async",
                activity: () => Promise.resolve(),
            },
        },
        done: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "C",
                states: {
                    C: {
                        type: "external",
                        source: C,
                        done: {
                            target: "B",
                            actions: () => log.push("OUTSIDE"),
                        },
                    },
                    B: { type: "atom" },
                },
            },
        },
    });

    const machine = root;
    machine.start();
    await machine.wait("root.A.B");
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});

test("composed: error actions defined inside and outside", async () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "parallel",
        states: {
            E: {
                type: "async",
                activity: () => Promise.resolve(),
            },
            D: {
                type: "async",
                activity: () => Promise.reject(),
            },
        },
        error: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "C",
                states: {
                    C: {
                        type: "external",
                        source: C,
                        error: {
                            target: "B",
                            actions: () => log.push("OUTSIDE"),
                        },
                    },
                    B: { type: "atom" },
                },
            },
        },
    });

    const machine = root;
    machine.start();
    await machine.wait("root.A.B");
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});
