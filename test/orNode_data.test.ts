import { StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("event data sending (single)", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            NEXT: {
                                target: "C",
                                actions: (data) => log.push(data),
                            },
                        },
                    },
                    C: {
                        type: "atom",
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("NEXT", "data");
    expect(log).toEqual(["data"]);
});

test("event data sending (multiple)", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            NEXT: {
                                target: "C",
                                actions: [
                                    (data) => log.push(data.data1),
                                    (data) => log.push(data.data2),
                                ],
                            },
                        },
                    },
                    C: {
                        type: "atom",
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("NEXT", { data1: "data1", data2: "data2" });
    expect(log).toEqual(["data1", "data2"]);
});
