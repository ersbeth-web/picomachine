import { type StateConfig, StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("Or Node: Wrong transition target", () => {
    expect(
        () =>
            new StateMachine({
                name: "root",
                type: "nested",
                initial: "A",
                states: {
                    A: { type: "atom", events: { TO_C: { target: "C" } } },
                    B: { type: "atom" },
                },
            }),
    ).toThrow();
});

test("Or Node: Wrong initial state", () => {
    expect(
        () =>
            new StateMachine({
                name: "root",
                type: "nested",
                initial: "C",
                states: {
                    A: { type: "atom" },
                    B: { type: "atom" },
                },
            }),
    ).toThrow();
});

test("Or Node: Fail if subnode source doesn't match subnode name", () => {
    const A = {
        name: "D",
        type: "atom",
    } as const satisfies StateConfig;
    expect(
        () =>
            new StateMachine({
                name: "root",
                type: "nested",
                initial: "C",
                states: {
                    A: {
                        type: "external",
                        source: A,
                    },
                    B: { type: "atom" },
                },
            }),
    ).toThrow();
});
