import { StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("multiple error transitions", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                error: [
                    { target: "C", guard: (data) => data === "ERROR_C" },
                    { target: "D", guard: (data) => data === "ERROR_D" },
                ],
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FAIL_C: { target: "ERROR_C" },
                            FAIL_D: { target: "ERROR_D" },
                        },
                    },
                    ERROR_C: {
                        type: "error",
                    },
                    ERROR_D: {
                        type: "error",
                    },
                },
            },
            C: {
                type: "atom",
                events: {
                    TO_A: { target: "A" },
                },
            },
            D: {
                type: "atom",
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toEqual(true);
    machine.send("FAIL_C");
    expect(machine.status.matches("root.C")).toEqual(true);
    machine.send("TO_A");
    expect(machine.status.matches("root.A.B")).toEqual(true);
    machine.send("FAIL_D");
    expect(machine.status.matches("root.D")).toEqual(true);
});

test("multiple error transitions with default", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                error: [
                    { target: "C", guard: (data) => data === "ERROR" },
                    { target: "D", guard: (data) => data === "ERROR_D" },
                ],
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FAIL_C: { target: "ERROR" },
                            FAIL_D: { target: "ERROR_D" },
                        },
                    },
                    ERROR: {
                        type: "error",
                    },
                    ERROR_D: {
                        type: "error",
                    },
                },
            },
            C: {
                type: "atom",
                events: {
                    TO_A: { target: "A" },
                },
            },
            D: {
                type: "atom",
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toEqual(true);
    machine.send("FAIL_C");
    expect(machine.status.matches("root.C")).toEqual(true);
    machine.send("TO_A");
    expect(machine.status.matches("root.A.B")).toEqual(true);
    machine.send("FAIL_D");
    expect(machine.status.matches("root.D")).toEqual(true);
});
