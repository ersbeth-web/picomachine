import { StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("composed: enter/exit actions", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                error: { target: "C" },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FAIL: { target: "ERROR" },
                        },
                    },
                    ERROR: {
                        type: "error",
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                    },
                },
            },
            C: {
                type: "atom",
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("FAIL");
    expect(log).toEqual(["enter OUTSIDE", "exit OUTSIDE"]);
});
