import { type StateConfig, StateMachine } from "@ersbeth/picomachine";
//@ts-ignore
import FakeTimers from "@sinonjs/fake-timers";
import { expect, test } from "vitest";

test("activity resolves", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => Promise.resolve(),
                done: { target: "FULFILLED" },
                error: { target: "REJECTED" },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.FULFILLED");
});

test("activity rejects", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => Promise.reject(),
                done: { target: "FULFILLED" },
                error: { target: "REJECTED" },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.REJECTED", 1000);
});

test("activity: event transition", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => Promise.resolve(),
                events: {
                    CANCEL: { target: "CANCELLED" },
                },
                done: { target: "FULFILLED" },
                error: { target: "REJECTED" },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
            CANCELLED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    machine.send("CANCEL");
    await machine.wait("root.CANCELLED");
});

test("activity: auto transition", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => Promise.resolve(),
                always: { target: "CANCELLED" },
                done: { target: "FULFILLED" },
                error: { target: "REJECTED" },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
            CANCELLED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.CANCELLED");
});

test("multiple done transitions", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "async",
                done: [
                    {
                        target: "B",
                    },
                    {
                        target: "C",
                    },
                ],
                error: [
                    {
                        target: "D",
                    },
                    {
                        target: "E",
                    },
                ],
                activity: () => Promise.resolve(),
            },
            B: { type: "atom" },
            C: { type: "atom" },
            D: { type: "atom" },
            E: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.B");
});

test("multiple error transitions", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "async",
                done: [
                    {
                        target: "B",
                    },
                    {
                        target: "C",
                    },
                ],
                error: [
                    {
                        target: "D",
                    },
                    {
                        target: "E",
                    },
                ],
                activity: () => Promise.reject(),
            },
            B: { type: "atom" },
            C: { type: "atom" },
            D: { type: "atom" },
            E: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.D");
});

test("guarded done transitions", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "async",
                done: [
                    {
                        target: "B",
                        guard: () => false,
                    },
                    {
                        target: "C",
                        guard: () => true,
                    },
                ],
                error: [
                    {
                        target: "D",
                        guard: () => false,
                    },
                    {
                        target: "E",
                        guard: () => true,
                    },
                ],
                activity: () => Promise.resolve(),
            },
            B: { type: "atom" },
            C: { type: "atom" },
            D: { type: "atom" },
            E: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.C");
});

test("guarded error transitions", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "async",
                done: [
                    {
                        target: "B",
                        guard: () => false,
                    },
                    {
                        target: "C",
                        guard: () => true,
                    },
                ],
                error: [
                    {
                        target: "D",
                        guard: () => false,
                    },
                    {
                        target: "E",
                        guard: () => true,
                    },
                ],
                activity: () => Promise.reject(),
            },
            B: { type: "atom" },
            C: { type: "atom" },
            D: { type: "atom" },
            E: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.E");
});

test("delayed done transition", async () => {
    const clock = FakeTimers.install();

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "async",
                done: {
                    target: "B",
                    delay: 500,
                },
                error: {
                    target: "C",
                    delay: 500,
                },
                activity: () => Promise.resolve(),
            },
            B: { type: "atom" },
            C: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    // tick only after promise has resolved
    setTimeout(async () => {
        clock.tick(600);
        await machine.wait("root.B");
    }, 0);
    clock.uninstall();
});

test("delayed error", async () => {
    const clock = FakeTimers.install();

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "async",
                activity: () => Promise.reject(),
                done: {
                    target: "B",
                    delay: 500,
                },
                error: {
                    target: "C",
                    delay: 500,
                },
            },
            B: { type: "atom" },
            C: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    // tick only after promise has resolved
    setTimeout(async () => {
        clock.tick(600);
        await machine.wait("root.C");
    }, 0);
    clock.uninstall();
});

test("composed: done transitions defined outside", async () => {
    const C = {
        name: "C",
        type: "async",
        activity: () => Promise.resolve(),
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        done: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("TO_C");
    await machine.wait("root.A.B");
});

test("composed: error transitions defined outside", async () => {
    const C = {
        name: "C",
        type: "async",
        activity: () => Promise.reject(),
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        error: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("TO_C");
    await machine.wait("root.A.B");
});

test("composed: event transitions defined outside", () => {
    const C = {
        name: "C",
        type: "async",
        activity: () => new Promise(() => {}),
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        events: {
                            CANCEL: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBe(true);
    machine.send("CANCEL");
    expect(machine.status.matches("root.A.B")).toBe(true);
});

test("composed: auto transitions defined outside", async () => {
    const A = {
        name: "A",
        type: "async",
        activity: () => new Promise(() => {}),
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "external",
                source: A,
                always: { target: "B" },
            },
            B: { type: "atom" },
        },
    });

    const machine = root;

    machine.start();
    await machine.wait("root.B");
});
