import { StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("all states are entered", () => {
    const root = new StateMachine({
        name: "root",
        type: "parallel",
        states: {
            A: { type: "atom" },
            B: { type: "atom" },
            C: { type: "atom" },
        },
    });

    const machine = root;
    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    expect(machine.status.matches("root.B")).toBe(true);
    expect(machine.status.matches("root.C")).toBe(true);
});

test("done transition", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "BASE",
        states: {
            BASE: {
                type: "parallel",
                done: {
                    target: "END",
                },
                error: {
                    target: "FAILED",
                },
                states: {
                    A: {
                        type: "async",
                        activity: () => Promise.resolve(),
                    },
                    B: {
                        type: "async",
                        activity: () => Promise.resolve(),
                    },
                    C: { type: "atom" },
                },
            },
            END: { type: "atom" },
            FAILED: { type: "atom" },
        },
    });

    const machine = root;

    machine.start();
    await machine.wait("root.END");
});

test("error transition", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "BASE",
        states: {
            BASE: {
                type: "parallel",
                done: {
                    target: "END",
                },
                error: {
                    target: "FAILED",
                },
                states: {
                    A: {
                        type: "async",
                        activity: () => Promise.resolve(),
                    },
                    B: {
                        type: "async",
                        activity: () => Promise.reject(),
                    },
                    C: { type: "atom" },
                },
            },
            END: { type: "atom" },
            FAILED: { type: "atom" },
        },
    });

    const machine = root;

    machine.start();
    await machine.wait("root.FAILED");
});

test("root done transition", async () => {
    const root = new StateMachine({
        name: "root",
        type: "parallel",
        states: {
            A: {
                type: "async",
                activity: () => Promise.resolve(),
            },
            B: {
                type: "async",
                activity: () => Promise.resolve(),
            },
            C: { type: "atom" },
        },
    });

    const machine = root;
    const promise = new Promise<void>((resolve) =>
        machine.onDone(() => {
            resolve();
        }),
    );
    machine.start();
    await promise;
});

test("error done transition", async () => {
    const root = new StateMachine({
        name: "root",
        type: "parallel",
        states: {
            A: {
                type: "async",
                activity: () => Promise.resolve(),
            },
            B: {
                type: "async",
                activity: () => Promise.reject(),
            },
            C: { type: "atom" },
        },
    });

    const machine = root;
    const promise = new Promise<void>((resolve) =>
        machine.onError(() => {
            resolve();
        }),
    );
    machine.start();
    await promise;
});
