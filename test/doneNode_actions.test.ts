import { StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("enter/exit actions", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                done: { target: "C" },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH: { target: "DONE" },
                        },
                    },
                    DONE: {
                        type: "done",
                        enter: () => log.push("enter"),
                        exit: () => log.push("exit"),
                    },
                },
            },
            C: {
                type: "atom",
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("FINISH");
    expect(log).toEqual(["enter", "exit"]);
});
