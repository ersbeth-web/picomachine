import { type StateConfig, StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("enter is top down priority", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                enter: () => log.push("enterA"),
                initial: "B",
                states: {
                    B: {
                        type: "nested",
                        enter: () => log.push("enterB"),
                        initial: "C",
                        states: {
                            C: {
                                type: "atom",
                                enter: () => log.push("enterC"),
                            },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual(["enterA", "enterB", "enterC"]);
});

test("exit is bottom up priority", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                exit: () => log.push("exitA"),
                events: {
                    TO_D: {
                        target: "D",
                    },
                },
                initial: "B",
                states: {
                    B: {
                        type: "nested",
                        exit: () => log.push("exitB"),
                        initial: "C",
                        states: {
                            C: {
                                type: "atom",
                                exit: () => log.push("exitC"),
                            },
                        },
                    },
                },
            },
            D: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("TO_D");
    expect(log).toEqual(["exitC", "exitB", "exitA"]);
});

test("exit/event/enter action order", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                enter: () => log.push("enterA"),
                exit: () => log.push("exitA"),
                events: {
                    TO_B: {
                        target: "B",
                        actions: () => log.push("transit A->B"),
                    },
                },
            },
            B: {
                type: "nested",
                enter: () => log.push("enterB"),
                initial: "C",
                states: {
                    C: {
                        type: "atom",
                        enter: () => log.push("enterC"),
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual(["enterA"]);
    machine.send("TO_B");
    expect(log).toEqual([
        "enterA",
        "exitA",
        "transit A->B",
        "enterB",
        "enterC",
    ]);
});

test("done actions (single)", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                done: { target: "DONE", actions: () => log.push("doneB") },
                error: { target: "ERROR", actions: () => log.push("errorB") },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: { type: "done" },
                    ERROR: { type: "error" },
                },
            },
            DONE: { type: "done" },
            ERROR: { type: "error" },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("FINISH");
    expect(log).toEqual(["doneB"]);
});

test("done actions (multi)", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                done: {
                    target: "DONE",
                    actions: [
                        () => log.push("doneB"),
                        () => log.push("doneB2"),
                    ],
                },
                error: {
                    target: "ERROR",
                    actions: [
                        () => log.push("errorB"),
                        () => log.push("errorB2"),
                    ],
                },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: { type: "done" },
                    ERROR: { type: "error" },
                },
            },
            DONE: { type: "done" },
            ERROR: { type: "error" },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("FINISH");
    expect(log).toEqual(["doneB", "doneB2"]);
});

test("error actions (single)", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                done: { target: "DONE", actions: () => log.push("doneB") },
                error: { target: "ERROR", actions: () => log.push("errorB") },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: { type: "done" },
                    ERROR: { type: "error" },
                },
            },
            DONE: { type: "done" },
            ERROR: { type: "error" },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("FAIL");
    expect(log).toEqual(["errorB"]);
});

test("error actions (multi)", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                done: {
                    target: "DONE",
                    actions: [
                        () => log.push("doneB"),
                        () => log.push("doneB2"),
                    ],
                },
                error: {
                    target: "ERROR",
                    actions: [
                        () => log.push("errorB"),
                        () => log.push("errorB2"),
                    ],
                },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: { type: "done" },
                    ERROR: { type: "error" },
                },
            },
            DONE: { type: "done" },
            ERROR: { type: "error" },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("FAIL");
    expect(log).toEqual(["errorB", "errorB2"]);
});

test("auto transition: ignore subnodes", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                type: "nested",
                always: {
                    target: "END",
                    actions: () => log.push("transient TRANSIENT->END"),
                },
                initial: "A",
                states: {
                    A: {
                        type: "atom",
                        always: {
                            target: "B",
                            actions: () => log.push("transient A->B"),
                        },
                    },
                    B: {
                        type: "atom",
                    },
                },
            },
            END: {
                type: "atom",
            },
        },
    });
    const machine = root;
    machine.start();
    expect(log).toEqual(["transient TRANSIENT->END"]);
});

test("raise event", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        events: {
            RAISED: { target: "self", actions: () => log.push("RAISED") },
        },
        states: {
            A: {
                type: "atom",
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                type: "atom",
                events: {
                    RAISE: {
                        target: "self",
                        actions: "RAISED",
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    machine.send("TO_B");
    machine.send("RAISE");
    await machine.wait("root.A");
    expect(log).toEqual(["RAISED"]);
});

test("composed: raise event", async () => {
    const log: string[] = [];

    const B = {
        name: "B",
        type: "atom",
        events: {
            RAISE: {
                target: "self",
                actions: "RAISED",
            },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        events: {
            RAISED: { target: "self", actions: () => log.push("RAISED") },
        },
        states: {
            A: {
                type: "atom",
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                type: "external",
                source: B,
            },
        },
    });
    const machine = root;

    machine.start();
    machine.send("TO_B");
    machine.send("RAISE");
    await machine.wait("root.A");
    expect(log).toEqual(["RAISED"]);
});

test("composed: enter/exit actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "nested",
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
        initial: "D",
        states: {
            E: {
                type: "atom",
            },
            D: {
                type: "atom",
            },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("TO_B");
    expect(log).toEqual([
        "enter OUTSIDE",
        "enter INSIDE",
        "exit INSIDE",
        "exit OUTSIDE",
    ]);
});

test("composed: events actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "nested",
        initial: "D",
        events: {
            SELF: { target: "self", actions: () => log.push("SELF") },
        },
        states: {
            E: {
                type: "atom",
            },
            D: {
                type: "atom",
            },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        events: {
                            TO_B: {
                                target: "B",
                                actions: () => log.push("TO_B"),
                            },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("SELF");
    machine.send("TO_B");
    expect(log).toEqual(["SELF", "TO_B"]);
});

test("composed: auto actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "nested",
        always: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
        initial: "D",
        states: {
            E: {
                type: "atom",
            },
            D: {
                type: "atom",
            },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "C",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        always: {
                            target: "B",
                            actions: () => log.push("OUTSIDE"),
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});

test("composed: done actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "nested",
        initial: "D",
        done: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
        states: {
            D: {
                type: "atom",
                events: {
                    FINISH: { target: "DONE" },
                },
            },
            DONE: { type: "done" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        done: {
                            target: "B",
                            actions: [() => log.push("OUTSIDE")],
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("TO_C");
    machine.send("FINISH");
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});

test("composed: error actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "nested",
        initial: "D",
        error: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
        states: {
            D: {
                type: "atom",
                events: {
                    FAIL: { target: "ERROR" },
                },
            },
            ERROR: { type: "error" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        error: {
                            target: "B",
                            actions: [() => log.push("OUTSIDE")],
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("FAIL");
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});
