import { type StateConfig, StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("history enabled", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                events: {
                    RE_ENTER: { target: "self" },
                },
                history: true,
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                    },
                    C: {
                        type: "atom",
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
});

test("history disabled", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                events: {
                    RE_ENTER: { target: "self" },
                },
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                    },
                    C: {
                        type: "atom",
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B")).toBeTruthy();
});

test("multi level history enabled", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                events: {
                    RE_ENTER: { target: "self" },
                },
                history: true,
                initial: "B",
                states: {
                    B: {
                        type: "nested",
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                        history: true,
                        initial: "D",
                        states: {
                            D: {
                                type: "atom",
                                events: {
                                    TO_E: {
                                        target: "E",
                                    },
                                },
                            },
                            E: {
                                type: "atom",
                            },
                        },
                    },
                    C: {
                        type: "atom",
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B.D")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B.D")).toBeTruthy();
    machine.send("TO_E");
    expect(machine.status.matches("root.A.B.E")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B.E")).toBeTruthy();
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
    machine.send("TO_B");
    expect(machine.status.matches("root.A.B.E")).toBeTruthy();
});

test("multi level history disabled", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                events: {
                    RE_ENTER: { target: "self" },
                },
                initial: "B",
                states: {
                    B: {
                        type: "nested",
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                        initial: "D",
                        states: {
                            D: {
                                type: "atom",
                                events: {
                                    TO_E: {
                                        target: "E",
                                    },
                                },
                            },
                            E: {
                                type: "atom",
                            },
                        },
                    },
                    C: {
                        type: "atom",
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B.D")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B.D")).toBeTruthy();
    machine.send("TO_E");
    expect(machine.status.matches("root.A.B.E")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B.D")).toBeTruthy();
    machine.send("TO_E");
    expect(machine.status.matches("root.A.B.E")).toBeTruthy();
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
    machine.send("TO_B");
    expect(machine.status.matches("root.A.B.D")).toBeTruthy();
});

test("multi level history mixed", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                events: {
                    RE_ENTER: { target: "self" },
                },
                initial: "B",
                states: {
                    B: {
                        type: "nested",
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                        history: true,
                        initial: "D",
                        states: {
                            D: {
                                type: "atom",
                                events: {
                                    TO_E: {
                                        target: "E",
                                    },
                                },
                            },
                            E: {
                                type: "atom",
                            },
                        },
                    },
                    C: {
                        type: "atom",
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B.D")).toBeTruthy();
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B.D")).toBeTruthy();
    machine.send("TO_E");
    expect(machine.status.matches("root.A.B.E")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B.E")).toBeTruthy();
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B.E")).toBeTruthy();
});

test("true conditional history", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                events: {
                    RE_ENTER: { target: "self" },
                },
                history: () => true,
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                    },
                    C: {
                        type: "atom",
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
});

test("false conditional history", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                events: {
                    RE_ENTER: { target: "self" },
                },
                history: () => false,
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: {
                                target: "C",
                            },
                        },
                    },
                    C: {
                        type: "atom",
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B")).toBeTruthy();
});

test("composed history inside", () => {
    const A = {
        name: "A",
        type: "nested",
        events: {
            RE_ENTER: { target: "self" },
        },
        initial: "B",
        history: true,
        states: {
            B: {
                type: "atom",
                events: {
                    TO_C: {
                        target: "C",
                    },
                },
            },
            C: {
                type: "atom",
            },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "external",
                source: A,
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.B")).toBeTruthy();
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
    machine.send("RE_ENTER");
    expect(machine.status.matches("root.A.C")).toBeTruthy();
});
