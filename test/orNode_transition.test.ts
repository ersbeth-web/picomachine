import { type StateConfig, StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("initial is recursive", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "nested",
                        initial: "C",
                        states: {
                            C: { type: "atom" },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    expect(machine.status.matches("root.A.B")).toBe(true);
    expect(machine.status.matches("root.A.B.C")).toBe(true);
});

test("event transition is recursive", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                type: "nested",
                initial: "C",
                states: {
                    C: { type: "atom" },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("TO_B");
    expect(machine.status.matches("root.B")).toBe(true);
    expect(machine.status.matches("root.B.C")).toBe(true);
});

test("subnodetransition keeps parent unchanged", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                type: "nested",
                events: {
                    TO_A: { target: "A" },
                },
                initial: "C",
                states: {
                    C: {
                        type: "atom",
                        events: {
                            TO_D: { target: "D" },
                        },
                    },
                    D: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("TO_B");
    expect(machine.status.matches("root.B")).toBe(true);
    expect(machine.status.matches("root.B.C")).toBe(true);
    machine.send("TO_D");
    expect(machine.status.matches("root.B")).toBe(true);
    expect(machine.status.matches("root.B.D")).toBe(true);
});

test("exiting is recursive", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    TO_B: { target: "B" },
                },
            },
            B: {
                type: "nested",
                events: {
                    TO_A: { target: "A" },
                },
                initial: "C",
                states: {
                    C: { type: "atom" },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("TO_B");
    expect(machine.status.matches("root.B")).toBe(true);
    expect(machine.status.matches("root.B.C")).toBe(true);
    machine.send("TO_A");
    expect(machine.status.matches("root.A")).toBe(true);
});

test("Most outer parent takes transition.", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                events: {
                    NEXT: { target: "D" },
                },
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            NEXT: {
                                target: "C",
                            },
                        },
                    },
                    C: { type: "atom" },
                },
            },
            D: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("NEXT");
    expect(machine.status.matches("root.D")).toBe(true);
});

test("done transition", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                done: { target: "DONE" },
                error: { target: "ERROR" },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: { type: "done" },
                    ERROR: { type: "error" },
                },
            },
            DONE: { type: "done" },
            ERROR: { type: "error" },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("FINISH");
    expect(machine.status.matches("root.DONE")).toBe(true);
});

test("error transition", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                done: { target: "DONE" },
                error: { target: "ERROR" },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: { type: "done" },
                    ERROR: { type: "error" },
                },
            },
            DONE: { type: "done" },
            ERROR: { type: "error" },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("FAIL");
    expect(machine.status.matches("root.ERROR")).toBe(true);
});

test("auto transition: ignore subnode", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                type: "nested",
                always: {
                    target: "END",
                },
                initial: "A",
                states: {
                    A: { type: "atom" },
                },
            },
            END: { type: "atom" },
        },
    });
    const machine = root;
    machine.start();
    expect(machine.status.matches("root.END")).toBe(true);
});

test("composed: events transitions defined outside", () => {
    const C = {
        name: "C",
        type: "nested",
        initial: "D",
        states: {
            E: { type: "atom" },
            D: { type: "atom" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C.D")).toBe(true);
    machine.send("TO_B");
    expect(machine.status.matches("root.A.B")).toBe(true);
});

test("composed: auto transitions defined outside", () => {
    const C = {
        name: "C",
        type: "nested",
        initial: "D",
        states: {
            E: { type: "atom" },
            D: { type: "atom" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "C",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        always: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    expect(machine.status.matches("root.A.B")).toBe(true);
});

test("composed: done transitions defined outside", () => {
    const C = {
        name: "C",
        type: "nested",
        initial: "D",
        states: {
            D: {
                type: "atom",
                events: {
                    FINISH: { target: "DONE" },
                },
            },
            DONE: { type: "done" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        done: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C.D")).toBe(true);
    machine.send("FINISH");
    expect(machine.status.matches("root.A.B")).toBe(true);
});

test("composed: error transitions defined outside", () => {
    const C = {
        name: "C",
        type: "nested",
        initial: "D",
        states: {
            D: {
                type: "atom",
                events: {
                    FAIL: { target: "ERROR" },
                },
            },
            ERROR: { type: "error" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        error: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C.D")).toBe(true);
    machine.send("FAIL");
    expect(machine.status.matches("root.A.B")).toBe(true);
});

test("composed: events transitions (parallel external)", () => {
    const C = {
        name: "C",
        type: "parallel",
        states: {
            E: { type: "atom" },
            D: { type: "atom" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C.D")).toBe(true);
    expect(machine.status.matches("root.A.C.E")).toBe(true);
    machine.send("TO_B");
    expect(machine.status.matches("root.A.B")).toBe(true);
});

test("composed: auto transitions defined outside", () => {
    const C = {
        name: "C",
        type: "parallel",
        states: {
            E: { type: "atom" },
            D: { type: "atom" },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "C",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        always: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
});

test("internal transition doesn't affect subnodes", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                history: false,
                events: {
                    SELF: {
                        target: "internal",
                    },
                },
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: { type: "atom" },
                },
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBe(true);
    machine.send("SELF");
    expect(machine.status.matches("root.A.C")).toBe(true);
});

test("multiple done levels", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "nested",
                        initial: "C",
                        done: { target: "DONE" },
                        states: {
                            C: {
                                type: "atom",
                                events: {
                                    FINISH: { target: "DONE" },
                                },
                            },
                            DONE: { type: "done" },
                        },
                    },
                    DONE: { type: "done" },
                },
            },
            DONE: { type: "done" },
        },
    });
    const machine = root;

    const log: string[][] = [];
    machine.onChange((status) => log.push(status.states));
    machine.start();
    machine.send("FINISH");
    expect(log).toEqual([
        ["root"],
        ["root.A"],
        ["root.A.B"],
        ["root.A.B.C"],
        ["root.A.B.DONE"],
        ["root.A.DONE"],
    ]);
});
