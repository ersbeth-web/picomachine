import { type StateConfig, StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

function activity(shouldResolve: boolean) {
    return new Promise<void>((resolve, reject) => {
        if (shouldResolve) {
            resolve();
        } else {
            reject();
        }
    });
}

test("transition actions order on resolve (single)", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => activity(true),
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done PENDING"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error PENDING"),
                },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.FULFILLED");
    expect(log).toStrictEqual(["done PENDING"]);
});

test("transition actions order on resolve (multi)", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => activity(true),
                done: {
                    target: "FULFILLED",
                    actions: [
                        () => log.push("done PENDING"),
                        () => log.push("done PENDING2"),
                    ],
                },
                error: {
                    target: "REJECTED",
                    actions: [
                        () => log.push("error PENDING"),
                        () => log.push("error PENDING2"),
                    ],
                },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.FULFILLED");
    expect(log).toEqual(["done PENDING", "done PENDING2"]);
});

test("transition actions order on reject (single)", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => activity(false),
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done PENDING"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error PENDING"),
                },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.REJECTED");
    expect(log).toEqual(["error PENDING"]);
});

test("transition actions order on reject (multi)", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => activity(false),
                done: {
                    target: "FULFILLED",
                    actions: [
                        () => log.push("done PENDING"),
                        () => log.push("done PENDING2"),
                    ],
                },
                error: {
                    target: "REJECTED",
                    actions: [
                        () => log.push("error PENDING"),
                        () => log.push("error PENDING2"),
                    ],
                },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.REJECTED");
    expect(log).toEqual(["error PENDING", "error PENDING2"]);
});

test("enter/exit/transition actions order on resolve", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                enter: () => log.push("enterPENDING"),
                exit: () => log.push("exitPENDING"),
                activity: () => activity(true),
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done PENDING"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error PENDING"),
                },
            },
            FULFILLED: {
                type: "atom",
                enter: () => log.push("enterFULFILLED"),
            },
            REJECTED: { type: "atom", enter: () => log.push("enterREJECTED") },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.FULFILLED");
    expect(log).toEqual([
        "enterPENDING",
        "exitPENDING",
        "done PENDING",
        "enterFULFILLED",
    ]);
});

test("enter/exit/transition actions order on reject", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                enter: () => log.push("enterPENDING"),
                exit: () => log.push("exitPENDING"),
                activity: () => activity(false),
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done PENDING"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error PENDING"),
                },
            },
            FULFILLED: {
                type: "atom",
                enter: () => log.push("enterFULFILLED"),
            },
            REJECTED: { type: "atom", enter: () => log.push("enterREJECTED") },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.REJECTED");
    expect(log).toEqual([
        "enterPENDING",
        "exitPENDING",
        "error PENDING",
        "enterREJECTED",
    ]);
});

test("activity: event action when resolves", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => Promise.resolve(),
                events: {
                    CANCEL: {
                        target: "CANCELLED",
                        actions: () => log.push("CANCEL"),
                    },
                },
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error"),
                },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
            CANCELLED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    machine.send("CANCEL");
    await machine.wait("root.CANCELLED");
    expect(log).toEqual(["CANCEL"]);
});

test("activity: event action when rejects", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => Promise.reject(),
                events: {
                    CANCEL: {
                        target: "CANCELLED",
                        actions: () => log.push("CANCEL"),
                    },
                },
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error"),
                },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
            CANCELLED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    machine.send("CANCEL");
    await machine.wait("root.CANCELLED");
    expect(log).toEqual(["CANCEL"]);
});

test("activity: auto action ", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "PENDING",
        states: {
            PENDING: {
                type: "async",
                activity: () => Promise.resolve(),
                always: { target: "CANCELLED" },
                done: {
                    target: "FULFILLED",
                    actions: () => log.push("done"),
                },
                error: {
                    target: "REJECTED",
                    actions: () => log.push("error"),
                },
            },
            FULFILLED: { type: "atom" },
            REJECTED: { type: "atom" },
            CANCELLED: { type: "atom" },
        },
    });
    const machine = root;

    machine.start();
    await machine.wait("root.CANCELLED");
});

test("composed: enter/exit actions defined inside and outside", async () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "async",
        activity: () => Promise.resolve(),
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                        done: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    await machine.wait("root.A.B");

    expect(log).toEqual([
        "enter OUTSIDE",
        "enter INSIDE",
        "exit INSIDE",
        "exit OUTSIDE",
    ]);
});

test("composed: done actions defined inside and outside", async () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "async",
        activity: () => Promise.resolve(),
        done: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        done: {
                            target: "B",
                            actions: [() => log.push("OUTSIDE")],
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    await machine.wait("root.A.B");
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});

test("composed: error actions defined inside and outside", async () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "async",
        activity: () => Promise.reject(),
        error: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        error: {
                            target: "B",
                            actions: [() => log.push("OUTSIDE")],
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    await machine.wait("root.A.B");
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});

test("composed: event actions defined inside and outside", () => {
    const log: string[] = [];
    const C = {
        name: "C",
        type: "async",
        events: {
            SELF: { target: "self", actions: [() => log.push("SELF")] },
        },
        activity: () => new Promise(() => {}),
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        events: {
                            CANCEL: {
                                target: "B",
                                actions: [() => log.push("CANCEL")],
                            },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("SELF");
    machine.send("CANCEL");
    expect(log).toEqual(["SELF", "CANCEL"]);
});

test("composed: auto actions defined inside and outside", async () => {
    const log: string[] = [];
    const A = {
        name: "A",
        type: "async",
        activity: () => new Promise(() => {}),
        always: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "external",
                source: A,
                always: { target: "B", actions: [() => log.push("OUTSIDE")] },
            },
            B: { type: "atom" },
        },
    });

    const machine = root;

    machine.start();
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});
