import { StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("multiple done transitions", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH_C: { target: "DONE_C" },
                            FINISH_D: { target: "DONE_D" },
                        },
                    },
                    DONE_C: {
                        type: "done",
                    },
                    DONE_D: {
                        type: "done",
                    },
                },
                done: [
                    {
                        target: "C",
                        guard: (data) => data === "DONE_C",
                    },
                    {
                        target: "D",
                        guard: (data) => data === "DONE_D",
                    },
                ],
            },
            C: {
                type: "atom",
                events: {
                    TO_A: { target: "A" },
                },
            },
            D: {
                type: "atom",
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toEqual(true);
    machine.send("FINISH_C");
    expect(machine.status.matches("root.C")).toEqual(true);
    machine.send("TO_A");
    expect(machine.status.matches("root.A.B")).toEqual(true);
    machine.send("FINISH_D");
    expect(machine.status.matches("root.D")).toEqual(true);
});

test("multiple done transitions with default", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH_C: { target: "DONE" },
                            FINISH_D: { target: "DONE_D" },
                        },
                    },
                    DONE: { type: "done" },
                    DONE_D: { type: "done" },
                },
                done: [
                    { target: "C", guard: (data) => data === "DONE" },
                    { target: "D", guard: (data) => data === "DONE_D" },
                ],
            },
            C: {
                type: "atom",
                events: {
                    TO_A: { target: "A" },
                },
            },
            D: {
                type: "atom",
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toEqual(true);
    machine.send("FINISH_C");
    expect(machine.status.matches("root.C")).toEqual(true);
    machine.send("TO_A");
    expect(machine.status.matches("root.A.B")).toEqual(true);
    machine.send("FINISH_D");
    expect(machine.status.matches("root.D")).toEqual(true);
});
