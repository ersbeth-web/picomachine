import { type StateConfig, StateMachine } from "@ersbeth/picomachine";
//@ts-ignore
import FakeTimers from "@sinonjs/fake-timers";
import { expect, test } from "vitest";

test("event transition", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                },
            },
            STATE2: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE3",
                    },
                },
            },
            STATE3: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.STATE1")).toBe(true);
    machine.send("NEXT");
    expect(machine.status.matches("root.STATE2")).toBe(true);
    machine.send("NEXT");
    expect(machine.status.matches("root.STATE3")).toBe(true);
});

test("event transition (wrong event)", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.STATE1")).toBe(true);
    expect(() => machine.send("WRONG_EVENT")).toThrowError();
});

test("event transition (no event)", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.STATE1")).toBe(true);
    expect(() => machine.send("WRONG_EVENT")).toThrowError();
});

test("event transition (multiple events)", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                    SELF: {
                        target: "self",
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.STATE1")).toBe(true);
    machine.send("SELF");
    expect(machine.status.matches("root.STATE1")).toBe(true);
    machine.send("NEXT");
    expect(machine.status.matches("root.STATE2")).toBe(true);
});

test("auto transition", async () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                type: "atom",
                always: {
                    target: "END",
                },
            },
            END: {
                type: "atom",
            },
        },
    });
    const machine = root;
    machine.start();
    expect(machine.status.matches("root.END")).toBe(true);
});

test("delayed event transition", async () => {
    const clock = FakeTimers.install();
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "B",
                        delay: 500,
                    },
                },
            },
            B: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("NEXT");
    expect(machine.status.matches("root.A")).toBe(true);
    clock.tick(500);
    await machine.wait("root.B");
    clock.uninstall();
});

test("delayed auto transition", async () => {
    const clock = FakeTimers.install();
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                type: "atom",
                always: {
                    target: "DONE",
                    delay: 500,
                },
            },
            DONE: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.TRANSIENT")).toBe(true);
    clock.tick(500);
    await machine.wait("root.DONE");
    clock.uninstall();
});

test("multiple event transitions", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    NEXT: [
                        {
                            target: "B",
                        },
                        {
                            target: "C",
                        },
                    ],
                },
            },
            B: {
                type: "atom",
            },
            C: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("NEXT");
    expect(machine.status.matches("root.B")).toBe(true);
});

test("multiple auto transitions", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                always: [
                    {
                        target: "B",
                    },
                    {
                        target: "C",
                    },
                ],
            },
            B: {
                type: "atom",
            },
            C: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.B")).toBe(true);
});

test("guarded event transitions", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    NEXT: [
                        {
                            target: "B",
                            guard: () => false,
                        },
                        {
                            target: "C",
                            guard: () => true,
                        },
                    ],
                },
            },
            B: {
                type: "atom",
            },
            C: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("NEXT");
    expect(machine.status.matches("root.C")).toBe(true);
});

test("guarded auto transitions", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                always: [
                    {
                        target: "B",
                        guard: () => false,
                    },
                    {
                        target: "C",
                        guard: () => true,
                    },
                ],
            },
            B: {
                type: "atom",
            },
            C: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.C")).toBe(true);
});

test("composition: events transitions defined outside", () => {
    const C = {
        name: "C",
        type: "atom",
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
    machine.send("TO_C");
    expect(machine.status.matches("root.A.C")).toBe(true);
    machine.send("TO_B");
    expect(machine.status.matches("root.A.B")).toBe(true);
});

test("composition: auto transitions defined outside", () => {
    const C = {
        name: "C",
        type: "atom",
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "C",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        always: { target: "B" },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A.B")).toBe(true);
});

test("internal transition", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "internal",
                    },
                },
            },
            B: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("NEXT");
    expect(machine.status.matches("root.A")).toBe(true);
});

test("children transition", () => {
    const machine = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        events: {
            toA: { distribute: true, target: "A" },
            toB: { distribute: true, target: "B" },
            toC: { distribute: true, target: "C" },
        },
        states: {
            A: {
                type: "atom",
            },
            B: {
                type: "atom",
            },
            C: {
                type: "atom",
            },
        },
    });

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("toB");
    expect(machine.status.matches("root.B")).toBe(true);
    machine.send("toC");
    expect(machine.status.matches("root.C")).toBe(true);
    machine.send("toA");
    expect(machine.status.matches("root.A")).toBe(true);
});

test("children guarded transition", () => {
    const machine = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        events: {
            toGuardedB: [
                { distribute: true, target: "A", guard: () => false },
                { distribute: true, target: "B" },
            ],
            toGuardedA: [
                { distribute: true, target: "A", guard: () => true },
                { distribute: true, target: "B" },
            ],
        },
        states: {
            A: {
                type: "atom",
            },
            B: {
                type: "atom",
            },
        },
    });

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("toGuardedB");
    expect(machine.status.matches("root.B")).toBe(true);
    machine.send("toGuardedA");
    expect(machine.status.matches("root.A")).toBe(true);
});

test("children transition override", () => {
    const machine = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        events: {
            next: { distribute: true, target: "C" },
        },
        states: {
            A: {
                type: "atom",
                events: {
                    next: { target: "B" },
                },
            },
            B: {
                type: "atom",
            },
            C: {
                type: "atom",
            },
        },
    });

    machine.start();
    expect(machine.status.matches("root.A")).toBe(true);
    machine.send("next");
    expect(machine.status.matches("root.C")).toBe(true);
});
