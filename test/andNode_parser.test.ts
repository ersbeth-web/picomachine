import { StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("And Node: Fail if subnode source doesn't match subnode name", () => {
    const A = new StateMachine({
        name: "D",
        type: "atom",
    });
    expect(
        () =>
            new StateMachine({
                name: "root",
                type: "parallel",
                states: {
                    A: {
                        type: "external",
                        source: A,
                    },
                    B: { type: "atom" },
                },
            }),
    ).toThrowError();
});
