import { type StateConfig, StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("enter/exit action (single)", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                enter: () => log.push("enter"),
                exit: () => log.push("exit"),
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual(["enter"]);
    machine.send("NEXT");
    expect(log).toEqual(["enter", "exit"]);
});

test("enter/exit action (multiple)", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                enter: [() => log.push("enter"), () => log.push("enter2")],
                exit: [() => log.push("exit"), () => log.push("exit2")],
                events: {
                    NEXT: {
                        target: "STATE2",
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual(["enter", "enter2"]);
    machine.send("NEXT");
    expect(log).toEqual(["enter", "enter2", "exit", "exit2"]);
});

test("event transition (single action)", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: () => log.push("next"),
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("NEXT");
    expect(log).toEqual(["next"]);
});

test("event transition (multiple actions)", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: [
                            () => log.push("next"),
                            () => log.push("next2"),
                        ],
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("NEXT");
    expect(log).toEqual(["next", "next2"]);
});

test("wrong event transition", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: () => log.push("next"),
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    expect(() => machine.send("WRONG_EVENT")).toThrowError();
});

test("multiple event transitions", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: () => log.push("next"),
                    },
                    SELF: {
                        target: "self",
                        actions: () => log.push("self"),
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("SELF");
    expect(log).toEqual(["self"]);
    machine.send("NEXT");
    expect(log).toEqual(["self", "next"]);
});

test("exit/event/enter action order", () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                enter: () => log.push("enterA"),
                exit: () => log.push("exitA"),
                events: {
                    TO_B: {
                        target: "B",
                        actions: () => log.push("transit A->B"),
                    },
                },
            },
            B: {
                type: "atom",
                enter: () => log.push("enterB"),
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual(["enterA"]);
    machine.send("TO_B");
    expect(log).toEqual(["enterA", "exitA", "transit A->B", "enterB"]);
});

test("auto transition (single action)", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                type: "atom",
                always: {
                    target: "END",
                    actions: () => log.push("transient action"),
                },
            },
            END: {
                type: "atom",
            },
        },
    });
    const machine = root;
    machine.start();
    expect(log).toEqual(["transient action"]);
});

test("auto transition (multi actions)", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                type: "atom",
                always: {
                    target: "END",
                    actions: [
                        () => log.push("transient action"),
                        () => log.push("transient action2"),
                    ],
                },
            },
            END: {
                type: "atom",
            },
        },
    });
    const machine = root;
    machine.start();
    expect(log).toEqual(["transient action", "transient action2"]);
});

test("enter/auto/exit actions order", async () => {
    const log: string[] = [];
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "TRANSIENT",
        states: {
            TRANSIENT: {
                type: "atom",
                enter: () => log.push("enterTRANSIENT"),
                exit: () => log.push("exitTRANSIENT"),
                always: {
                    target: "END",
                    actions: () => log.push("transient TRANSIENT->END"),
                },
            },
            END: {
                type: "atom",
                enter: () => log.push("enterEND"),
            },
        },
    });
    const machine = root;
    machine.start();
    expect(log).toEqual([
        "enterTRANSIENT",
        "exitTRANSIENT",
        "transient TRANSIENT->END",
        "enterEND",
    ]);
});

test("composed: enter/exit actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "atom",
        enter: () => log.push("enter INSIDE"),
        exit: () => log.push("exit INSIDE"),
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        enter: () => log.push("enter OUTSIDE"),
                        exit: () => log.push("exit OUTSIDE"),
                        events: {
                            TO_B: { target: "B" },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("TO_B");
    expect(log).toEqual([
        "enter OUTSIDE",
        "enter INSIDE",
        "exit INSIDE",
        "exit OUTSIDE",
    ]);
});

test("composed: events actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "atom",
        events: {
            SELF: { target: "self", actions: [() => log.push("SELF")] },
        },
    } as const satisfies StateConfig;

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        events: {
                            TO_B: {
                                target: "B",
                                actions: [() => log.push("TO_B")],
                            },
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    machine.send("TO_C");
    machine.send("SELF");
    machine.send("TO_B");
    expect(log).toEqual(["SELF", "TO_B"]);
});

test("composed: auto actions defined inside and outside", () => {
    const log: string[] = [];

    const C = {
        name: "C",
        type: "atom",
        always: {
            target: "self",
            guard: () => {
                log.push("GUARD_INSIDE");
                return false;
            },
            actions: [() => log.push("INSIDE")],
        },
    } as const satisfies StateConfig;
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "C",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: {
                        type: "external",
                        source: C,
                        always: {
                            target: "B",
                            actions: [() => log.push("OUTSIDE")],
                        },
                    },
                },
            },
        },
    });

    const machine = root;

    machine.start();
    expect(log).toEqual(["GUARD_INSIDE", "OUTSIDE"]);
});

test("internal transition", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "internal",
                        actions: () => log.push("INTERNAL"),
                    },
                },
            },
            B: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    machine.send("NEXT");
    expect(log).toEqual(["INTERNAL"]);
});
