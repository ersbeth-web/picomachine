import { StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("event data (single)", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: (data) => log.push(data),
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("NEXT", "data");
    expect(log).toEqual(["data"]);
});

test("event data sending (multiple)", () => {
    const log: string[] = [];

    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "STATE1",
        states: {
            STATE1: {
                type: "atom",
                events: {
                    NEXT: {
                        target: "STATE2",
                        actions: [
                            (data) => log.push(data.data1),
                            (data) => log.push(data.data2),
                        ],
                    },
                },
            },
            STATE2: {
                type: "atom",
            },
        },
    });
    const machine = root;

    machine.start();
    expect(log).toEqual([]);
    machine.send("NEXT", { data1: "data1", data2: "data2" });
    expect(log).toEqual(["data1", "data2"]);
});
