import { StateMachine } from "@ersbeth/picomachine";
import { expect, test } from "vitest";

test("done is raised", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                done: { target: "DONE" },
                error: { target: "ERROR" },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: { type: "done" },
                    ERROR: { type: "error" },
                },
            },
            DONE: { type: "done" },
            ERROR: { type: "error" },
        },
    });
    const machine = root;
    let done = false;
    machine.onDone(() => {
        done = true;
    });
    machine.start();
    expect(done).toBe(false);
    machine.send("FINISH");
    expect(done).toBe(true);
});

test("error is raised", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                initial: "B",
                done: { target: "DONE" },
                error: { target: "ERROR" },
                states: {
                    B: {
                        type: "atom",
                        events: {
                            FINISH: { target: "DONE" },
                            FAIL: { target: "ERROR" },
                        },
                    },
                    DONE: { type: "done" },
                    ERROR: { type: "error" },
                },
            },
            DONE: { type: "done" },
            ERROR: { type: "error" },
        },
    });
    const machine = root;
    let error = false;
    machine.onError(() => {
        error = true;
    });
    machine.start();
    expect(error).toBe(false);
    machine.send("FAIL");
    expect(error).toBe(true);
});

test("changed is raised", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "nested",
                events: {
                    TO_D: { target: "D" },
                },
                initial: "B",
                states: {
                    B: {
                        type: "atom",
                        events: {
                            TO_C: { target: "C" },
                        },
                    },
                    C: { type: "atom" },
                },
            },
            D: { type: "atom" },
        },
    });
    const machine = root;
    let changeCount = 0;
    machine.onChange(() => changeCount++);
    machine.start();
    expect(changeCount).toBe(3);
    machine.send("TO_C");
    expect(changeCount).toBe(4);
    machine.send("TO_D");
    expect(changeCount).toBe(5);
});

test("events are stacked", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    move: {
                        target: "internal",
                        actions: () => root.send("to_B"),
                    },
                    to_B: { target: "B" },
                },
            },
            B: { type: "atom" },
        },
    });
    const machine = root;
    machine.start();
    machine.send("move");
    expect(machine.status.matches("B")).toEqual(true);
});

test("throw on unknown event", () => {
    const root = new StateMachine({
        name: "root",
        type: "nested",
        initial: "A",
        states: {
            A: {
                type: "atom",
                events: {
                    to_B: { target: "B" },
                },
            },
            B: { type: "atom" },
        },
    });
    const machine = root;
    machine.start();
    expect(() => machine.send("TO_C")).toThrow();
});
